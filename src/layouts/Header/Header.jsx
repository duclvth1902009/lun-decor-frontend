/* eslint-disable */
import React from 'react';
import { NavLink } from 'react-router-dom';

function Header() {

    const onclickNav = () => {
        $(".mobile-main-menu").addClass('active');
        $(".backdrop__body-backdrop___1rvky").addClass('active');
    }
    const closeNav = () => {
        $(".mobile-main-menu").removeClass('active');
        $(".backdrop__body-backdrop___1rvky").removeClass('active');
    }
    const packageHover = () => {
        $("#iconPackage").removeClass('fa-angle-right').addClass('fa-angle-down');
    }
    const packageNotHover = () => {
        $("#iconPackage").addClass('fa-angle-right').removeClass('fa-angle-down');
    }
    const projectHover = () => {
        $("#iconProject").removeClass('fa-angle-right').addClass('fa-angle-down');
    }
    const projectNotHover = () => {
        $("#iconProject").addClass('fa-angle-right').removeClass('fa-angle-down');
    }

    return (
        <div>
            <header className="header">
                <div className="container">
                    <div className="row">
                        <div className="top-link clearfix hidden-sm hidden-xs">
                            <div className="col-md-6 col-xs-12 header-static">
                                <div className="social-title">Theo dõi: </div>
                                <NavLink to="#" className="fb" title="Facebook"/>
                                <NavLink to="#" className="tt" title="Twitter"/>
                                <NavLink to="#" className="gp" title="Google Plus"/>
                                <NavLink to="#" className="pin" title="Pinterest"/>
                            </div>
                            <div className="col-md-6 col-xs-12 right">
                                <ul className="header links">
                                    <li><NavLink to="/tracking-orders-auth" title="Theo dõi đơn hàng"><i className="fa fa-sign-in" aria-hidden="true"/> Theo dõi đơn hàng</NavLink></li>
                                    <li><NavLink to="/package-booking" title="Thuê dịch vụ"><i className="fa fa-user-plus" aria-hidden="true"/> Thuê dịch vụ</NavLink></li>
                                </ul>
                            </div>
                        </div>
                        <div className="header-main clearfix">
                            <div className="col-lg-3 col-md-3 col-100-h">
                                <button type="button" onClick={onclickNav} className="navbar-toggle collapsed visible-sm visible-xs" id="trigger-mobile">
                                    <span className="sr-only">Toggle navigation</span><span className="icon-bar"/>
                                    <span className="icon-bar"/><span className="icon-bar"/>
                                </button>
                                <div className="logo"><NavLink to="/" className="logo-wrapper" title="Lun Decor"><img src={process.env.PUBLIC_URL + '/logo.svg'} alt="Logo Lun Decor" /></NavLink></div>
                                <div className="mobile-cart visible-sm visible-xs">
                                    <NavLink to="/cart" title="Giỏ hàng"><i className="fa fa-cart-arrow-down"/>
                                        <div className="cart-right"><span className="count_item_pr">0</span></div>
                                    </NavLink>
                                </div>
                            </div>
                            <div className="col-lg-5 col-md-5">
                                <form className="input-group search-bar search_form has-validation-callback" action="#" method="get" role="search">
                                    <input type="search" name="query" defaultValue="" placeholder="Tìm kiếm..." className="input-group-field st-default-search-input search-text" autoComplete="off" />
                                    <span className="input-group-btn"><button className="btn icon-fallback-text"><i className="fa fa-search"/></button></span>
                                </form>
                            </div>
                            <div className="col-lg-4 col-md-4 text-right hidden-sm hidden-xs clearfix">
                                <div className="items-cart-inner clearfix mini-cart">
                                    <NavLink className="showcart" to="/cart" title="Giỏ hàng">
                                        <i className="fa fa-cart-arrow-down"/>
                                        <div className="basket-item-count count_item_pr">2</div>
                                    </NavLink>
                                    <div className="top-cart-content">
                                        <ul id="cart-sidebar" className="mini-products-list count_li">
                                            <ul className="list-item-cart">
                                                <li className="item productid-22735316">
                                                    <NavLink className="product-image" to="#" title="Gói Sắc Hồng">
                                                        <img alt="Gói Sắc Hồng" src="https://drive.google.com/uc?export=view&id=1R-JfK7DmNyi4sU8-LghAXV2OzUXkBjpJ" width="80" />
                                                    </NavLink>
                                                    <div className="detail-item">
                                                        <div className="product-details">
                                                            <NavLink to="#" data-id="22735316" title="Xóa" className="remove-item-cart fa fa-remove">&nbsp;</NavLink>
                                                            <p className="product-name">
                                                                <NavLink to="#" title="Gói Sắc Hồng">Gói Sắc Hồng</NavLink>
                                                            </p>
                                                        </div>
                                                        <div className="product-details-bottom">
                                                            <span className="price">8,000,000₫</span>
                                                            <div className="quantity-select">
                                                                <input className="variantID" type="hidden" name="variantId" defaultValue="22735316" />
                                                                <button className="reduced items-count btn-minus" disabled="" type="button">–</button>
                                                                <input type="text" disabled="" maxLength="3" min="1" className="input-text number-sidebar qty22735316" id="qty22735316" name="Lines" size="4" defaultValue="1" />
                                                                <button className="increase items-count btn-plus" type="button">+</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li className="item productid-22735227">
                                                    <NavLink className="product-image" to="#" title="Cây Vạn Niên Thanh">
                                                        <img alt="Gói Sắc Tím" src="https://drive.google.com/uc?export=view&id=1yFZbo5rU2Xo_qybie2AqUIGj4sOiPj15" width="80" />
                                                    </NavLink>
                                                    <div className="detail-item">
                                                        <div className="product-details">
                                                            <NavLink to="#" data-id="22735227" title="Xóa" className="remove-item-cart fa fa-remove">&nbsp;</NavLink>
                                                            <p className="product-name">
                                                                <NavLink to="#" title="Cây Vạn Niên Thanh">Gói Sắc Tím</NavLink>
                                                            </p>
                                                        </div>
                                                        <div className="product-details-bottom">
                                                            <span className="price">5,000,000₫</span>
                                                            <div className="quantity-select">
                                                                <input className="variantID" type="hidden" name="variantId" defaultValue="22735227" />
                                                                <button className="reduced items-count btn-minus" disabled="" type="button">–</button>
                                                                <input type="text" disabled="" maxLength="3" min="1" className="input-text number-sidebar qty22735227" id="qty22735227" name="Lines" size="4" defaultValue="1" />
                                                                <button className="increase items-count btn-plus" type="button">+</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                            <div>
                                                <div className="top-subtotal">Tổng cộng: <span className="price">13,000,000₫</span></div>
                                            </div>
                                            <div>
                                                <div className="actions clearfix">
                                                    <NavLink to="/checkout" className="btn btn-gray btn-checkout" title="Thanh toán">
                                                        <span>Thanh toán</span>
                                                    </NavLink>
                                                    <NavLink to="/cart" className="view-cart btn btn-white margin-left-5" title="Giỏ hàng">
                                                        <span>Giỏ hàng</span>
                                                    </NavLink>
                                                </div>
                                            </div>
                                        </ul>
                                    </div>
                                </div>
                                <div className="customer-support-text clearfix">
                                    <img src="//bizweb.dktcdn.net/100/344/983/themes/704702/assets/hotline_image.svg?1619100636914" alt="Hotline" />
                                    <div className="text">
                                        <span>Hotline</span><a href="tel:0399002423">0399 002 423</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <nav className="hidden-sm hidden-xs">
                    <div className="container">
                        <ul id="nav" className="nav">
                            <li className="nav-item"><NavLink to="/" activeStyle={{ color: "#F67A80" }} exact={true} className="nav-link" title="Trang chủ">Trang chủ</NavLink></li>
                            <li onMouseOver={packageHover} onMouseLeave={packageNotHover} className="nav-item has-mega">
                                <NavLink to="/package" activeStyle={{ color: "#F67A80" }} className="nav-link" title="Dịch vụ">Gói Dịch Vụ <i id="iconPackage" className="fa fa-angle-right"  data-toggle="dropdown"/></NavLink>
                                <div className="mega-content">
                                    <ul className="level0 col-md-9 no-padding">
                                        <li className="level1 parent item">
                                            <NavLink className="hmega" to="#">Tiệc cưới</NavLink>
                                            <ul className="level1">
                                                <li className="level2">
                                                    <NavLink to="#">Gói Sắc Hồng</NavLink>
                                                </li>
                                                <li className="level2">
                                                    <NavLink to="#">Tiệc cưới 2</NavLink>
                                                </li>
                                                <li className="level2">
                                                    <NavLink to="#">Tiệc cưới 3</NavLink>
                                                </li>
                                            </ul>
                                        </li>
                                        <li className="level1 parent item">
                                            <NavLink className="hmega" to="#">Tiệc sinh nhật</NavLink>
                                            <ul className="level1">
                                                <li className="level2">
                                                    <NavLink to="#">Tiệc sinh nhật 1</NavLink>
                                                </li>
                                                <li className="level2">
                                                    <NavLink to="#">Tiệc sinh nhật 2</NavLink>
                                                </li>
                                                <li className="level2">
                                                    <NavLink to="#">Tiệc sinh nhật 3</NavLink>
                                                </li>
                                            </ul>
                                        </li>
                                        <li className="level1 parent item">
                                            <NavLink className="hmega" to="#">Tiệc tân gia</NavLink>
                                            <ul className="level1">
                                                <li className="level2">
                                                    <NavLink to="#">Tiệc tân gia 1</NavLink>
                                                </li>
                                                <li className="level2">
                                                    <NavLink to="#">Tiệc tân gia 2</NavLink>
                                                </li>
                                                <li className="level2">
                                                    <NavLink to="#">Tiệc tân gia 3</NavLink>
                                                </li>
                                            </ul>
                                        </li>
                                        <li className="level1 parent item">
                                            <NavLink className="hmega" to="#">Tiệc tròn tuổi</NavLink>
                                            <ul className="level1">
                                                <li className="level2">
                                                    <NavLink to="#">Tiệc tròn tuổi 1</NavLink>
                                                </li>
                                                <li className="level2">
                                                    <NavLink to="#">Tiệc tròn tuổi 2</NavLink>
                                                </li>
                                                <li className="level2">
                                                    <NavLink to="#">Tiệc tròn tuổi 3</NavLink>
                                                </li>
                                            </ul>
                                        </li>
                                        <li className="level1 parent item">
                                            <NavLink className="hmega" to="#">Tiệc bạn bè</NavLink>
                                            <ul className="level1">
                                                <li className="level2">
                                                    <NavLink to="#">Tiệc bạn bè 1</NavLink>
                                                </li>
                                                <li className="level2">
                                                    <NavLink to="#">Tiệc bạn bè 2</NavLink>
                                                </li>
                                                <li className="level2">
                                                    <NavLink to="#">Tiệc bạn bè 3</NavLink>
                                                </li>
                                            </ul>
                                        </li>
                                        <li className="level1 parent item">
                                            <NavLink className="hmega" to="#">Tiệc khai trương</NavLink>
                                            <ul className="level1">
                                                <li className="level2">
                                                    <NavLink to="#">Tiệc khai trương 1</NavLink>
                                                </li>
                                                <li className="level2">
                                                    <NavLink to="#">Tiệc khai trương 2</NavLink>
                                                </li>
                                                <li className="level2">
                                                    <NavLink to="#">Tiệc khai trương 3</NavLink>
                                                </li>
                                            </ul>
                                        </li>
                                        <li className="level1 parent item">
                                            <NavLink className="hmega" to="#">Gói dịch vụ khác</NavLink>
                                            <ul className="level1">
                                                <li className="level2">
                                                    <NavLink to="#">Gói dịch vụ khác 1</NavLink>
                                                </li>
                                                <li className="level2">
                                                    <NavLink to="#">Gói dịch vụ khác 2</NavLink>
                                                </li>
                                                <li className="level2">
                                                    <NavLink to="#">Gói dịch vụ khác 3</NavLink>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                    <div className="col-md-3 no-padding">
                                        <NavLink to="#" className="clearfix" title="Sản phẩm">
                                            <picture>
                                                <img src="https://m.media-amazon.com/images/I/71TlRjhBE-L._AC_SL1001_.jpg" alt="Sản phẩm" className="img-responsive center-block" />
                                            </picture>
                                        </NavLink>
                                    </div>
                                </div>
                            </li>
                            <li onMouseOver={projectHover} onMouseLeave={projectNotHover} className="nav-item has-mega">
                                <NavLink to="/project" activeStyle={{ color: "#F67A80" }} className="nav-link" title="Dịch vụ">Các tiệc <i id="iconPackage" className="fa fa-angle-right"  data-toggle="dropdown"/></NavLink>
                                <div className="mega-content">
                                    <ul className="level0 col-md-9 no-padding">
                                        <li className="level1 parent item">
                                            <NavLink className="hmega" to="#">Tiệc cưới</NavLink>
                                            <ul className="level1">
                                                <li className="level2">
                                                    <NavLink to="#">Gói Sắc Hồng</NavLink>
                                                </li>
                                                <li className="level2">
                                                    <NavLink to="#">Tiệc cưới 2</NavLink>
                                                </li>
                                                <li className="level2">
                                                    <NavLink to="#">Tiệc cưới 3</NavLink>
                                                </li>
                                            </ul>
                                        </li>
                                        <li className="level1 parent item">
                                            <NavLink className="hmega" to="#">Tiệc sinh nhật</NavLink>
                                            <ul className="level1">
                                                <li className="level2">
                                                    <NavLink to="#">Tiệc sinh nhật 1</NavLink>
                                                </li>
                                                <li className="level2">
                                                    <NavLink to="#">Tiệc sinh nhật 2</NavLink>
                                                </li>
                                                <li className="level2">
                                                    <NavLink to="#">Tiệc sinh nhật 3</NavLink>
                                                </li>
                                            </ul>
                                        </li>
                                        <li className="level1 parent item">
                                            <NavLink className="hmega" to="#">Tiệc tân gia</NavLink>
                                            <ul className="level1">
                                                <li className="level2">
                                                    <NavLink to="#">Tiệc tân gia 1</NavLink>
                                                </li>
                                                <li className="level2">
                                                    <NavLink to="#">Tiệc tân gia 2</NavLink>
                                                </li>
                                                <li className="level2">
                                                    <NavLink to="#">Tiệc tân gia 3</NavLink>
                                                </li>
                                            </ul>
                                        </li>
                                        <li className="level1 parent item">
                                            <NavLink className="hmega" to="#">Tiệc tròn tuổi</NavLink>
                                            <ul className="level1">
                                                <li className="level2">
                                                    <NavLink to="#">Tiệc tròn tuổi 1</NavLink>
                                                </li>
                                                <li className="level2">
                                                    <NavLink to="#">Tiệc tròn tuổi 2</NavLink>
                                                </li>
                                                <li className="level2">
                                                    <NavLink to="#">Tiệc tròn tuổi 3</NavLink>
                                                </li>
                                            </ul>
                                        </li>
                                        <li className="level1 parent item">
                                            <NavLink className="hmega" to="#">Tiệc bạn bè</NavLink>
                                            <ul className="level1">
                                                <li className="level2">
                                                    <NavLink to="#">Tiệc bạn bè 1</NavLink>
                                                </li>
                                                <li className="level2">
                                                    <NavLink to="#">Tiệc bạn bè 2</NavLink>
                                                </li>
                                                <li className="level2">
                                                    <NavLink to="#">Tiệc bạn bè 3</NavLink>
                                                </li>
                                            </ul>
                                        </li>
                                        <li className="level1 parent item">
                                            <NavLink className="hmega" to="#">Tiệc khai trương</NavLink>
                                            <ul className="level1">
                                                <li className="level2">
                                                    <NavLink to="#">Tiệc khai trương 1</NavLink>
                                                </li>
                                                <li className="level2">
                                                    <NavLink to="#">Tiệc khai trương 2</NavLink>
                                                </li>
                                                <li className="level2">
                                                    <NavLink to="#">Tiệc khai trương 3</NavLink>
                                                </li>
                                            </ul>
                                        </li>
                                        <li className="level1 parent item">
                                            <NavLink className="hmega" to="#">Gói dịch vụ khác</NavLink>
                                            <ul className="level1">
                                                <li className="level2">
                                                    <NavLink to="#">Gói dịch vụ khác 1</NavLink>
                                                </li>
                                                <li className="level2">
                                                    <NavLink to="#">Gói dịch vụ khác 2</NavLink>
                                                </li>
                                                <li className="level2">
                                                    <NavLink to="#">Gói dịch vụ khác 3</NavLink>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                    <div className="col-md-3 no-padding">
                                        <NavLink to="#" className="clearfix" title="Sản phẩm">
                                            <picture>
                                                <img src="https://m.media-amazon.com/images/I/71TlRjhBE-L._AC_SL1001_.jpg" alt="Sản phẩm" className="img-responsive center-block" />
                                            </picture>
                                        </NavLink>
                                    </div>
                                </div>
                            </li>
                            <li className="nav-item ">
                                <NavLink to="/new" activeStyle={{ color: "#F67A80" }} className="nav-link" title="Tin tức">Tin tức</NavLink>
                            </li>
                            <li className="nav-item"><NavLink activeStyle={{ color: "#F67A80" }} to="/about" className="nav-link" title="Giới thiệu">Giới thiệu</NavLink></li>
                            <li className="nav-item"><NavLink activeStyle={{ color: "#F67A80" }} to="/contact" className="nav-link" title="Liên hệ">Liên hệ</NavLink></li>
                        </ul>
                    </div>
                </nav>
            </header>
            <div id="backdrop" className="backdrop__body-backdrop___1rvky" onClick={closeNav}/>
            <div id="mobile-main-menu" className="mobile-main-menu">
                <div className="drawer-header d-flex">
                    <button type="button" onClick={closeNav} className="navbar-toggle collapsed" id="trigger-mobile" style={{ display: "block!important", float: "left", marginTop: "12px" }}>
                        <i className="fa fa-times" aria-hidden="true" style={{color: "#fff"}}/>
                    </button>
                    <NavLink to="tracking-orders-auth ">
                        <div className="drawer-header--auth" style={{ justifyContent: "flex-end" }}>
                            <div className="_object"><img src="//bizweb.dktcdn.net/100/344/983/themes/704702/assets/user.svg?1619100636914"
                                alt="Lun Decor" /></div>
                        </div>
                    </NavLink>
                </div>
                <div className="la-scroll-fix-infor-user">
                    <div className="la-nav-menu-items">
                        <div className="la-title-nav-items"><strong>Danh mục</strong></div>
                        <ul className="la-nav-list-items" onClick={closeNav}>
                            <li className="ng-scope"><NavLink to="/" title="Trang chủ">Trang chủ</NavLink></li>
                            <li className="ng-scope"><NavLink to="/package" title="Dịch vụ">Gói dịch vụ</NavLink></li>
                            <li className="ng-scope"><NavLink to="/new" title="Tin tức">Tin tức</NavLink></li>
                            <li className="ng-scope"><NavLink to="/about" title="Giới thiệu">Giới thiệu</NavLink></li>
                            <li className="ng-scope"><NavLink to="/contact" title="Liên hệ">Liên hệ</NavLink></li>
                            <li className="ng-scope"><NavLink to="/tracking-orders-auth" title="Theo dõi đơn hàng" style={{ color: "#F67A80" }}>Theo dõi đơn hàng</NavLink></li>
                            <li className="ng-scope"><NavLink to="/package-booking" title="Thuê dịch vụ ngay" style={{ color: "#F67A80" }}>Thuê dịch vụ ngay</NavLink></li>
                        </ul>
                    </div>
                </div>
                <ul className="mobile-support">
                    <li>
                        <div className="drawer-text-support">HỖ TRỢ</div>
                    </li>
                    <li><i className="fa fa-map-marker" aria-hidden="true"/> Địa chỉ: <a href="#" title="0123456789">Số 8A Tôn Thất Thuyết, Mỹ Đình, Nam Từ Liêm, Hà Nội 100000</a></li>
                    <li><i className="fa fa-phone" aria-hidden="true"/> Điện thoại: <a href="tel:0399002423" title="0399 002 423">0399 002 423</a></li>
                    <li><i className="fa fa-envelope" aria-hidden="true"/> Email: <a href="mailto:lundecor@gmail.com"
                        title="lundecor@gmail.com">lundecor@gmail.com</a></li>
                    <p className="copyright"><span>© Copyright  <span className="s480-f">|</span> <strong>Lun Decor</strong></span></p>

                </ul>
            </div>
        </div>
    )
}

export default Header;
