import React from 'react';
import { NavLink } from 'react-router-dom';
import {backToTop} from "../../utils/utils";

function Footer() {
    return (
        <footer className="footer-container">
            <div className="footer-top">
                <div className="container">
                    <div className="row">
                        <div className="col-md-4 col-sm-4 col-xs-12">
                            <div className="footer-title">
                                <h3 className="footer-logo"><a href="/" title="Lun Decor"><img src={process.env.PUBLIC_URL + '/logo.svg'} alt="Logo Lun Decor"  className="img-responsive" /></a></h3>
                            </div>
                            <div className="footer-content">
                                <div className="footer-info">
                                    <p><strong>Địa chỉ: </strong>Số 8A Tôn Thất Thuyết, Mỹ Đình, Nam Từ Liêm, Hà Nội 100000</p>
                                    <p><strong>Điện thoại: </strong><a href="tel:0399002423" title="0399 002 423">0399 002 423</a></p>
                                    <p><strong>Email: </strong><a href="mailto:lundecor@gmail.com" title="lundecor@gmail.com">lundecor@gmail.com</a></p>
                                </div>
                                <ul className="social">
                                    <li><NavLink to="#" title="Facebook" target="_blank"><span className="fa fa-facebook"></span></NavLink></li>
                                    <li><NavLink to="#" title="Twitter" target="_blank"><span className="fa fa-twitter"></span></NavLink></li>
                                    <li><NavLink to="#" title="Google Plus" target="_blank"><span className="fa fa-google-plus"></span></NavLink></li>
                                    <li><NavLink to="#" title="Pinterest" target="_blank"><span className="fa fa-pinterest-p"></span></NavLink></li>
                                </ul>
                            </div>
                        </div>
                        <div className="col-md-8 col-sm-8 col-xs-12">
                            <div className="row">
                                <div className="col-md-4 col-sm-4 col-xs-12 footer-click">
                                    <div className="footer-title">
                                        <h3>Về chúng tôi</h3>
                                    </div>
                                    <div className="footer-content">
                                        <ul className="menu">
                                            <li><NavLink to="/" title="Trang chủ">Trang chủ</NavLink></li>
                                            <li><NavLink to="/package" title="Dịch vụ">Gói dịch vụ</NavLink></li>
                                            <li><NavLink to="/new" title="Tin tức">Tin tức</NavLink></li>
                                        </ul>
                                    </div>
                                </div>
                                <div className="col-md-4 col-sm-4 col-xs-12 footer-click">
                                    <div className="footer-title">
                                        <h3>Chính sách</h3>
                                    </div>
                                    <div className="footer-content">
                                        <ul className="menu">
                                            <li><NavLink to="/about" title="Giới thiệu">Giới thiệu</NavLink></li>
                                        </ul>
                                    </div>
                                </div>
                                <div className="col-md-4 col-sm-4 col-xs-12 footer-click">
                                    <div className="footer-title">
                                        <h3>Hỗ trợ</h3>
                                    </div>
                                    <div className="footer-content">
                                        <ul className="menu">
                                        <li><NavLink to="/contact" title="Liên hệ">Liên hệ</NavLink></li>
                                        </ul>
                                    </div>
                                </div>
                                <div className="col-md-12 col-sm-12 col-xs-12">
                                    <div className="maichimp-footer clearfix">
                                        <h3 className="text-center">Nhận tin khuyến mãi từ chúng tôi</h3>
                                        <form action="//facebook.us7.list-manage.com/subscribe/post?u=97ba6d3ba28239250923925a8&id=4ef3a755a8" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" target="_blank">
                                            <div className="input-group">
                                                <input type="email" className="form-control" defaultValue="" placeholder="Email của bạn" name="EMAIL" id="mail" />
                                                <span className="input-group-btn"><button className="btn btn-default" name="subscribe" id="subscribe" type="submit">Đăng ký</button></span>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="footer-bottom">
                <div className="container">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="footer-copyright text-center">
                                <small className="copyright"><span>© Copyright  <span className="s480-f">|</span> <strong>Lun Decor</strong></span></small>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="back-to-top" onClick={backToTop}><i className="fa fa-angle-double-up" aria-hidden="true"/></div>
            </div>
        </footer>
    )
};

export default Footer;
