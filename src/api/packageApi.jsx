import axiosClient from "./axiosClient";
import apiUtil from "./apiUtil";

const packageApi = {
    getAll: (atPageNum) => {
        const url = '/package?'+apiUtil.getPageOfDefault(atPageNum);
        return axiosClient.get(url).then(res=>{
            return res
        });
    },

    getAllNoPagi: () => {
        const url = '/package';
        return axiosClient.get(url).then(res=>{
            return res
        });
    },

    getAllEventVenue: () => {
        const url = '/eventVenue';
        return axiosClient.get(url).then(res=>{
            return res
        });
    },


    createRequestOrder: (payLoad) => {
        return axiosClient.post(`requestOrder`, payLoad).then(res => {
            return res
        });
    },

    getById: (id) => {
        const url = `/package/${id}`;
        return axiosClient.get(url).then(res => {
            return res
        });
    },

    getAllType: () => {
        const url = '/eventType';
        return axiosClient.get(url).then(res => {
            return res
        });
    },
    getByType: (type) => {
        const url = '/package/eventType?eventType=' + type;
        return axiosClient.get(url).then(res => {
            return res
        });
    },

    getAllTopic: () => {
        const url = '/topic';
        return axiosClient.get(url).then(res => {
            return res
        });
    },
}

export default packageApi;
