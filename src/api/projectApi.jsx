import axiosClient from "./axiosClient";


const projectApi = {
    sendOtp: (phone, email) => {
        const url = `/requestOrder/sendOtp?phone=${phone}&email=${email}`;
        return axiosClient.get(url).then(res=>{
            return res
        });
    },
    verifyOtp: (phone, email, otpCode, otpId) => {
        const url = `/project?phone=${phone}&email=${email}&otp_id=${otpId}&otp_code=${otpCode}`;
        return axiosClient.get(url).then(res=>{
            return res
        });
    },

    checkRequestOrder: (orderId , otp_id, otp_code) => {
        const url = `requestOrder/checkOtpForRequestOrder?order_id=${orderId}&otp_id=${otp_id}&otp_code=${otp_code}`;
        return axiosClient.get(url).then(res=>{
            return res
        });
    }
}

export default projectApi;
