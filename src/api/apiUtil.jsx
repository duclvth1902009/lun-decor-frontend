
const apiUtil = {
    defaultPagination: () => {
        return 'page=0&size=10&sort=id,ASC'
    },
    buildPagination: (page, size, sortAttribute, sortType) => {
        if (page === null || page !== parseInt(page, 10)) page = 0;
        if (size === null || size !== parseInt(size, 10)) size = 10;
        if (sortAttribute === null) sortAttribute = 'id';
        if (sortType === null || sortType.upper !== 'DESC') sortType = 'ASC';
        return 'page=' + page + '&size=' + size + '&sort=' + sortAttribute + ',' + sortType
    },
    getPageOfDefault: (page) => {
        return 'page=' + page + '&size=12&sort=id,ASC'
    },
}

export default apiUtil;
