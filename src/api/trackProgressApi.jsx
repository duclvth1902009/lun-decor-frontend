import axiosClient from "./axiosClient";

const tracksProgressApi = {
    getTracksByProjectId: (projectId, phoneNumber, OTPCode,OTPId) => {
        const url = `/project/progress?id=${projectId}&phone=${phoneNumber}&otp=${OTPCode}&otp_id=${OTPId}`;
        return axiosClient.get(url);
    }
}

export default tracksProgressApi;
