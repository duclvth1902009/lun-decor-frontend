import React from 'react';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';

import './App.css';

import Header from '../layouts/Header/Header';
import Footer from '../layouts/Footer/Footer';

import Home from '../pages/Home/Home';
import About from '../pages/About/About';
import Contact from '../pages/Contact/Contact';
import New from '../pages/New/New';
import NotFound from '../pages/NotFound/NotFound';

import Package from '../components/Package/Package';

import PackageDetail from '../components/Package/PackageDetail/PackageDetail';
import NewDetail from '../pages/New/NewDetail/NewDetail';
import Cart from '../pages/Cart/Cart';
import Checkout from '../pages/Cart/Checkout/Checkout';
import PackageBooking from '../components/Package/PackageBooking/PackageBooking';
import AuthTracking from "../components/Project/AuthTracking";
import TrackingRequest from "../components/OrderTracking/TrackingRequest";
import ProjectTracking from "../components/Project/ProjectTracking";
import Project from "../components/Project/Project";


function App() {
    return (
        <Router>
            <div className="App">
                <Header/>
                <h1 className="hidden">Lun Decor - </h1>
                <Switch>
                    <Route exact path="/">
                        <Home/>
                    </Route>
                    <Route exact path="/package">
                        <Package/>
                    </Route>
                    <Route exact path="/project">
                        <Project/>
                    </Route>
                    <Route exact path="/new">
                        <New/>
                    </Route>
                    <Route exact path="/about">
                        <About/>
                    </Route>
                    <Route exact path="/contact">
                        <Contact/>
                    </Route>
                    <Route exact path="/tracking-order">
                        <TrackingRequest/>
                    </Route>
                    <Route exact path="/tracking-orders">
                        <ProjectTracking/>
                    </Route>
                    <Route exact path="/tracking-orders-auth">
                        <AuthTracking/>
                    </Route>
                    <Route exact path="/package-booking">
                        <PackageBooking/>
                    </Route>
                    <Route exact path="/package-detail/**">
                        <PackageDetail/>
                    </Route>
                    <Route exact path="/new-detail">
                        <NewDetail/>
                    </Route>
                    <Route exact path="/cart">
                        <Cart/>
                    </Route>
                    <Route exact path="/checkout">
                        <Checkout/>
                    </Route>
                    <Route exact path="*">
                        <NotFound/>
                    </Route>
                </Switch>
                <Footer/>
            </div>
        </Router>
    );
}

export default App;
