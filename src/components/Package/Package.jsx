import React, { useEffect, useState } from 'react';
import {NavLink, useHistory, useLocation} from 'react-router-dom';
import packageApi from '../../api/packageApi';
import PaginationCustom from "../Commons/PaginationCustom";
import $ from 'jquery';
import SinglePackage from "../Commons/SinglePackage";
import {backToTop} from "../../utils/utils";

function Package() {
  const [packageList, setPackageList] = useState([]);
  const [typeList, setTypeList] = useState([]);
  const [type, setType] = useState('all');
  const [pageCurrent, setPageCurrent] = useState(1);
  const [totalItem, setTotalItem] = useState(0);
  const history = useHistory();

  useEffect(() => {
    const fetchPackageList = async () => {
      setPackageList([])
      try {
        let rsp;
        if (type!=='all' && type !== null){
          rsp = await packageApi.getByType(type);
        } else {
          rsp = await packageApi.getAll(pageCurrent-1);
        }
        setPackageList(rsp.data);
        setTotalItem(parseInt(rsp.headers['x-total-count']))
        history.push(`?page=${pageCurrent}&type=${type}`)
        backToTop()
      } catch (error) {
        console.log('Failed to fetch service package list', error);
      }
    }
    fetchPackageList();
  }, [history, pageCurrent, type])

  useEffect(() => {
    const fetchTypeList = async () => {
      try {
        const rsp = await packageApi.getAllType();
        setTypeList(rsp.data);
      } catch (error) {
        console.log('Failed to fetch type list', error);
      }
    }
    fetchTypeList();
  }, [])

  const pageParams = useLocation().search;

  useEffect(() => {
    const pagePick = new URLSearchParams(pageParams).get('page')
    const typePick = new URLSearchParams(pageParams).get('type')
    if (history.location.pathname === '/package') {
      pagePick == null ? setPageCurrent(1) : setPageCurrent(pagePick);
      typePick == null ? setType('all') : setType(typePick);
    }
  }, [history.location.pathname, pageParams]);

  const openFilterMobile = () => {
    $('#ant-sidebar').toggleClass('open');
  }
  function openFilterMobileOut(){
    $('#ant-sidebar').removeClass('open')
  }
  return (
    <div>
      <div className="container"  style={{marginTop:'2em'}}>
        <div className="row">
          <section className="main_container collection col-md-9 col-md-push-3">
            <h1 className="title-head margin-top-0">Tất cả các gói dịch vụ</h1>
            <div className="category-products products category-products-grids">
              <div className="sort-cate clearfix margin-top-10 margin-bottom-10">
                <div className="sort-cate-left hidden-xs">
                  <h3>Xếp theo:</h3>
                  <ul>
                    <li className="btn-quick-sort position-desc">
                      <NavLink to="#"  title="Độ hot"><i/>Độ hot</NavLink>
                    </li>
                    <li className="btn-quick-sort price-asc">
                      <NavLink to="#"  title="Giá tăng dần"><i/>Giá tăng dần</NavLink>
                    </li>
                    <li className="btn-quick-sort price-desc">
                      <NavLink to="#"  title="Giá giảm dần"><i/>Giá giảm dần</NavLink>
                    </li>
                  </ul>
                </div>
                <div className="sort-cate-right-mobile hidden-lg hidden-md hidden-sm">
                  <div id="sort-by">
                    <label className="left">Xếp theo: </label>
                    <ul>
                      <li><span>Thứ tự</span>
                        <ul>
                          <li><NavLink to="#"  title="Độ hot"><i/>Độ hot</NavLink></li>
                          <li><NavLink to="#"  title="Giá tăng dần">Giá tăng dần</NavLink></li>
                          <li><NavLink to="#"  title="Giá giảm dần">Giá giảm dần</NavLink></li>
                        </ul>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <section className="products-view products-view-grid">
                <div className="row">
                  {
                    packageList.map(pack =>
                        (pack && <SinglePackage key={pack.id} pack={pack}/>))
                  }
                </div>
                <div className="text-xs-right">
                  {totalItem > 12 && <PaginationCustom totalItem={totalItem} itemPerPage={12}/>}
                </div>
              </section>
            </div>
          </section>
          <aside onMouseLeave={openFilterMobileOut} id="ant-sidebar" className="ant-sidebar sidebar left-content col-md-3 col-md-pull-9">
            <aside className="aside-item collection-category">
              <div className="aside-title">
                <h3 className="title-head margin-top-0"><span>Danh mục</span></h3>
              </div>
              <div className="aside-content">
                <nav className="nav-category navbar-toggleable-md">
                  <ul className="nav navbar-pills">
                    {typeList && typeList.map(type =>
                        <li key={type.id} className="nav-item">
                          <NavLink id={type.id} to={`/package?page=1&type=${type.id}`} className="nav-link" title=""><i className="fa fa-caret-right" aria-hidden="true"/>{type.name}</NavLink>
                        </li>
                    )}
                    <li className="nav-item">
                      <NavLink to={`/package?page=1&type=all`} className="nav-link" title=""><i className="fa fa-caret-right" aria-hidden="true"/>Tất cả</NavLink>
                    </li>
                  </ul>
                </nav>
              </div>
            </aside>
            <div className="aside-filter">
              <div className="heading">
                <h2 className="title-head">Bộ lọc</h2>
              </div>
              <div className="aside-hidden-mobile">
                <div className="filter-container">
                  <aside className="aside-item filter-price">
                    <div className="aside-title">
                      <h2 className="title-head margin-top-0"><span>Giá sản phẩm</span></h2>
                    </div>
                    <div className="aside-content filter-group">
                      <ul>
                        <li className="filter-item filter-item--check-box filter-item--green">
                          <span>
                            <label htmlFor="filter-duoi-100-000d">
                              <input type="checkbox" id="filter-duoi-100-000d" data-group="Khoảng giá" data-field="price_min" data-text="Dưới 5,000,000đ" defaultValue="(<5000000)" data-operator="OR" />
                              <i className="fa"></i>
                              Giá dưới 5,000,000đ
                            </label>
                          </span>
                        </li>
                        <li className="filter-item filter-item--check-box filter-item--green">
                          <span>
                            <label htmlFor="filter-100-000d-200-000d">
                              <input type="checkbox" id="filter-100-000d-200-000d" data-group="Khoảng giá" data-field="price_min" data-text="5,000,000đ - 8,000,000đ" defaultValue="(>5000000 AND <8000000)" data-operator="OR" />
                              <i className="fa"></i>
                              5,000,000đ - 8,000,000đ
                            </label>
                          </span>
                        </li>
                        <li className="filter-item filter-item--check-box filter-item--green">
                          <span>
                            <label htmlFor="filter-200-000d-300-000d">
                              <input type="checkbox" id="filter-200-000d-300-000d" data-group="Khoảng giá" data-field="price_min" data-text="8,000,000đ - 10,00,000đ" defaultValue="(>8000000 AND <10000000)" data-operator="OR" />
                              <i className="fa"></i>
                              8,000,000đ - 10,000,000đ
                            </label>
                          </span>
                        </li>
                        <li className="filter-item filter-item--check-box filter-item--green">
                          <span>
                            <label htmlFor="filter-tren1-000-000d">
                              <input type="checkbox" id="filter-tren1-000-000d" data-group="Khoảng giá" data-field="price_min" data-text="Trên 10,000,000đ" defaultValue="(>10000000)" data-operator="OR" />
                              <i className="fa"></i>
                              Giá trên 10,000,000đ
                            </label>
                          </span>
                        </li>
                      </ul>
                    </div>
                  </aside>
                </div>
              </div>
            </div>
          </aside>
          <div id="open-filters" onClick={openFilterMobile} className="open-filters hidden-lg hidden-md">
            <i className="fa fa-filter" aria-hidden="true"/>
          </div>
          <div className="col-md-12">
            <div className="collections-top-sale">
              <div className="section_flash_sale">
                <div className="flash-sale-title margin-bottom-30">
                  <a href="san-pham-moi" title="Top Bán Chạy">Top Bán Chạy</a>
                </div>
                <div className="section-tour-owl2 products products-view-grid owl-carousel not-dqowl owl-loaded owl-drag">
                  <div className="owl-stage-outer">
                    <div className="owl-stage">
                      <div className="owl-item active col-sm-3 col-xs-6">
                        <div className="item">
                          <div className="ant-single-product">
                            <div className="ant-single-product-image">
                              <NavLink to="#"><img src="//bizweb.dktcdn.net/100/344/983/themes/704702/assets/loader.svg?1619100636914" alt="" className="img-responsive center-block" /></NavLink>
                              <form action="/cart/add" method="post" encType="multipart/form-data" className="hover-icons hidden-sm hidden-xs variants form-nut-grid form-ajaxtocart" data-id="product-actions-13604778">
                                <input type="hidden" name="variantId" defaultValue="22735280" />
                                <NavLink className="button ajax_addtocart add_to_cart" to="#" title="Mua ngay"></NavLink>
                                <NavLink className="add-to-cart quick-view quickview" to="#" data-handle="" title="Xem nhanh"></NavLink>
                              </form>
                            </div>
                            <div className="ant-product-border">
                              <h3 className="product-title"><NavLink to="#" title="">Gói Sinh Nhật Hồng</NavLink></h3>
                              <div className="product-price">
                                <span className="price">160.000₫</span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Package;
