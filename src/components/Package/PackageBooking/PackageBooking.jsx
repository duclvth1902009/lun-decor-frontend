import React from 'react';
import MultiStepForm from './MultiStepForm/MultiStepForm';

function PackageBooking() {
    return (
        <div className="container">
            <MultiStepForm  style={{marginTop:'2em'}}/>
        </div>
    )
}

export default PackageBooking;
