/* eslint-disable */
import React, {useEffect, useState} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import {Button, TextField, Typography,} from '@material-ui/core';
import {BlockSharp, SendSharp} from '@material-ui/icons';
import DateFnsUtils from '@date-io/date-fns';
import {KeyboardDatePicker, KeyboardTimePicker, MuiPickersUtilsProvider,} from '@material-ui/pickers';
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import ButtonBase from '@material-ui/core/ButtonBase';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormLabel from '@material-ui/core/FormLabel';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';

import userForm from "../../../../../hooks/useForm";
import "./StepOrder.css";
import packageApi from "../../../../../api/packageApi";



const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    margin: `${theme.spacing(2.7)}px auto`,
    maxWidth: 500,
    background: "lightgoldenrodyellow",
  },
  image: {
    width: 128,
    height: 128,
  },
  selectWithImg: {
    justifyContent: "space-between",
  },
  selectedWithImg: {
    background: "#999",
    justifyContent: "space-between",
  },
  img: {
    margin: 'auto',
    display: 'block',
    maxWidth: '100%',
    maxHeight: '100%',
  },
  title: {color: "#999", textAlign: "center"},
  mainContainer: {
    "& input[type='text'], & input[type='password']": {
      margin: '0!important',
      border: 'unset!important'
    },
    "& .MuiSelect-select.MuiOutlinedInput-input": {
      padding: "14px!important"
    },
    justifyContent: "center",
    position: "relative",
    zIndex: "5",
  },
  formContainer: {
    position: "relative",
    height: "auto",
    marginBottom: "0!important"
  },
  inputField: {
    width: "100%",
    marginBottom: "1.5rem"
  },
  btn: {
    width: "90%",
    height: "4rem",
    background: "pink",
    color: "#f1f1f1",
    "&:hover": {
      color: "red",
      opacity: "0.7",
      transition: ".3s ease-in-out"
    },
    margin: "36px"
  },
  disabledBtn: {
    background: "rbga(0,0,0, 0.38)",
    width: "90%",
    height: "4rem",
    margin: "36px"
  },
  errorMsg: {
    marginTop: "0",
    color: "red",
    fontWeight: "200",
    marginBottom: "12px",
    textAlign: "left"
  },
  formControl: {
    margin: "10px 0",
    width: "100%",
  },
  selectEmpty: {
    marginTop: "16px",
  },
  backdrop: {
    zIndex: 999,
    color: '#fff',
  },
  RadioFormLabel: {
    textAlign: "left",
    margin: "10px 0"
  },
  rangeContainer: {
    display: "flex",
    alignItems: "center",
    padding: "5px",
    // "& #output": {
    //   background: "#1976d2",
    //   color: "#fff",
    //   borderRadius: "2px",
    //   padding: "3px 7px",
    //   margin: "0px 10px",
    //   textAlign: "center",
    //   position: "relative",
    //   fontSize: "16px",
    //   fontWeight: "400",
    //   "&:before": {
    //     content: '""',
    //     position: "absolute",
    //     left: "-12px",
    //     top: "50%",
    //     transform: "translateY(-50%)",
    //     height: 0,
    //     width: 0,
    //     border: "solid 6px #1976d2",
    //     zIndex: "1",
    //     borderTopColor: "white",
    //     borderBottomColor: "white",
    //     borderLeftColor: "white"
    //   }
    // }
},
  congrats: {
  backgroundColor: "#e2aa01",
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  position: "relative"
},
  hideTimePicker: {
    display: "none"
  }
}));

const StepOrderDetail = ({handleNextStep, activeStep, steps, valueOfField}) => {
  // Store types, packs, venues from Server
  const [types, setTypes] = useState();
  const [packs, setPacks] = useState();
  const [topics, setTopics] = useState();
  const [venues, setVenues] = useState();

  // Store current type and pack which user has been chosen
  const [currentPack, setCurrentPack] = useState();
  const [currentType, setCurrentType] = useState();

  const [AddressRadioSelected, setAddressRadioSelected] = useState('home');
  const [budgetRange, setBudgetRange] = useState(valueOfField[`budget`] ? valueOfField[`budget`] : "10000000");
  const [isOpenSpinner, setIsOpenSpinner] = useState(true);

  // Handle time value, time picker open and close state
  const [timePickedValue, setTimePickedValue] = useState(new Date());
  const [isTimePickerOpened, setIsTimePickerOpened] = useState(false);
  const [isDatePickerOpenFirstTime, setIsDatePickerOpenFirstTime] = useState(true);

  const orderType = JSON.parse(sessionStorage.getItem('type'));
  const orderPack = JSON.parse(sessionStorage.getItem('pack'));

  const stateSchema = {
    holdTime: {value: new Date(), error: ""},
    type: {value: "", error: ""},
    pack: {value: "", error: ""},
    topic: {value: "", error: ""},
    address: {value: "", error: ""},
    budget: {value: "", error: ""},
    venue: {value: "", error: ""},
    about: {value: "", error: ""},
    guestNum: {value: "", error: ""},
  }

  const stateValidatorSchema = {
    holdTime: {
      required: true
    },
    address: {
      required: true,
    },
  }

  useEffect(()=> {
    const sessionTypes = sessionStorage.getItem('types');
    const sessionPacks = sessionStorage.getItem('packs');
    const sessionVenues = sessionStorage.getItem('venues');
    const sessionTopics = sessionStorage.getItem('topics');

    if (orderPack) {
      stateSchema.type.value = orderType.id;
      stateSchema.pack.value = orderPack.id;
      // stateSchema.topic.value = orderTopic.id;
    }

    if(valueOfField.holdTime) {
      stateSchema.holdTime.value = valueOfField[`holdTime`] || new Date();
      stateSchema.type.value = valueOfField[`type`].id;
      stateSchema.pack.value = valueOfField[`pack`].id;
      stateSchema.topic.value = valueOfField[`topic`].id;
      stateSchema.venue.value = valueOfField[`venue`].id;
      stateSchema.address.value = valueOfField[`address`];
      stateSchema.about.value = valueOfField[`about`];
      stateSchema.guestNum.value = valueOfField[`guestNum`];
    }

    if (valueOfField[`address`]) {
      setAddressRadioSelected('home');
    } else {
      setAddressRadioSelected('venue');
    }

    if (!sessionTypes || !sessionPacks || !sessionVenues || !sessionTopics) {
      const fetchTypes = async () => {
        const result = (await packageApi.getAllType()).data;
        setTypes(result);
        sessionStorage.setItem('types', JSON.stringify(result));
      };
      const fetchPacks = async () => {
        const result = (await packageApi.getAllNoPagi()).data;
        setPacks(result)
        sessionStorage.setItem('packs', JSON.stringify(result));
        setIsOpenSpinner(false)
      };
      const fetchVenues = async () => {
        const result = (await packageApi.getAllEventVenue()).data;
        setVenues(result)
        sessionStorage.setItem('venues', JSON.stringify(result));
      };
      const fetchTopics = async () => {
        const result = (await packageApi.getAllTopic()).data;
        setTopics(result)
        sessionStorage.setItem('topics', JSON.stringify(result));
      };

      fetchTypes().then(r => r);
      fetchPacks().then(r => r);
      fetchVenues().then(r => r);
      fetchTopics().then(r => r);
    } else {
      setTypes(JSON.parse(sessionTypes));
      setPacks(JSON.parse(sessionPacks));
      setVenues(JSON.parse(sessionVenues));
      setTopics(JSON.parse(sessionTopics));
      setIsOpenSpinner(false)
    }
  }, []);

  const {values, errors, dirty, handleOnChange} = userForm(stateSchema, stateValidatorSchema);

  let {holdTime, type, pack, topic, address, budget, venue, about, guestNum} = values;

  useEffect(() => {
    if(valueOfField[`type`]) {
      setCurrentType(valueOfField[`type`]);
    }
    const selectedType = types && types.find(value => value.id === type);
    setCurrentType(selectedType);
    values.pack = '';
  }, [type]);

  useEffect(() => {
    if(valueOfField[`pack`]) {
      setCurrentPack(valueOfField[`pack`]);
    }
    const selectedPack = packs && packs.find(value => value.id === pack);
    setCurrentPack(selectedPack);
    if (selectedPack && selectedPack.id) {
      values.pack = selectedPack.id;
    }
  }, [pack]);

  const handleChangeAddressOption = (ev) => {
    setAddressRadioSelected(ev.target.value);
  };

  const formatCurrency = (str) => {
    return str.toLocaleString('vi', {style : 'currency', currency : 'VND'});
  }


  const classes = useStyles();

  // store time picker to state
  function handleTimePickerValue(e) {
    setTimePickedValue(e);
  }

  // Manual open or close time picker modal
  function handleOnClickTimePickerModal() {
    setIsTimePickerOpened(!isTimePickerOpened);
  }

  const onAcceptDatePickerThenOpenTimePicker = () => {
    if (isDatePickerOpenFirstTime) {
      setIsTimePickerOpened(true);
      setIsDatePickerOpenFirstTime(false);
    }
  };

  // manual store time picker value when close time picker modal on accept event
  const handleAcceptTimePicker = (e) => {
    setTimePickedValue(e);
    setIsTimePickerOpened(false);
  };

  // manual close time picker modal on cancel event
  const handleCancelTimePickerModal = () => {
    setIsTimePickerOpened(!isTimePickerOpened);
  };

  return (
    <div className={classes.root}>
      <Typography
        variant="h4"
        className={classes.title}>
        Thông tin đơn hàng
      </Typography>
      <Grid container spacing={2} >
        <Grid item sm={6}>
          <Paper className={`${classes.paper} ${classes.mainContainer}`}>
            <form className={classes.formContainer} action="#">
              {/*Select type*/}
              { types && (<FormControl variant="outlined" className={classes.formControl}>
                  <InputLabel id="type">Loại tiệc</InputLabel>
                  <Select
                    style={{ textAlign: 'left' }}
                    inputProps={{MenuProps: {disableScrollLock: true}}}
                    labelId="type"
                    id="type"
                    name="type"
                    value={type}
                    onChange={handleOnChange}
                    label="Loại tiệc"
                  >
                    {
                      types.map(type => <MenuItem key={type.id} value={type.id}>{type.name}</MenuItem>)
                    }
                  </Select>
                </FormControl>
              )}

              {/*Select pack*/}
              { packs && (<FormControl variant="outlined" className={classes.formControl}>
                  <InputLabel id="pack">Gói tiệc</InputLabel>
                  <Select
                    style={{ textAlign: 'left' }}
                    inputProps={
                      {MenuProps: {
                          disableScrollLock: true
                        },
                      }}
                    labelId="pack"
                    id="pack"
                    name="pack"
                    value={pack}
                    onChange={handleOnChange}
                    label="Gói tiệc"
                  >
                    {
                      packs.filter(p => p.type.id === type)
                        .map(pack =>
                          <MenuItem key={pack.id} value={pack.id}>
                            {pack.name}&nbsp;
                            <b>({pack.price_from ? `${formatCurrency(pack.price_from)} - ${formatCurrency(pack.price_to)}` : <></>})</b>
                          </MenuItem>)
                    }
                  </Select>
                </FormControl>
              )}

              { topics && (<FormControl variant="outlined" className={classes.formControl}>
                    <InputLabel id="pack">Chủ đề</InputLabel>
                    <Select
                        style={{ textAlign: 'left' }}
                        inputProps={
                          {MenuProps: {
                              disableScrollLock: true
                            },
                          }}
                        labelId="topic"
                        id="topic"
                        value={topic}
                        name="topic"
                        label="Chủ đề"
                        onChange={handleOnChange}
                    >
                      {
                        topics.map(top =>
                        <MenuItem key={top.id} value={top.id} className={classes.selectWithImg}>
                          {top.name}<img style={{height: '60px', marginLeft: '20px'}} src={top.images[0].url}/>
                        </MenuItem>)
                      }
                    </Select>
                  </FormControl>
              )}

              {/*Select address*/}
              <FormControl className={classes.formControl}>
                <FormLabel component="legend" className={classes.RadioFormLabel}>Địa điểm tổ chức</FormLabel>
                <RadioGroup row aria-label="position" name="position" defaultValue="top" onChange={handleChangeAddressOption} value={AddressRadioSelected}>
                  <FormControlLabel
                    value="home"
                    control={<Radio color="primary" />}
                    label="Địa điểm tùy chọn"
                    labelPlacement="end"
                  />
                  <FormControlLabel
                    value="venue"
                    control={<Radio color="primary" />}
                    label="Địa điểm gợi ý"
                    labelPlacement="end"
                  />
                </RadioGroup>
              </FormControl>
              {
                AddressRadioSelected === 'home'
                ?
                  (<>
                    <TextField
                    className={classes.inputField}
                    label="Địa chỉ tùy chọn"
                    variant="outlined"
                    name="address"
                    value={address}
                    onChange={handleOnChange}
                  />
                  {
                    errors.address && dirty.address && (
                      <Typography
                        className={classes.errorMsg}
                      >
                        {errors.address}
                      </Typography>
                    )
                  }</>)
                :
               ( <>{
                  venues && (<FormControl variant="outlined" className={classes.formControl}>
                  <InputLabel id="venue">Địa điểm tổ chức</InputLabel>
                  <Select
                    style={{textAlign: 'left'}}
                    inputProps={{MenuProps: {disableScrollLock: true}}}
                    labelId="venue"
                    id="venue"
                    name="venue"
                    value={venue}
                    onChange={handleOnChange}
                    label="Địa điểm tổ chức"
                    >
                    {
                      venues.map(venue => <MenuItem key={venue.id} value={venue.id}>{venue.name}</MenuItem>)
                    }
                  </Select>
                  </FormControl>)
                }</>)
              }

              {/*Select budget*/}
              <FormControl className={classes.formControl}>
                <FormLabel component="legend" className={classes.RadioFormLabel}>Ngân sách phù hợp:
                  <strong> {new Intl.NumberFormat('vi-VN', {style: 'currency', currency: 'VND'}).format(budgetRange)}</strong>
                </FormLabel>
                <div className={classes.rangeContainer}>
                  <input id="range" type="range"
                       value={budgetRange}
                       min="2000000"
                       max="20000000"
                       step="500000"
                       onChange={(e) => setBudgetRange(e.target.value)}
                />
              </div>
              </FormControl>

              {/*Select customers number*/}
              <TextField
                className={classes.inputField}
                label="Số lượng khách"
                variant="outlined"
                name="guestNum"
                value={guestNum}
                onKeyPress={event => {
                  if (!/[0-9]/.test(event.key)) {
                    event.preventDefault();
                  }
                }}
                onChange={handleOnChange}
              />
              {
                errors.guestNum && dirty.guestNum && (
                  <Typography
                    className={classes.errorMsg}
                  >
                    {errors.guestNum}
                  </Typography>
                )
              }

              {/*Select date*/}
              <FormControl variant="outlined" className={classes.formControl}>
                <MuiPickersUtilsProvider utils={DateFnsUtils} inputVariant="outlined">
                  <KeyboardDatePicker
                    inputVariant="outlined"
                    format="MM/dd/yyyy"
                    margin="normal"
                    id="date-picker-inline"
                    name="holdTime"
                    label="Thời gian tổ chức"
                    value={holdTime}
                    onChange={e => handleOnChange({target: {name: 'holdTime', value: e}})}
                    KeyboardButtonProps={{
                      'aria-label': 'change date',
                    }}
                    onAccept={e => onAcceptDatePickerThenOpenTimePicker()}
                  />
                  <KeyboardTimePicker
                    className={isDatePickerOpenFirstTime ? classes.hideTimePicker : ''}
                    inputVariant="outlined"
                    margin="normal"
                    id="time-picker"
                    label="Giờ tổ chức"
                    value={timePickedValue}
                    open={isTimePickerOpened}
                    onAccept={(e) => handleAcceptTimePicker(e)}
                    onChange={e => handleTimePickerValue(e)}
                    onClick={e => handleOnClickTimePickerModal()}
                    onClose={e => handleCancelTimePickerModal()}
                    KeyboardButtonProps={{
                      'aria-label': 'change time',
                    }} />
                </MuiPickersUtilsProvider>
              </FormControl>

            </form>
          </Paper>
        </Grid>
        <Grid item sm={6} display="flex">
          <Paper className={classes.paper}>
            <Grid container spacing={2}>
              <Grid item>
                <ButtonBase className={classes.image}>
                  <img className={classes.img} alt="service images" src={pack ? (currentPack ? currentPack && currentPack.images[0] && currentPack.images[0].url: valueOfField[`pack`] && valueOfField[`pack`].images[0] && valueOfField[`pack`].images[0].url) :  '/image-placeholder.png'} />
                </ButtonBase>
              </Grid>
              <Grid item xs={12} sm container>
                <Grid item xs container direction="column" spacing={2}>
                  <Grid item xs>
                    <Typography gutterBottom variant="subtitle1" align="left">
                      {
                        type
                        ?
                        (<><b>Tên loại: </b> {currentType ? currentType.name : valueOfField[`type`] && valueOfField[`type`].name}</>)
                        :
                        (<><b>Tên loại: </b> <em>Mời bạn chọn loại dịch vụ</em> </>)
                      }
                    </Typography>

                    <Typography gutterBottom variant="subtitle1" align="left">
                      {
                        pack
                          ?
                          (<><b>Tên gói :</b> {currentPack ? currentPack.name : valueOfField[`pack`] && valueOfField[`pack`].name}</>)
                          :
                          (<><b>Tên gói :</b> <em>Mời bạn chọn gói dịch vụ </em></>)
                      }
                    </Typography>

                    <Typography gutterBottom variant="subtitle1" align="left">
                      {
                        pack
                        ?
                          (currentPack
                            ?
                            (<><b>Khoảng giá: </b> {currentPack.price_from ? `${formatCurrency(currentPack.price_from)} - ${formatCurrency(currentPack.price_to)}` : <></>}</>)
                            :
                            (<><b>Khoảng giá: </b> {valueOfField[`pack`] && valueOfField[`pack`].price_from ? `${formatCurrency(valueOfField[`pack`].price_from)} - ${formatCurrency(valueOfField[`pack`].price_to)}` : <></>}</>)
                          )
                        : <></>
                      }
                    </Typography>
                    {/*<Typography gutterBottom variant="subtitle1" align="left">*/}
                    {/*  {*/}
                    {/*    pack*/}
                    {/*    ?*/}
                    {/*      (currentPack*/}
                    {/*        ?*/}
                    {/*        (<><b>Mô tả: </b> {currentPack.description}</>)*/}
                    {/*        :*/}
                    {/*        (<><b>Mô tả: </b> {valueOfField[`pack`] && valueOfField[`pack`].description}</>)*/}
                    {/*      )*/}
                    {/*    : <></>*/}
                    {/*  }*/}
                    {/*</Typography>*/}
                    {
                      pack
                      ?
                        (currentPack && currentPack.products.length !== 0
                          ?
                          (
                            <Typography gutterBottom variant="subtitle1" align="left">
                              <b>Các sản phẩm bao gồm: </b> {currentPack && currentPack.products.map(p => p.name).join(', ')}
                            </Typography>
                          )
                          : <></>
                        )
                      : <></>
                    }
                    {
                      pack
                        ?
                        (currentPack && currentPack.services.length !== 0
                          ?
                          (
                            <Typography gutterBottom variant="subtitle1" align="left">
                              <b>Các dịch vụ bao gồm: </b> {currentPack && currentPack.services.map(p => p.name).join(', ')}
                            </Typography>
                          )
                          : <></>
                        )
                        : <></>
                    }
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </Paper>
          <Paper className={classes.paper}>
            <Grid container spacing={2} style={{display: "flex", flexDirection: "column"}}>
                {/*About field*/}
                <Typography
                  variant="h6"
                  className={classes.title}>
                  Ghi chú thêm
                </Typography>
                <FormControl component="fieldset">
                  <TextareaAutosize
                    style={{fontSize: "14px", fontWeight: "400", height: "152px"}}
                    variant="outlined"
                    name="about"
                    value={about}
                    onChange={handleOnChange}
                    minRows={2}
                    aria-label="maximum height"
                    placeholder="Mời bạn nhập thêm ghi chú"
                  />
                </FormControl>
            </Grid>
          </Paper>
          <Paper className={`${classes.paper} ${classes.congrats}`}>
            <Grid container spacing={2} style={{display: "flex", flexDirection: "column"}}>
              {/*About field*/}
                <div className="firework-1"></div>
                <div className="firework-2"></div>
                <div className="firework-3"></div>
                <div className="firework-4"></div>
                <div className="firework-5"></div>
                <div className="firework-6"></div>
                <div className="firework-7"></div>
                <div className="firework-8"></div>
                <div className="firework-9"></div>
                <div className="firework-10"></div>
                <div className="firework-11"></div>
                <div className="firework-12"></div>
                <div className="firework-13"></div>
                <div className="firework-14"></div>
                <div className="firework-15"></div>
                <div className="title">Congratulations 🎉</div>
            </Grid>
          </Paper>
        </Grid>
      </Grid>
        {
          !type ||
          !pack ||
          !guestNum
            ?
            (<Button
              className={classes.disabledBtn}
              variant="contained"
              disabled
              endIcon={<BlockSharp />}
            >
              {activeStep ===steps.length ? "Confirm" : "Continue"}
            </Button>)
            :
            (<Button
              className={classes.btn}
              variant="contained"
              onClick={() => {
                values.pack = packs && packs.find(value => value.id === pack);
                values.type = types && types.find(value => value.id === type);
                values.topics = topics && topics.find(value => value.id === topic);
                if (AddressRadioSelected === 'home') {
                  values.venue = "";
                } else {
                  values.venue = venues && venues.find(value => value.id === venue);
                  values.address = "";
                }
                values.budget = budgetRange;
                const date = new Date(holdTime).toDateString();
                const time = new Date(timePickedValue).toTimeString();
                values.holdTime = new Date(`${date} ${time}`)
                handleNextStep(values)
              }}
              endIcon={<SendSharp />}
            >
              {activeStep ===steps.length ? "Confirm" : "Continue"}
            </Button>)
        }
        <div>
          <Backdrop className={classes.backdrop} open={isOpenSpinner}>
            <CircularProgress color="inherit" />
          </Backdrop>
        </div>
    </div>
  )
};

export default StepOrderDetail;
