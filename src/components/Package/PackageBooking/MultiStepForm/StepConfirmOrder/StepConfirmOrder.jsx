/* eslint-disable */
import React, {useEffect, useState} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import {Button, Typography,} from '@material-ui/core'
import {BlockSharp, SendSharp} from '@material-ui/icons'
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import {useHistory} from "react-router-dom";
import CircularProgress from '@material-ui/core/CircularProgress';
import projectApi from "../../../../../api/projectApi";
import OtpInput from "react-otp-input";
const useStyles = makeStyles({
  title: {color: "#999", textAlign: "center", margin: "20px 0"},
  tableComputer: {
    // ["@media (max-width:560px)"]: {
    //   display: "none",
    // },
  },
  mainContainer: {
    background: "lightgoldenrodyellow",
    padding: '15px',
    "& input[type='text'], & input[type='password']": {
      margin: '0!important',
      padding: '0!important',
      fontWeight: "500"
    },
    "& input[type='tel'], & input[type='number']": {
      padding: '0!important',
      fontWeight: "500"
    },
    justifyContent: "center",
    position: "relative",
    zIndex: "5",
  },
  formContainer: {
    position: "relative",
    height: "auto",
    padding: "2rem",
  },
  inputField: {
    width: "100%",
    marginBottom: "1.5rem"
  },
  btn: {
    width: "40%",
    height: "4rem",
    background: "pink",
    color: "#f1f1f1",
    "&:hover": {
      color: "red",
      opacity: "0.7",
      transition: ".3s ease-in-out"
    },
    margin: "30px 15px"
  },
  disabledBtn: {
    background: "rbga(0,0,0, 0.38)",
    width: "40%",
    height: "4rem",
    margin: "30px 15px",
    fontWeight: "500"
  },
  backBtn: {
    background: "orange",
    color: "#fff",
    width: "40%",
    height: "4rem",
    margin: "30px 15px",
    fontWeight: "500"
  },
  table: {
    marginBottom: " 0!important"
  },
  otp: {
    display: "flex",
    justifyContent: "center",
    "& input": {
      width: "2em!important",
      marginLeft: "15px"
    }
  }
})

const createData = (pack, type, price) => {
  return { pack, type, price};
}


const StepConfirmOrder = ({handleNextStep, activeStep, steps, handleBackStep, valueOfField}) => {

  const [OTP, setOTP] = useState("");
  const [OTPResponse, setOTPResponse] = useState(false);
  const [isRequestingOTP, setIsRequestingOTP] = useState(true);
  const history = useHistory();

  const formatCurrency = (str) => {
    if (!str) {return '';}
    return str.toLocaleString('vi', {style : 'currency', currency : 'VND'});
  }

  const cost = valueOfField[`pack`] && `${formatCurrency(((+valueOfField[`pack`].price_from + valueOfField[`pack`].price_to)/2))}`
  const rows = [
      createData(valueOfField[`pack`].name, valueOfField[`type`].name, cost),
  ];

  useEffect(() => {
    const timeoutID = window.setTimeout(() => {
      const sendOTPRequest = async () => {
        const result = (await projectApi.sendOtp(valueOfField['phone'], valueOfField['email'])).data;
        setOTPResponse(result);
        setIsRequestingOTP(false);

      }

      sendOTPRequest().then(r => r);
    }, 1000);
    return () => window.clearTimeout(timeoutID);
  }, [valueOfField])

  const classes = useStyles();

  return (
    <div className={classes.mainContainer}>
      <Typography
        variant="h4"
        className={classes.title}>
        Xác nhận lại đơn hàng
      </Typography>

      <TableContainer className={classes.tableComputer} component={Paper} style={{margin: "20px 0"}}>
        <Table className={classes.table} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell style={{fontSize: "16px", fontWeight: "700"}}>Gói dịch vụ</TableCell>
              <TableCell align="right" style={{fontSize: "16px", fontWeight: "700"}}>Loại dịch vụ</TableCell>
              <TableCell align="right" style={{fontSize: "16px", fontWeight: "700"}}>Ước tính giá trị</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map((row) => (
              <TableRow key={row.type}>
                <TableCell style={{fontSize: "14px"}} component="th" scope="row">{row.pack}</TableCell>
                <TableCell style={{fontSize: "14px"}} align="right">{row.type}</TableCell>
                <TableCell style={{fontSize: "14px"}} align="right">{row.price}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <div className={classes.formContainer}>
      {
        isRequestingOTP
          ?
          (<>
            <CircularProgress />
          </>)
          :
          (OTPResponse && OTPResponse.success
            ?
            (<>
              <Typography
                variant="h4"
                className={classes.title}>
                Mời nhập mã OTP xác thực
              </Typography>

              <div className={classes.otp}>
                <OtpInput
                  value={OTP}
                  onChange={setOTP}
                  numInputs={6}
                  isInputNum="true"
                />
              </div>
              {
                OTP.length < 6
                  ?
                  (<Button
                    className={classes.disabledBtn}
                    variant="contained"
                    disabled
                    endIcon={<BlockSharp/>}
                  >
                    {activeStep === steps.length ? "Confirm" : "Continue"}
                  </Button>)
                  :
                  (<Button
                    className={classes.btn}
                    variant="contained"
                    onClick={() => handleNextStep({OTPResult: OTPResponse, OTP})}
                    endIcon={<SendSharp/>}
                  >
                    {activeStep === steps.length ? "Confirm" : "Continue"}
                  </Button>)
              }
            </>)
            :
            (
              <Typography className={classes.instructions}>
                <p>Có lỗi xảy ra khi gửi OTP</p>
                Mời bạn liên hệ với số điện thoại 0961329400 để được hỗ trợ.
                <br/>
                <Button
                  className={classes.backBtn}
                  variant="contained"
                  onClick={() => history.push("/")}
                  endIcon={<SendSharp/>}
                >
                  Trang chủ
                </Button>
              </Typography>
            ))
      }
      </div>
    </div>
  )
};

export default StepConfirmOrder;
