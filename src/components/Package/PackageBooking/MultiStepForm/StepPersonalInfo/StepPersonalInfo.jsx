/* eslint-disable */
import React, {useEffect} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import {Button, TextField, Typography,} from '@material-ui/core'
import {BlockSharp, SendSharp} from '@material-ui/icons'

import userForm from "../../../../../hooks/useForm";

const useStyles = makeStyles({
  title: {color: "#999", textAlign: "center", marginTop: "20px"},
  mainContainer: {
    background: "lightgoldenrodyellow",
    "& input[type='text'], & input[type='password']": {
     margin: '0!important'
    },
    justifyContent: "center",
    position: "relative",
    zIndex: "5",
  },
  formContainer: {
    position: "relative",
    height: "auto",
    padding: "2rem",
  },
  inputField: {
    width: "100%",
    marginBottom: "1.5rem"
  },
  btn: {
    width: "40%",
    height: "4rem",
    background: "pink",
    color: "#f1f1f1",
    "&:hover": {
      color: "red",
      opacity: "0.7",
      transition: ".3s ease-in-out"
    },
    margin: "10px 24px"

  },
  disabledBtn: {
    background: "rbga(0,0,0, 0.38)",
    width: "40%",
    height: "4rem",
    margin: "10px 24px"
  },
  errorMsg: {
    marginTop: "0",
    color: "red",
    fontWeight: "200",
    marginBottom: "12px",
    textAlign: "left"
  },
  backBtn: {
    background: "orange",
    color: "#fff",
    width: "40%",
    height: "4rem",
    margin: "30px 15px",
    fontWeight: "500"
  }
})

const StepPersonalInfo = ({handleNextStep, activeStep, steps, handleBackStep, valueOfField}) => {

  const stateSchema = {
    cusName: {value: "", error: ""},
    phone: {value: "", error: ""},
    email: {value: "", error: ""},
  }

  useEffect(()=> {
    if(valueOfField) {
      stateSchema.cusName.value = valueOfField[`cusName`];
      stateSchema.phone.value = valueOfField[`phone`];
      stateSchema.email.value = valueOfField[`email`];
    }
  })

  const stateValidatorSchema = {
    cusName: {
      required: true
    },
    phone: {
      required: true,
      validator: {
        func: value =>  /^(0?)(3[2-9]|5[6|8|9]|7[0|6-9]|8[0-6|8|9]|9[0-4|6-9])[0-9]{7}$/.test(value),
        error: "Số điện thoại không hợp lệ!"
      }
    },
    email: {
      required: true,
      validator: {
        func: value =>  /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/.test(value),
        error: "Email không hợp lệ. Mời nhập lại"
      }
    },
  }

  const {values, errors, dirty, handleOnChange} = userForm(stateSchema, stateValidatorSchema);

  const {cusName, phone, email} = values;

  const classes = useStyles();


  return (
    <div className={classes.mainContainer}>
      <Typography
        variant="h4"
        className={classes.title}>
        Thông tin cá nhân
      </Typography>

      <div className={classes.formContainer}>
        <form action="#">

          <TextField
            className={classes.inputField}
            label="Tên"
            variant="outlined"
            name="cusName"
            value={cusName || ''}
            onChange={handleOnChange}
          />
          {
            errors.cusName && dirty.cusName && (
              <Typography
                className={classes.errorMsg}
              >
                {errors.cusName}
              </Typography>
            )
          }
          <TextField
            className={classes.inputField}
            label="Email"
            variant="outlined"
            name="email"
            value={email || ''}
            onChange={handleOnChange}
          />
          {
            errors.email && dirty.email && (
              <Typography
                className={classes.errorMsg}
              >
                {errors.email}
              </Typography>
            )
          }
          <TextField
            className={classes.inputField}
            label="Điện thoại"
            variant="outlined"
            name="phone"
            value={phone || ''}
            onChange={handleOnChange}
          />
          {
            errors.phone && dirty.phone && (
              <Typography
                className={classes.errorMsg}
              >
                {errors.phone}
              </Typography>
            )
          }
          <Button
            className={classes.backBtn}
            onClick={handleBackStep}
            variant="contained">
            Back
          </Button>
          {
              !phone ||
              !cusName ||
              !email
                ?
                (<Button
                  className={classes.disabledBtn}
                  variant="contained"
                  disabled
                  endIcon={<BlockSharp />}
                >
                  {activeStep ===steps.length ? "Confirm" : "Continue"}
                </Button>)
                :
                (<Button
                  className={classes.btn}
                  variant="contained"
                  onClick={() => handleNextStep(values)}
                  endIcon={<SendSharp />}
                >
                  {activeStep ===steps.length ? "Confirm" : "Continue"}
                </Button>)
          }
        </form>
      </div>
    </div>
  )
};

export default StepPersonalInfo;
