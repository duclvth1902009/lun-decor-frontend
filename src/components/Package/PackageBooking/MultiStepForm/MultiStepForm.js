/* eslint-disable */
import React, {useState} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import {
  Stepper,
  Step,
  StepLabel,
  Typography,
  StepConnector,
  Button,
  Backdrop,
  CircularProgress
} from '@material-ui/core';
import { useHistory } from "react-router-dom";

import StepPersonalInfo from "./StepPersonalInfo";
import StepOrderDetail from "./StepOrderDetail";
import StepConfirmOrder from "./StepConfirmOrder";
import {SendSharp} from "@material-ui/icons";
import packageApi from "../../../../api/packageApi";
import projectApi from "../../../../api/projectApi";

const useStyles = makeStyles({
  root: {
    ["@media (max-width:960px)"]: {
      width: "100%",
    },
    width: "85%",
    margin: "6rem auto",

    background: "white",
    fontSize: "2rem",
    "& .MuiStepIcon-root.MuiStepIcon-active": {
      color: "#FF9800"
    },
    "& .MuiStepIcon-root.MuiStepIcon-completed": {
      color: "green"
    },
    "& .MuiStepIcon-text": {
      fontSize: "1.25rem"
    },
    ".MuiStepper-root": {
      padding: "24px 0!important"
    }
  },
  instructions : {
    textAlign: "center",
    fontSize: "20px",
    fontWeight: "bold",
    marginTop: "2rem",
    background: "lightgoldenrodyellow",
    padding: '15px',
  },
  iconContainer: {
    transform: "scale(1.5)",
  },
  titleLabel: {
    ["@media (max-width:480px)"]: {
      display: "none",
    },
  },
  stepConnector : {
    top: "0!important"
  },
  backBtn: {
    background: "orange",
    fontSize: "14px",
    color: "#fff",
    width: "40%",
    height: "4rem",
    margin: "30px 15px",
    fontWeight: "500"
  },
  backdrop: {
    zIndex: 999,
    color: '#fff',
  },
})

const MultiStepForm = () => {

  const [activeStep, setActiveStep] = useState(0);

  const [infoObject, setInfoObject] = useState({});

  const [orderResponse, setOrderResponse] = useState(false);

  const [verifyOTPResponse, setVerifyOTPResponse] = useState(false);

  const [isVerifiedOTP, setIsVerifiedOTP] = useState(false);

  const getSteps = () => ["Thông tin đơn hàng", "Thông tin cá nhân", "Xác nhận đơn hàng"];

  const history = useHistory();

  const handleNextStep = (object) => {
    const info = Object.assign(infoObject, object);
    setInfoObject(info);
    // Send request for OTP confirm
    if(activeStep === 1) {
      handleOrder();
    }
    // Complete order
    if(activeStep === 2) {
      verifyOTP();
    }
    setActiveStep(prevActiveStep => prevActiveStep + 1)
  }

  const handleBackStep = () => {
    setActiveStep(prevActiveStep => prevActiveStep - 1);
  }

  const steps = getSteps();

  const getStepsContent = (stepIndex) => {
    switch (stepIndex) {
      case 0:
        return  (
          <StepOrderDetail
            handleNextStep={handleNextStep}
            activeStep={activeStep}
            steps={steps}
            valueOfField={infoObject}
          />
        );
      case 1:
        return  (
          <StepPersonalInfo
            handleNextStep={handleNextStep}
            handleBackStep={handleBackStep}
            activeStep={activeStep}
            steps={steps}
            valueOfField={infoObject}
          />
        );
      case 2:
        return  (
          <StepConfirmOrder
            handleNextStep={handleNextStep}
            handleBackStep={handleBackStep}
            activeStep={activeStep}
            steps={steps}
            valueOfField={infoObject}
          />
        );
      default:
        return (
          isVerifiedOTP
            ? (
                verifyOTPResponse
                  ?
                  (
                    <Typography className={classes.instructions}>
                      Bạn đã hoàn thành đặt hàng!
                      <br/>
                      <Button
                        className={classes.backBtn}
                        variant="contained"
                        onClick={() => history.push("/")}
                        endIcon={<SendSharp />}
                      >
                        Quay về trang chủ
                      </Button>
                    </Typography>
                  )
                  :
                  (
                    <Typography className={classes.instructions}>
                  <p>Không tìm thấy mã xác thực tương ứng! </p>
                  Mời bạn liên hệ với số điện thoại 0961329400 để được hỗ trợ.
                  <br/>
                  <Button
                    className={classes.backBtn}
                    variant="contained"
                    onClick={() => history.push("/")}
                    endIcon={<SendSharp />}
                  >
                    Quay về trang chủ
                  </Button>
                </Typography>
                  )
              )
            : ( <CircularProgress />)
            )
    }
  }

  const handleOrder = () => {
    const order = async () => {
      const payLoad = Object.assign({}, infoObject);
      if (!payLoad || !payLoad.address) {
        payLoad.address = payLoad.venue.address
      }
      delete payLoad.venue;
      payLoad.topics = [payLoad.topics]
      const orderResult = (await packageApi.createRequestOrder(payLoad)).data;
      setOrderResponse(orderResult);
      const info = Object.assign(infoObject, {orderResult: orderResult});
      setInfoObject(info);
    }
    order().then(r => r)
  }

  const verifyOTP = () => {
      const sendOrder = async  () => {
        const OTPResult = (await projectApi.checkRequestOrder((orderResponse && orderResponse.requestOrderId), infoObject[`OTPResult`].description, infoObject[`OTP`])).data;
        setVerifyOTPResponse(OTPResult.success);
        setIsVerifiedOTP(true);
      }
    sendOrder().then(r => r)
  }

  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Stepper
        activeStep={activeStep}
        alternativeLabel
        connector={
          <StepConnector
            classes={{
              root: classes.stepConnector
            }}
          />
        }>
        {steps.map(label => (
          <Step key={label}>
            <StepLabel className={classes.iconContainer}>
              <span className={classes.titleLabel}>
                {label}
              </span>
            </StepLabel>
          </Step>
        ))}
      </Stepper>
      <div className={classes.instructions}>
        {getStepsContent(activeStep)}
      </div>
    </div>
  );
}

export default MultiStepForm;
