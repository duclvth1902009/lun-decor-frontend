/* eslint-disable */
import React, { useEffect, useState } from 'react';
import {NavLink, useLocation } from 'react-router-dom';
// material ui core
import { Avatar } from "@material-ui/core";

// material ui icons
import StarIcon from "@material-ui/icons/Star";
import {
    FaTwitter,
    FaGooglePlusG,
    FaInstagram,
    FaFacebookF,
} from "react-icons/fa";

import "./PackageDetail.css";
import packageApi from "../../../api/packageApi";
import {backToTop, formatCurrency} from "../../../utils/utils";
import {ArrowLeftIcon} from "@material-ui/pickers/_shared/icons/ArrowLeftIcon";
import {ExpandLess, ExpandMore} from "@material-ui/icons";

function PackageDetail() {
    const [packObj, setPackObj] = useState({});
    const [packImages, setPackImages] = useState([]);
    const [products, setProducts] = useState([]);
    const [services, setServices] = useState([]);
    const [banner, setBanner] = useState();
    const [selectedStar, setSelectedStar] = useState(0);
    const [hoveredStar, setHoveredStar] = useState(0);
    const [isCollapse, setIsCollapse] = useState(false);

    useEffect(() => {
      const fetchPackage = async () => {
        try {
          const pack = (await packageApi.getById(id)).data;
          setPackObj(pack);
          setPackImages(pack.images.map(x => x.url));
          setBanner(packImages[0]);
          setProducts(pack.products);
          setServices(pack.services);
          backToTop()
        } catch (error) {
          console.log('Failed to fetch service package list', error);
        }
      }
      fetchPackage();
    }, [])

    useEffect(() => {
          setBanner(packImages[0]);
    }, [packImages])

    const route = useLocation().pathname;
    const id = route.substr(16, route.length)

    const renderExtraImage = () => {
        return packImages.map(src => (
          <div onClick={() => setBanner(src)}>
              <div className="product-image-bounder">
                  <img className="product-image" src={src} alt="product display" />
              </div>
          </div>
        ));
    };

    const handleOpen = () => {
        setIsCollapse(!isCollapse);
    };

    const handleSelectedStar = (pos) => {
      setSelectedStar(pos);
    };

    const handleHoveredStar = (pos) => {
      setHoveredStar(pos);
    };

    const savePackToSessionStorage = () => {
        sessionStorage.setItem('pack', JSON.stringify(packObj))
        sessionStorage.setItem('type', JSON.stringify(packObj.type))
    }

    return (
        <div>
            <section className="product product-fpt-with-stick-nav" itemScope itemType="http://schema.org/Product" style={{ marginTop: '2em' }}>
                <div className="container">
                    <div className="row">
                        <div className="col-lg-12 col-md-12 details-product">
                            <div className="row product-bottom">
                                <div className="clearfix padding-bottom-10">
                                    <div className="col-xs-12 col-sm-12 col-lg-12 col-md-12 details-pro">
                                        <div className="product-top clearfix">
                                            <h1 className="title-head">{packObj.name}</h1>
                                            <div className="sku-product">
                                                Mã sản phẩm: <span className="variant-sku" itemProp="sku" content="Đang cập nhật">{packObj.id}</span>
                                                <span className="hidden" itemProp="brand" itemScope itemType="https://schema.org/brand">Lun Decor</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="main product product-fpt-with-stick-nav">
                                        <div className="row">
                                            {/* Product Images */}
                                            <div className="product-image-wrapper col-md-6">
                                                <img
                                                  className="product-banner-image"
                                                  src={banner}
                                                  alt="product display"/>

                                                  <div className="list-product-images">
                                                      <div className="product-extra-images">
                                                          {renderExtraImage()}
                                                      </div>
                                                  </div>

                                            </div>
                                            {/* Product description */}
                                            <div className="product-description-wrapper col-md-6">
                                                <p className="product-name">Giá chỉ từ: {packObj && packObj.price_from && formatCurrency(packObj.price_from)}</p>
                                                <p className="product-price">
                                                <span className="product-discounted-price">
                                                   Giá thị trường:
                                                   <del>
                                                       {packObj && packObj.price_to && formatCurrency(packObj.price_to)}
                                                   </del>
                                                </span>
                                                <br/>
                                                <span className="product-save-price">Tiết kiệm:
                                                <span className="price product-price-save">{packObj && packObj.price_to && packObj.price_from && formatCurrency(packObj.price_to - packObj.price_from)}</span></span>
                                                </p>
                                                <div>
                                                    {/* Check if in stock */}
                                                    <div className="inventory_quantity">
                                                      <span className="stock-brand-title">
                                                          <strong><i className="ion ion-ios-checkmark-circle"/>Tình trạng:</strong>
                                                      </span>
                                                      <span className="a-stock a2"><link itemProp="availability" href="http://schema.org/InStock"/> Còn hàng</span>
                                                    </div>

                                                    {/* Product buy buttons */}
                                                    <button className="product-buy-buttons" onClick={savePackToSessionStorage}>
                                                        <NavLink to="/package-booking" className="txt-main">
                                                            <i className="fa fa-cart-arrow-down padding-right-10"/>
                                                            Đặt hàng ngay
                                                        </NavLink>
                                                    </button>

                                                    <div className="product-details">
                                                        <p className="title-product">Các hạng mục gồm có:</p>
                                                        <p className="content-product">{products && products.map(pro => (
                                                                <div className="row">
                                                                    <div className="col-xs-4">
                                                                        {pro.name}:
                                                                    </div>
                                                                    <div className="col-xs-8">
                                                                        {pro.description}
                                                                    </div>
                                                                    <hr/>
                                                                </div>
                                                        ))}</p>
                                                        <hr/>
                                                        <p className="title-product">Các dịch vụ bao gồm:</p>
                                                        <p className="content-product">{services && services.map(ser => (
                                                            <div className="row">
                                                                <div className="col-xs-4">
                                                                    {ser.name}:
                                                                </div>
                                                                <div className="col-xs-8">
                                                                    {ser.description}
                                                                </div>
                                                                <hr/>
                                                            </div>
                                                        ))}</p>
                                                    </div>

                                                    {/* Share links */}
                                                    <div className="product-link">
                                                        <div className="social-share col-sm-3">
                                                            <p>Chia sẻ </p>
                                                            <FaFacebookF className="social-icon" />
                                                            <FaGooglePlusG className="social-icon" />
                                                            <FaTwitter className="social-icon" />
                                                            <FaInstagram className="social-icon" />
                                                        </div>
                                                        <div className="call-and-payment col-sm-9">
                                                            <div className="hotline_product">
                                                                Gọi điện để được tư vấn: <a href="tel:0399002423" title="0399 002 423">0399 002 423</a>
                                                            </div>
                                                            <div className="payment_product">
                                                                <span className="payment_product_text">Chấp nhận thanh toán bằng: </span>
                                                                <img src="//bizweb.dktcdn.net/100/344/983/themes/704702/assets/payment-1.svg?1619100636914" alt="Lun Decor" />
                                                                <img src="//bizweb.dktcdn.net/100/344/983/themes/704702/assets/payment-2.svg?1619100636914" alt="Lun Decor" />
                                                            </div>
                                                        </div>

                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-xs-12 col-sm-12 col-lg-12 col-md-12">
                                                <div className={isCollapse? '': 'contentCollapsed'}>
                                                    <div id="package-description">
                                                        <div className="col-xs-12 col-sm-12 col-lg-12 col-md-12">
                                                            <div>
                                                                <h3>Chi tiết về gói dịch vụ</h3>
                                                            </div>
                                                        </div>
                                                        <div className="package-description">
                                                            <div dangerouslySetInnerHTML={{__html: packObj.description}}/>
                                                        </div>
                                                    </div>
                                                    <div className={isCollapse? 'collapsible' : 'collapsible collapsedContent'}>
                                                        <div id="listProduct">
                                                            {/* Product Images */}
                                                            <div className="col-xs-12 col-sm-12 col-lg-12 col-md-12">
                                                                <div>
                                                                    <h6>Chi tiết về các hạng mục</h6>
                                                                </div>
                                                            </div>
                                                            <div className="col-xs-12 col-sm-12 col-lg-12 col-md-12">
                                                                {products && products.map(pro => (
                                                                    <div className="row single-product">
                                                                        <div className="col-xs-4 text-bold">
                                                                            {pro.name}:
                                                                        </div>
                                                                        <div className="col-xs-8">
                                                                            {pro.description}
                                                                        </div>
                                                                        <br/>
                                                                        <div className="list-product-service-images col-lg-12 col-xs-12 col-md-12">
                                                                            {pro && pro.images && pro.images.map(img => (
                                                                                <div className="single-product-service-images">
                                                                                    <img className="product-service-image" src={img.url}/>
                                                                                </div>
                                                                            ))}
                                                                        </div>
                                                                        <br/>
                                                                    </div>
                                                                ))}
                                                            </div>
                                                        </div>
                                                        <div id="listService">
                                                            {/* Service Images */}
                                                            {services && services.length >0 && (<>
                                                                <div className="col-xs-12 col-sm-12 col-lg-12 col-md-12">
                                                                    <div>
                                                                        <h6>Các dịch vụ đi kèm</h6>
                                                                    </div>
                                                                </div>
                                                                <div className="package-description col-lg-12 col-xs-12 col-md-12">
                                                                    {services && services.map(ser => (
                                                                        <div className="row single-product">
                                                                            <div className="col-xs-4 text-bold">
                                                                                {ser.name}:
                                                                            </div>
                                                                            <div className="col-xs-8">
                                                                                {ser.description}
                                                                            </div>
                                                                            <div className="list-product-service-images col-lg-12 col-xs-12 col-md-12">
                                                                                {ser && ser.images && ser.images.map(img => (
                                                                                    <div className="single-product-service-images">
                                                                                        <img className="product-service-image" src={img.url}/>
                                                                                    </div>
                                                                                ))}
                                                                            </div>
                                                                            <hr/>
                                                                        </div>
                                                                    ))}
                                                                </div>
                                                            </>)}
                                                        </div>
                                                    </div>


                                                </div>
                                                {!isCollapse &&
                                                (
                                                    <div className="darker-bottom text-center">
                                                        <span onClick={handleOpen} className="btn--view-more"><ExpandMore style={{fontSize: "2.5em"}}/></span>
                                                    </div>
                                                )}

                                                {isCollapse &&
                                                (
                                                    <div className="text-center">
                                                        <span onClick={handleOpen}><ExpandLess style={{fontSize: "2.5em", marginTop: -50}}/></span>
                                                    </div>
                                                )}
                                            </div>


                                        </div>
                                        {/* Tabs */}
                                        <div className="detail-tab-user">
                                            <Avatar
                                                src=""
                                                className="detail-tab-user__avatar"
                                                alt="Avatar"
                                            />

                                            <form  className="detail-tab-user__form">
                                                <div className="detail-tab-user__row">
                                                    <div className="detail-tab-user__rate">
                                                        {Array(5)
                                                            .fill()
                                                            .map((_, index) => (
                                                                <StarIcon
                                                                    onClick={() => handleSelectedStar(index + 1)}
                                                                    onMouseOver={() => handleHoveredStar(index + 1)}
                                                                    onMouseLeave={() => handleHoveredStar(0)}
                                                                    style={{
                                                                        fill:
                                                                            index < (selectedStar || hoveredStar)
                                                                                ? "#fbb403"
                                                                                : "#FDDA81",
                                                                    }}
                                                                />
                                                            ))}
                                                    </div>
                                                    <span className="detail-tab-user__msg">(Mời chọn sao đánh giá)</span>
                                                </div>
                                                <textarea
                                                    className="detail-tab-user__textarea"
                                                    placeholder="Mời nhập nhận xét..."
                                                />
                                                <button className="product-buy-buttons" type="button">
                                                    <span className="txt-main">
                                                      Post comment
                                                    </span>
                                                </button>
                                            </form>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    )
}

export default PackageDetail;
