import React, { useEffect, useState } from 'react';
import {NavLink, useHistory, useLocation} from 'react-router-dom';
import packageApi from '../../api/packageApi';
import {formatCurrency} from "../../utils/utils";
import PaginationCustom from "../Commons/PaginationCustom";

function Project() {
    const [packageList, setPackageList] = useState([]);
    const [pageCurrent, setPageCurrent] = useState(1);
    const [totalItem, setTotalItem] = useState(0);
    const history = useHistory();

    useEffect(() => {
        const fetchPackageList = async () => {
            try {
                const rsp = await packageApi.getAll(pageCurrent-1);
                setPackageList(rsp.data);
                setTotalItem(parseInt(rsp.headers['x-total-count']))
                history.push(`?page=${pageCurrent}`)
            } catch (error) {
                console.log('Failed to fetch service package list', error);
            }
        }
        fetchPackageList();
    }, [history, pageCurrent])

    const pageParams = useLocation().search;

    useEffect(() => {
        const pagePick = new URLSearchParams(pageParams).get('page')
        console.log(history.location.pathname)
        if (history.location.pathname === '/package' && pagePick == null) setPageCurrent(1)
        else setPageCurrent(pagePick);
    }, [history.location.pathname, pageParams]);

    return (
        <div>
            <div className="container"  style={{marginTop:'2em'}}>
                <div className="row">
                    <section className="main_container collection col-md-9 col-md-push-3">
                        <h1 className="title-head margin-top-0">Tất cả sản phẩm</h1>
                        <div className="category-products products category-products-grids">
                            <div className="sort-cate clearfix margin-top-10 margin-bottom-10">
                                <div className="sort-cate-left hidden-xs">
                                    <h3>Xếp theo:</h3>
                                    <ul>
                                        <li className="btn-quick-sort alpha-asc">
                                            <NavLink to="#"  title="Tên A-Z"><i></i>Tên A-Z</NavLink>
                                        </li>
                                        <li className="btn-quick-sort alpha-desc">
                                            <NavLink to="#"  title="Tên Z-A"><i></i>Tên Z-A</NavLink>
                                        </li>
                                        <li className="btn-quick-sort position-desc">
                                            <NavLink to="#"  title="Hàng mới"><i></i>Hàng mới</NavLink>
                                        </li>
                                        <li className="btn-quick-sort price-asc">
                                            <NavLink to="#"  title="Giá thấp đến cao"><i></i>Giá thấp đến cao</NavLink>
                                        </li>
                                        <li className="btn-quick-sort price-desc">
                                            <NavLink to="#"  title="Giá cao xuống thấp"><i></i>Giá cao xuống thấp</NavLink>
                                        </li>
                                    </ul>
                                </div>
                                <div className="sort-cate-right-mobile hidden-lg hidden-md hidden-sm">
                                    <div id="sort-by">
                                        <label className="left">Xếp theo: </label>
                                        <ul>
                                            <li><span>Thứ tự</span>
                                                <ul>
                                                    <li><NavLink to="#"  title="Tên A-Z"><i></i>Tên A-Z</NavLink></li>
                                                    <li><NavLink to="#"  title="Tên Z-A"><i></i>Tên Z-A</NavLink></li>
                                                    <li><NavLink to="#"  title="Giá tăng dần">Giá tăng dần</NavLink></li>
                                                    <li><NavLink to="#"  title="Giá giảm dần">Giá giảm dần</NavLink></li>
                                                    <li><NavLink to="#"  title="Hàng mới">Hàng mới</NavLink></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <section className="products-view products-view-grid">
                                <div className="text-xs-right">
                                    {totalItem>0 && <PaginationCustom totalItem={totalItem} itemPerPage={12}/>}
                                </div>
                                <div className="row">
                                    {
                                        packageList.map(pack =>
                                            <div key={pack.id} className="col-xs-6 col-sm-4 col-md-3 col-lg-3">
                                                <div className="ant-single-product">
                                                    <div className="ant-single-product-image">
                                                        <NavLink to="#"><img src={pack.images[0].url} alt="" className="img-responsive center-block" /></NavLink>
                                                        <span className="discount-label discount-label--green">- 49% </span>
                                                        <form action="/cart/add" method="post" encType="multipart/form-data" className="hover-icons hidden-sm hidden-xs variants form-nut-grid form-ajaxtocart" data-id="product-actions-13604813">
                                                            <input type="hidden" name="variantId" defaultValue="22735316" />
                                                            <NavLink className="button ajax_addtocart add_to_cart" to="#" title="Mua ngay"></NavLink>
                                                            <NavLink className="add-to-cart quick-view quickview" to="#" data-handle={pack.name} title="Xem nhanh"></NavLink>
                                                        </form>
                                                    </div>
                                                    <div className="ant-product-border">
                                                        <h3 className="product-title"><NavLink to="#" title={pack.name} >{pack.name}</NavLink></h3>
                                                        <div className="product-price">
                                                            <span className="price">{formatCurrency(pack.price_from)}</span>
                                                            <span className="price-before-discount">{formatCurrency(pack.price_to)}</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        )

                                    }
                                </div>

                            </section>
                        </div>
                    </section>
                    <aside id="ant-sidebar" className="ant-sidebar sidebar  left-content col-md-3 col-md-pull-9">
                        <aside className="aside-item collection-category">
                            <div className="aside-title">
                                <h3 className="title-head margin-top-0"><span>Danh mục</span></h3>
                            </div>
                            <div className="aside-content">
                                <nav className="nav-category navbar-toggleable-md">
                                    <ul className="nav navbar-pills">
                                        <li className="nav-item">
                                            <NavLink to="#" className="nav-link" title=""><i className="fa fa-caret-right" aria-hidden="true"></i>Tiệc cưới</NavLink>
                                            <i className="fa fa-angle-down"></i>
                                            <ul className="dropdown-menu">
                                                <li className="nav-item ">
                                                    <NavLink className="nav-link" to="#" title="">Tiệc cưới 1</NavLink>
                                                </li>
                                            </ul>
                                        </li>
                                        <li className="nav-item ">
                                            <NavLink to="#" className="nav-link" title=""><i className="fa fa-caret-right" aria-hidden="true"></i>Tiệc sinh nhật</NavLink>
                                            <i className="fa fa-angle-down"></i>
                                            <ul className="dropdown-menu">
                                                <li className="nav-item ">
                                                    <NavLink className="nav-link" to="#" title="">Tiệc sinh nhật 1</NavLink>
                                                </li>
                                            </ul>
                                        </li>
                                        <li className="nav-item ">
                                            <NavLink to="#" className="nav-link" title=""><i className="fa fa-caret-right" aria-hidden="true"></i>Tiệc tân gia</NavLink>
                                            <i className="fa fa-angle-down"></i>
                                            <ul className="dropdown-menu">
                                                <li className="nav-item ">
                                                    <NavLink className="nav-link" to="#" title="">Tiệc tân gia 1</NavLink>
                                                </li>
                                            </ul>
                                        </li>
                                        <li className="nav-item ">
                                            <NavLink to="#" className="nav-link" title=""><i className="fa fa-caret-right" aria-hidden="true"></i>Tiệc tròn tuổi</NavLink>
                                            <i className="fa fa-angle-down"></i>
                                            <ul className="dropdown-menu">
                                                <li className="nav-item ">
                                                    <NavLink className="nav-link" to="#" title="">Tiệc tròn tuổi 1</NavLink>
                                                </li>
                                            </ul>
                                        </li>
                                        <li className="nav-item ">
                                            <NavLink to="#" className="nav-link" title=""><i className="fa fa-caret-right" aria-hidden="true"></i> Tiệc bạn bè</NavLink>
                                            <i className="fa fa-angle-down"></i>
                                            <ul className="dropdown-menu">
                                                <li className="nav-item ">
                                                    <NavLink className="nav-link" to="#" title="">Tiệc bạn bè</NavLink>
                                                </li>
                                            </ul>
                                        </li>
                                        <li className="nav-item ">
                                            <NavLink to="#" className="nav-link" title=""><i className="fa fa-caret-right" aria-hidden="true"></i> Tiệc khai trương</NavLink>
                                            <i className="fa fa-angle-down"></i>
                                            <ul className="dropdown-menu">
                                                <li className="nav-item ">
                                                    <NavLink className="nav-link" to="#" title=""> Tiệc khai trương</NavLink>
                                                </li>
                                            </ul>
                                        </li>
                                        <li className="nav-item ">
                                            <NavLink to="#" className="nav-link" title=""><i className="fa fa-caret-right" aria-hidden="true"></i> Dịch vụ khác</NavLink>
                                            <i className="fa fa-angle-down"></i>
                                            <ul className="dropdown-menu">
                                                <li className="nav-item ">
                                                    <NavLink className="nav-link" to="#" title=""> Dịch vụ khác</NavLink>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                        </aside>
                        <script src="//bizweb.dktcdn.net/100/344/983/themes/704702/assets/search_filter.js?1619100636914" type="text/javascript"></script>
                        <div className="aside-filter">
                            <div className="heading">
                                <h2 className="title-head">Bộ lọc</h2>
                            </div>
                            <div className="aside-hidden-mobile">
                                <div className="filter-container">
                                    <div className="filter-containers">
                                        <div className="filter-container__selected-filter" style={{ display: "none" }}>
                                            <div className="filter-container__selected-filter-header clearfix">
                                                <span className="filter-container__selected-filter-header-title">Bạn chọn</span>
                                                <NavLink to="#"  className="filter-container__clear-all" title="Bỏ hết">Bỏ hết <i className="fa fa-angle-right"></i></NavLink>
                                            </div>
                                            <div className="filter-container__selected-filter-list">
                                                <ul></ul>
                                            </div>
                                        </div>
                                    </div>
                                    <aside className="aside-item filter-vendor">
                                        <div className="aside-title">
                                            <h2 className="title-head margin-top-0"><span>Thương hiệu</span></h2>
                                        </div>
                                        <div className="aside-content filter-group">
                                            <div className="field-search input-group">
                                                <input type="text" placeholder="Tìm Thương hiệu" className="form-control filter-vendor-list" />
                                                <span className="input-group-btn">
                          <button className="btn btn-default"><i className="fa fa-search" aria-hidden="true"></i></button>
                        </span>
                                            </div>
                                            <ul className="filter-vendor">
                                                <li className="filter-item filter-item--check-box filter-item--green ">
                                                    <label data-filter="lun decor" htmlFor="filter-ant-green" className="ant-green">
                                                        <input type="checkbox" id="filter-ant-green" data-group="Hãng" data-field="vendor.filter_key" data-text="Lun Decor" defaultValue="(&#34;Lun Decor&#34;)" data-operator="OR" />
                                                        <i className="fa"></i>
                                                        Lun Decor
                                                    </label>
                                                </li>
                                            </ul>
                                        </div>
                                    </aside>
                                    <aside className="aside-item filter-price">
                                        <div className="aside-title">
                                            <h2 className="title-head margin-top-0"><span>Giá sản phẩm</span></h2>
                                        </div>
                                        <div className="aside-content filter-group">
                                            <ul>
                                                <li className="filter-item filter-item--check-box filter-item--green">
                          <span>
                            <label htmlFor="filter-duoi-100-000d">
                              <input type="checkbox" id="filter-duoi-100-000d" data-group="Khoảng giá" data-field="price_min" data-text="Dưới 5,000,000đ" defaultValue="(<5000000)" data-operator="OR" />
                              <i className="fa"></i>
                              Giá dưới 5,000,000đ
                            </label>
                          </span>
                                                </li>
                                                <li className="filter-item filter-item--check-box filter-item--green">
                          <span>
                            <label htmlFor="filter-100-000d-200-000d">
                              <input type="checkbox" id="filter-100-000d-200-000d" data-group="Khoảng giá" data-field="price_min" data-text="5,000,000đ - 8,000,000đ" defaultValue="(>5000000 AND <8000000)" data-operator="OR" />
                              <i className="fa"></i>
                              5,000,000đ - 8,000,000đ
                            </label>
                          </span>
                                                </li>
                                                <li className="filter-item filter-item--check-box filter-item--green">
                          <span>
                            <label htmlFor="filter-200-000d-300-000d">
                              <input type="checkbox" id="filter-200-000d-300-000d" data-group="Khoảng giá" data-field="price_min" data-text="8,000,000đ - 10,00,000đ" defaultValue="(>8000000 AND <10000000)" data-operator="OR" />
                              <i className="fa"></i>
                              8,000,000đ - 10,000,000đ
                            </label>
                          </span>
                                                </li>
                                                <li className="filter-item filter-item--check-box filter-item--green">
                          <span>
                            <label htmlFor="filter-tren1-000-000d">
                              <input type="checkbox" id="filter-tren1-000-000d" data-group="Khoảng giá" data-field="price_min" data-text="Trên 10,000,000đ" defaultValue="(>10000000)" data-operator="OR" />
                              <i className="fa"></i>
                              Giá trên 10,000,000đ
                            </label>
                          </span>
                                                </li>
                                            </ul>
                                        </div>
                                    </aside>
                                    <aside className="aside-item filter-type">
                                        <div className="aside-title">
                                            <h2 className="title-head margin-top-0"><span>Loại</span></h2>
                                        </div>
                                        <div className="aside-content filter-group">
                                            <div className="field-search input-group">
                                                <input type="text" placeholder="Tìm Loại" className="form-control filter-type-list" />
                                                <span className="input-group-btn">
                          <button className="btn btn-default"><i className="fa fa-search" aria-hidden="true"></i></button>
                        </span>
                                            </div>
                                            <ul className="filter-type">
                                                <li className="filter-item filter-item--check-box filter-item--green">
                                                    <label data-filter="tiệc sinh nhật" htmlFor="filter-cay-de-ban">
                                                        <input type="checkbox" id="filter-cay-de-ban" data-group="Loại" data-field="product_type.filter_key" data-text="Tiệc Sinh Nhật" defaultValue="(Tiệc Sinh Nhật)" data-operator="OR" />
                                                        <i className="fa"></i>
                                                        Tiệc Sinh Nhật
                                                    </label>
                                                </li>
                                                <li className="filter-item filter-item--check-box filter-item--green">
                                                    <label data-filter="tiệc cưới" htmlFor="filter-cay-canh-trong-nha">
                                                        <input type="checkbox" id="filter-cay-canh-trong-nha" data-group="Loại" data-field="product_type.filter_key" data-text="Tiệc Cưới" defaultValue="(Tiệc Cưới)" data-operator="OR" />
                                                        <i className="fa"></i>
                                                        Tiệc Cưới
                                                    </label>
                                                </li>
                                            </ul>
                                        </div>
                                    </aside>
                                    <aside className="aside-item filter-tag-style-1 tag-filtster">
                                        <div className="aside-title">
                                            <h2 className="title-head margin-top-0"><span>Quy mô</span></h2>
                                        </div>
                                        <div className="aside-content filter-group">
                                            <ul>
                                                <li className="filter-item filter-item--check-box filter-item--green">
                          <span>
                            <label htmlFor="filter-lon">
                              <input type="checkbox" id="filter-lon" data-group="tag2" data-field="tags" data-text="Lớn" defaultValue="(Lớn)" data-operator="OR" />
                              <i className="fa"></i>
                              Khách sạn
                            </label>
                          </span>
                                                </li>
                                                <li className="filter-item filter-item--check-box filter-item--green">
                          <span>
                            <label htmlFor="filter-nho">
                              <input type="checkbox" id="filter-nho" data-group="tag2" data-field="tags" data-text="Nhỏ" defaultValue="(Nhỏ)" data-operator="OR" />
                              <i className="fa"></i>
                              Nhà hàng
                            </label>
                          </span>
                                                </li>
                                                <li className="filter-item filter-item--check-box filter-item--green">
                          <span>
                            <label htmlFor="filter-vua">
                              <input type="checkbox" id="filter-vua" data-group="tag2" data-field="tags" data-text="Vừa" defaultValue="(Vừa)" data-operator="OR" />
                              <i className="fa"></i>
                              Resort
                            </label>
                          </span>
                                                </li>
                                            </ul>
                                        </div>
                                    </aside>
                                </div>
                            </div>
                        </div>
                    </aside>
                    <div id="open-filters" onClick={onmouseenter} className="open-filters hidden-lg hidden-md">
                        <i className="fa fa-filter" aria-hidden="true"></i>
                    </div>
                    <div className="col-md-12">
                        <div className="collections-top-sale">
                            <div className="section_flash_sale">
                                <div className="flash-sale-title margin-bottom-30">
                                    <a href="san-pham-moi" title="Top Bán Chạy">Top Bán Chạy</a>
                                </div>
                                <div className="section-tour-owl2 products products-view-grid owl-carousel not-dqowl owl-loaded owl-drag">
                                    <div className="owl-stage-outer">
                                        <div className="owl-stage">
                                            <div className="owl-item active col-sm-3 col-xs-6">
                                                <div className="item">
                                                    <div className="ant-single-product">
                                                        <div className="ant-single-product-image">
                                                            <NavLink to="#"><img src="//bizweb.dktcdn.net/100/344/983/themes/704702/assets/loader.svg?1619100636914" alt="" className="img-responsive center-block" /></NavLink>
                                                            <form action="/cart/add" method="post" encType="multipart/form-data" className="hover-icons hidden-sm hidden-xs variants form-nut-grid form-ajaxtocart" data-id="product-actions-13604778">
                                                                <input type="hidden" name="variantId" defaultValue="22735280" />
                                                                <NavLink className="button ajax_addtocart add_to_cart" to="#" title="Mua ngay"></NavLink>
                                                                <NavLink className="add-to-cart quick-view quickview" to="#" data-handle="" title="Xem nhanh"></NavLink>
                                                            </form>
                                                        </div>
                                                        <div className="ant-product-border">
                                                            <h3 className="product-title"><NavLink to="#" title="">Gói Sinh Nhật Hồng</NavLink></h3>
                                                            <div className="product-price">
                                                                <span className="price">160.000₫</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Project;
