/* eslint-disable */
import React, {useState} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import {Button, TextField, Typography,} from '@material-ui/core'
import {BlockSharp, SendSharp} from '@material-ui/icons'
import {useHistory} from "react-router-dom";
import userForm from "../../../hooks/useForm";
import FormControl from "@material-ui/core/FormControl";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Radio from "@material-ui/core/Radio";
import projectApi from "../../../api/projectApi";
import OtpInput from "react-otp-input";

const useStyles = makeStyles({
  title: {color: "#999", textAlign: "center", margin: "20px 0"},
  mainContainer: {
    background: "lightgoldenrodyellow",
    padding: '15px',
    "& input[type='text'], & input[type='password']": {
      margin: '0!important',
      fontWeight: "500"
    },
    "& input[type='tel'], & input[type='number']": {
      padding: '0!important',
      fontWeight: "500"
    },
    justifyContent: "center",
    position: "relative",
    zIndex: "5",
  },
  formContainer: {
    position: "relative",
    height: "auto",
    padding: "2rem",
  },
  inputField: {
    width: "40%",
    margin: "0 auto 1.5rem"
  },
  btn: {
    width: "40%",
    height: "4rem",
    background: "pink",
    color: "#f1f1f1",
    "&:hover": {
      color: "red",
      opacity: "0.7",
      transition: ".3s ease-in-out"
    },
    margin: "15px",
    textAlign: "center"
  },
  disabledBtn: {
    background: "rbga(0,0,0, 0.38)",
    width: "40%",
    height: "4rem",
    margin: "15px",
    fontWeight: "500",
    textAlign: "center"
  },
  errorMsg: {
    marginTop: "0",
    color: "red",
    fontWeight: "200",
    marginBottom: "12px",
    textAlign: "center"
  },
  RadioFormLabel: {
    textAlign: "left",
    margin: "10px 0"
  },
  formControl: {
    margin: "10px 0",
    width: "100%",
  },
  otp: {
    display: "flex",
    justifyContent: "center",
    "& input": {
      width: "2.5em!important",
      marginRight: "15px"
    }
  }
})
export const AuthTrackingRequest = () => {
  const [OTP, setOTP] = useState("");
  const [emailOTPResponse, setEmailOTPResponse] = useState(false);
  const [OTPVerifiedResponse, setOTPVerifiedResponse] = useState();
  const [isRequestingOTP, setIsRequestingOTP] = useState(false);
  const [OTPMethodRadioSelected, setOTPMethodRadioSelected] = useState('email');
  const history = useHistory();

  const stateSchema = {
    email: {value: "", error: ""},
    phone: {value: "", error: ""},
  }

  const stateValidatorSchema = {
    email: {
      required: true,
      validator: {
        func: value =>  /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/.test(value),
        error: "Email không hợp lệ. Mời nhập lại"
      }
    },
    phone: {
      required: true,
      validator: {
        func: value =>  /^(0?)(3[2-9]|5[6|8|9]|7[0|6-9]|8[0-6|8|9]|9[0-4|6-9])[0-9]{7}$/.test(value),
        error: "Số điện thoại không hợp lệ!"
      }
    },
  }


  const sendEmailOTPRequest = async () => {
    const result = (await projectApi.sendOtp(phone, email)).data;
    setEmailOTPResponse(result);
  }

  const verifyOTP = async () => {
        setIsRequestingOTP(true)
        const result = (await projectApi.verifyOtp(phone, email, OTP, emailOTPResponse.description)).data
        setOTPVerifiedResponse(result)
        setIsRequestingOTP(false)
        if (Array.isArray(result) && result.length > 0) {
          sessionStorage.setItem('tracking-list', JSON.stringify(result));
          history.push("/tracking-orders");
        }
      }

  const handleChange = (ev) => {
    setOTPMethodRadioSelected(ev.target.value);
  };

  const {values, errors, dirty, handleOnChange} = userForm(stateSchema, stateValidatorSchema);

  const {email, phone} = values;

  const classes = useStyles();

  return (

      <div className="container">

        <div className={classes.mainContainer}>
          {
            emailOTPResponse && emailOTPResponse.success
                ?
                (<></>)
                :
                (<div>
                  <Typography
                      variant="h4"
                      className={classes.title}>
                    Xác thực theo dõi đơn hàng
                  </Typography>
                  <Typography
                      variant="h6"
                      className={classes.title}>
                    Bạn hãy nhập email bạn đã đăng kí để nhận OTP đăng nhập
                  </Typography>
                  {/*Select method*/}
                  <FormControl className={`${classes.formControl}`}>
                    <RadioGroup row aria-label="position"
                                name="position"
                                defaultValue="top"
                                onChange={handleChange}
                                style={{justifyContent: "center"}}
                                value={OTPMethodRadioSelected}>
                      <FormControlLabel
                          value="email"
                          control={<Radio color="primary"/>}
                          label="Email"
                          labelPlacement="end"
                      />
                      <FormControlLabel
                          value="phone"
                          control={<Radio color="primary"/>}
                          label="Số điện thoại"
                          labelPlacement="end"
                      />
                    </RadioGroup>
                  </FormControl>
                    {
                      typeof emailOTPResponse === "boolean"
                          ?
                          <></>
                          :
                          <Typography
                              variant="h6"
                              className={classes.errorMsg}>
                            {emailOTPResponse.description} <span>Mời bạn liên hệ quản trị viên để được trợ giúp!</span>
                          </Typography>
                    }
                  <div style={{textAlign: "center"}}>
                    {
                      OTPMethodRadioSelected === 'email'
                          ?
                          (<>
                            <TextField
                                className={classes.inputField}
                                label="Email"
                                variant="outlined"
                                name="email"
                                value={email}
                                onChange={handleOnChange}
                            />
                            {
                              errors.email && dirty.email && (
                                  <Typography
                                      className={classes.errorMsg}
                                  >
                                    {errors.email}
                                  </Typography>
                              )
                            }</>)
                          :
                          (<>
                            <TextField
                                className={classes.inputField}
                                label="Số điện thoại"
                                variant="outlined"
                                name="phone"
                                value={phone}
                                onChange={handleOnChange}
                            />
                            {
                              errors.phone && dirty.phone && (
                                  <Typography
                                      className={classes.errorMsg}
                                  >
                                    {errors.phone}
                                  </Typography>
                              )
                            }</>)
                    }
                  </div>
                  <div style={{textAlign: "center"}}>
                    {
                      ((OTPMethodRadioSelected === 'email' && !email) && (errors.email && dirty.email)) ||
                      ((OTPMethodRadioSelected === 'phone' && !phone) && (errors.phone && dirty.phone))
                          ?
                          (<Button
                              className={classes.disabledBtn}
                              variant="contained"
                              disabled
                              endIcon={<BlockSharp/>}
                          >
                            {OTPMethodRadioSelected === 'email' ? <>Gửi email</> : <>Gửi OTP</>}
                          </Button>)
                          :
                          (<Button
                              className={classes.btn}
                              variant="contained"
                              endIcon={<SendSharp/>}
                              onClick={sendEmailOTPRequest}
                          >
                            {OTPMethodRadioSelected === 'email' ? <>Gửi email</> : <>Gửi OTP</>}
                          </Button>)
                    }
                  </div>
                </div>)
          }
          {
            emailOTPResponse && emailOTPResponse.success
              ?
                  (<>
                    {
                      OTPVerifiedResponse === undefined
                      ?
                        (<>
                          <Typography
                              variant="h4"
                              className={classes.title}>
                            Mời nhập mã OTP xác thực
                          </Typography>

                          <div className={classes.otp}>
                            <OtpInput
                              value={OTP}
                              onChange={setOTP}
                              numInputs={6}
                              isInputNum="true"
                              isDisabled={isRequestingOTP}
                            />
                          </div>

                          <div style={{textAlign: "center"}}>
                            {
                              isRequestingOTP || OTP.length < 6
                                  ?
                                  (<Button
                                      className={classes.disabledBtn}
                                      variant="contained"
                                      disabled
                                      endIcon={<BlockSharp/>}
                                  >
                                    Tiếp tục
                                  </Button>)
                                  :
                                  (<Button
                                      className={classes.btn}
                                      variant="contained"
                                      onClick={verifyOTP}
                                      endIcon={<SendSharp/>}
                                  >
                                    Tiếp tục
                                  </Button>)
                            }
                          </div>
                        </>)
                      : (
                            <Typography
                                variant="h4"
                                className={classes.title}>
                              Đã có lỗi xảy ra, bạn hãy refresh trang để xác thực lại.
                            </Typography>
                        )
                    }
                  </>)
              :
              (<></>)
          }
      </div>
    </div>
  )
}


export default AuthTrackingRequest;
