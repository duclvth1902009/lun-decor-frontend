/* eslint-disable */
import React, {useEffect, useState} from "react";
import "react-vertical-timeline-component/style.min.css";
import Typography from '@material-ui/core/Typography';
import {makeStyles} from '@material-ui/core/styles';
import './ProjectTrack.css';
import DoneIcon from "@material-ui/icons/Done";
import BuildIcon from "@material-ui/icons/Build";
import ScheduleIcon from "@material-ui/icons/Schedule";

const useStyles = makeStyles((theme) => ({
    title: {color: "#999", textAlign: "center", margin: "20px 0"},
    avatarImg: {height: "100px"},
    eventAvt: {
        border: "1px solid #ddd",
        borderRadius: "4px",
        padding: "5px",
        width: "250px"
    }
}));

export const TrackingProjects = () => {

    const [customer, setCustomer] = useState();
    const [listProjectResponse, setListProjectResponse] = useState();
    const [isRequestedTracks, setIsRequestedTracks] = useState(false);
    const currentTime = new Date().toLocaleString("vi-VN", {timeZone: "Asia/Jakarta"}).split(',')[1]

    const icon = (stt) => {
        switch (stt) {
            case 'done':
                return (<><DoneIcon style={{fontSize: "2.5em"}}/><br/>Đã hoàn thiện</>);
            case 'pending':
                return (<><ScheduleIcon style={{fontSize: "2.5em"}}/><br/>Đang chờ</>);
            case 'active':
                return (<><BuildIcon style={{fontSize: "2.5em"}}/><br/>Đang thực hiện</>);
            default:
                return (<></>)
        }
    }

    useEffect(() => {
        const trackingList = JSON.parse(sessionStorage.getItem('tracking-list'));
        if (Array.isArray(trackingList)) {
            setListProjectResponse(trackingList)
            setCustomer(trackingList[0].customer)
            setIsRequestedTracks(true);
        }
    }, [])

    const formatCurrency = (str) => {
        if (str === null || str === 0 || str === '0'){
            return '0₫'
        }
        return str.toLocaleString('vi', {style : 'currency', currency : 'VND'});
    }

    const classes = useStyles();

    const formatDate = (dateStr) => {
        return new Date(dateStr).toLocaleString("vi-VN", {timeZone: "Asia/Jakarta"}).split(',')[1]
    }

    const formatHour = (dateStr) => {
        const hour = new Date(dateStr).toLocaleString("vi-VN", {timeZone: "Asia/Jakarta"}).split(',')[0].split(':')
        return hour[0] + ' : ' + hour[1]
    }




    const handleItemClick = (id) => {
        const track = listProjectResponse.find(track =>  track.id === id);
    }

    const BindingProjectData = (project) => (
        <>{
            project
            ?
            (<div className="post-feed-item" onClick={() => handleItemClick(project.id)}>
                <div className="invoice-description">
                    <div className="description-table">
                        <table id="table-content">
                            <thead className="tb-content-header">
                            <tr>
                                <th scope="col">Trạng thái</th>
                                <th scope="col">Tên đơn hàng</th>
                                <th scope="col">Thời gian tổ chức</th>
                                <th scope="col">Địa điểm tổ chức</th>
                                <th scope="col">Đơn giá</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <th scope="col" style={{textAlign: "center"}}>{icon(project.status)}</th>
                                <td scope="col" style={{textAlign: "center"}}>
                                    {project.images.length > 0
                                        ? <img src={project && project.images[0] && project.images[0].url} alt="" className={classes.eventAvt} />
                                        : <img src="https://drive.google.com/uc?export=view&id=14fxBR27qIkZl0FqStLcYvkKOMGgYGS3l" alt="" className={classes.eventAvt} />
                                    }
                                    {
                                        project && project.name
                                        ?
                                        <div className="event-name">{project && project.name}</div>
                                        :
                                        (
                                          <div>
                                            <span className="event-name">{project && project.type && project.type.name}</span>
                                            <span> - </span>
                                            <span className="event-name">{project && project.requestOrder && project.requestOrder.pack && project.requestOrder.pack.name}</span>
                                          </div>)
                                    }
                                </td>
                                <td scope="col">
                                    <div className="event-hour">{formatHour(project && project.holdTime)}</div>
                                    <div className="event-date">{formatDate(project && project.holdTime)}</div>
                                </td>
                                <td scope="col">
                                    <div className="event-location">Địa chỉ:</div>
                                    <div className="event-address">{project && project.location}{project.finTotal}</div>
                                </td>
                                <td scope="col"><div className="event-cost">{formatCurrency(project.finTotal - 0)}</div></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div className="calc-cost">
                        <div className="calc-title">
                            <div>Phí dự kiến</div>
                            <div>Phí thực tế</div>
                            <div>Giảm giá</div>
                            <div className="purchase-title">Thanh toán</div>
                        </div>

                        <div className="calc-data">
                            <div>{formatCurrency(project && project.preTotal)}</div>
                            <div>{formatCurrency(project && project.finTotal)}</div>
                            <div>{formatCurrency(project && project.requestOrder.discount)}</div>
                            <div className="purchase-data">{formatCurrency(project.finTotal - 0)}</div>
                        </div>
                    </div>
                    <div className="btns">
                        <button className="invoice-btn">Edit</button>
                        <button className="invoice-btn">Print/PDF</button>
                        <button className="invoice-btn">Purchase</button>
                    </div>
                </div>
            </div>)
            :
            <></>
        }</>
    )

    const BindingCustomerData = (customer) => (
        <>{
            customer ? (
                <div className="post-feed-item">
                        <div className="card mb-3">
                            <div className="row g-0">
                                <div className="col-sm-2">
                                </div>
                                <div className="col-sm-2">
                                    <img
                                        src="https://www.pngitem.com/pimgs/m/421-4212617_person-placeholder-image-transparent-hd-png-download.png"
                                        alt="..."
                                        className=" avatarImg"
                                    />
                                </div>
                                <div className="col-sm-6">
                                    <div className="card-body">
                                        <p className="card-text">Tên khách hàng: <strong>{customer.name}</strong>
                                        </p>
                                        <p className="card-text">Số điện thoại khách
                                            hàng: <strong>{customer.phone}</strong></p>
                                        <p className="card-text">Email khách
                                            hàng: <strong>{customer.email}</strong></p>
                                        <p className="card-text">Số đơn hàng đã đặt: <strong>{listProjectResponse.length}</strong></p>
                                        <hr/>
                                    </div>
                                </div>
                                <div className="col-sm-2">
                                </div>
                            </div>
                        </div>
                    </div>
            ) : <></>
        }</>
    )


    const ListTracks = (
        <>
            <Typography
                variant="h4"
                className={classes.title}>
                CÁC ĐƠN HÀNG CỦA BẠN
            </Typography>
            {
                Array.isArray(listProjectResponse) && listProjectResponse.length > 0
                    ?
                    (<>
                        {
                        listProjectResponse.map(project => (
                            <div className="container">
                                {
                                    BindingProjectData(project)
                                }
                            </div>
                        ))
                        }
                    </>)
                    :
                    <></>
            }
        </>
        )

    return (
        <div style={{ padding: "20px", backgroundColor: "#EEEEEE"}}>
            <div>
                <Typography
                    variant="h4"
                    className={classes.title}>
                    THÔNG TIN KHÁCH HÀNG
                </Typography>
                { BindingCustomerData(customer) }
            </div>

            {ListTracks}
        </div>

    );
}

export default TrackingProjects;
