/* eslint-disable */
import React, {useEffect, useState} from 'react';
import {makeStyles, useTheme} from '@material-ui/core/styles';
import MobileStepper from '@material-ui/core/MobileStepper';
import Button from '@material-ui/core/Button';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import SwipeableViews from 'react-swipeable-views';
import {autoPlay} from 'react-swipeable-views-utils';
import {NavLink} from "react-router-dom";
import {formatCurrency} from "../../../utils/utils";
const useStyles = makeStyles((theme) => ({

}));

export const SinglePackage = (props) => {
    const classes = useStyles();
    const theme = useTheme();
    const [pack] = useState(props.pack)

    const savePackToSessionStorage = () => {
        sessionStorage.setItem('pack', JSON.stringify(pack))
        sessionStorage.setItem('type', JSON.stringify(pack.type))
    }


    return (
        <div className="owl-item active col-xs-6 col-md-3 col-sm-6 col-lg-3">
            <div className="ant-single-product">
                <div className="ant-single-product-image">
                    <NavLink to={"/package-detail/" + pack.id}><img src={pack ? pack && pack.images[0] && pack.images[0].url : '/image-placeholder.png'} className="img-responsive center-block"  alt="picture"/></NavLink>
                    <span className="discount-label discount-label--green">- 10% </span>
                    <form className="hover-icons hidden-sm hidden-xs variants form-nut-grid form-ajaxtocart" data-id="product-actions-13604813">
                        <input type="hidden"/>
                        <NavLink className="button ajax_addtocart add_to_cart" to="/package-booking" title="Đặt tiệc ngay" onClick={savePackToSessionStorage(pack)}/>
                        <NavLink className="add-to-cart quick-view quickview" to="#" data-handle={pack.name} title="Xem nhanh"/>
                    </form>
                </div>
                <div className="ant-product-border">
                    <h3 className="product-title"><NavLink to="/package-booking" title={pack.name}>{pack.name}</NavLink></h3>
                    <div className="product-price">
                        <span className="price">{formatCurrency(pack.price_from)} </span>
                        <span className="price-before-discount">{formatCurrency(pack.price_to)} </span>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default SinglePackage;
