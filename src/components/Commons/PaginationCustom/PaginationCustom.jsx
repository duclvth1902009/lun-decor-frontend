/* eslint-disable */
import React, {useEffect, useState} from 'react';
import {makeStyles, useTheme} from '@material-ui/core/styles';
import MobileStepper from '@material-ui/core/MobileStepper';
import Button from '@material-ui/core/Button';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import SwipeableViews from 'react-swipeable-views';
import {autoPlay} from 'react-swipeable-views-utils';
import {NavLink, useHistory, useLocation} from "react-router-dom";

const useStyles = makeStyles((theme) => ({
}));

export const PaginationCustom = (props) => {
    const [pageCurrent, setPageCurrent] = useState(1);
    const totalPage= Math.ceil(props.totalItem/props.itemPerPage);
    let count = 1;

    const pageParams = useLocation().search;


    useEffect(() => {
        document.getElementById('nextPageBtn').classList.remove('disabled');
        document.getElementById('backPageBtn').classList.remove('disabled');
        let numBtnCr = document.getElementById(`pageBtn${pageCurrent}`);
        if (numBtnCr != null){
            numBtnCr.classList.remove('disabled');
            numBtnCr.classList.remove('active')
        }
        const pagePick = new URLSearchParams(pageParams).get('page')
        let numBtn = document.getElementById(`pageBtn${pagePick}`);
        if (numBtn != null){
            numBtn.classList.add('disabled');
        }
        if (pagePick==1) {
            document.getElementById('backPageBtn').classList.add('disabled');
        } else if (pagePick==totalPage) {
            document.getElementById('nextPageBtn').classList.add('disabled');
        }
        setPageCurrent(pagePick)
    }, [pageParams]);
    return (
        <nav className="text-center">
            <ul className="pagination clearfix">
                <li id="backPageBtn" className={pageCurrent == 1 ? 'page-item disabled' : 'page-item'}><NavLink className="page-link" to={`/package?page=1`} title="«">«</NavLink></li>
                {[...Array(totalPage)].map(pg => <li key={count} id={`pageBtn${count}`} className={pageCurrent == count ? 'active page-item disabled' : 'page-item'}><NavLink className="page-link" to={`/package?page=${count}`} title={count}>{count++}</NavLink></li>
                )}
                <li id="nextPageBtn" className={pageCurrent == totalPage ? 'page-item disabled' : 'page-item'}><NavLink className="page-link" to={`/package?page=${totalPage}`} title="»">»</NavLink></li>
            </ul>
        </nav>
    );
}

export default PaginationCustom;
