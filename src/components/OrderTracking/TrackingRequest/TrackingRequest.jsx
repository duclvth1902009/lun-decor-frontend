/* eslint-disable */
import React, { useEffect, useState } from "react";
import { VerticalTimeline } from "react-vertical-timeline-component";
import "react-vertical-timeline-component/style.min.css";
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import TimeLineItem from "./TimeLineElement";
import { CircularProgress } from "@material-ui/core";
import "./TrackingRequest.css"
import queryString from 'query-string'
import { useLocation } from 'react-router-dom'
import tracksProgressApi from "../../../api/trackProgressApi";
import { textAlign } from "@material-ui/system";

const useStyles = makeStyles((theme) => ({
    title: { color: "#999", textAlign: "center", margin: "20px 0" },
}));

export const TrackingRequest = () => {

    const [trackStepsResponse, setTrackStepsResponse] = useState();
    const [projectResponse, setProjectResponse] = useState();
    const [isRequestedTracks, setIsRequestedTracks] = useState(false);
    const { search } = useLocation()
    const values = queryString.parse(search)
    const currentTime = new Date().toLocaleString("vi-VN", { timeZone: "Asia/Jakarta" }).split(',')[1]

    useEffect(() => {
        const { id, phone, otp, otp_id } = values;
        const getTracks = async () => {
            const result = (await tracksProgressApi.getTracksByProjectId(id, phone, otp, otp_id)).data;
            console.log(result);
            const list = result.stages.sort((a,b) => a.step > b.step ? 1 : -1)
            console.log(list)
            setTrackStepsResponse(list);
            setProjectResponse(result)
            setIsRequestedTracks(true);
        }
        getTracks().then(r => r);
    }, [])

    const formatDate = (dateStr) => {
        return new Date(dateStr).toLocaleString("vi-VN", {timeZone: "Asia/Jakarta"}).split(',')[1]
    }

    const formatCurrency = (str) => {
        if (str === null || str === 0 || str === '0'){
            return '0₫'
        }
        return str.toLocaleString('vi', {style : 'currency', currency : 'VND'});

    }

    const formatHour = (dateStr) => {
        const hour = new Date(dateStr).toLocaleString("vi-VN", {timeZone: "Asia/Jakarta"}).split(',')[0].split(':')
        return hour[0] + ' : ' + hour[1]
    }

    const classes = useStyles();

    const ProjectInfoBinding = (project) => (
        <>{
            project
                ?
                (<div className="invoice-container">
                    <div className="post-feed-item">
                        <div className="card mb-3">
                            <div className="row g-0">
                                <div className="col-md-4">
                                    <img
                                        src="https://www.pngitem.com/pimgs/m/421-4212617_person-placeholder-image-transparent-hd-png-download.png"
                                        alt="..."
                                        className="img-fluid"
                                    />
                                </div>
                                <div className="col-md-8">
                                    <div className="card-body">
                                        {
                                            project
                                            && (<>
                                                <h4 className="card-title text-center">Thông tin khách hàng</h4>
                                                <p className="card-text">Tên khách hàng: <strong>{project.customer.name}</strong></p>
                                                <p className="card-text">Số điện thoại khách hàng:  <strong>{project.customer.phone}</strong></p>
                                                <p className="card-text">Email khách hàng: <strong>{project.customer.email}</strong></p>
                                                <hr />
                                                <h4 className="card-title text-center">Thông tin đơn hàng</h4>
                                                <p className="card-text">Tên đơn hàng: <strong>{project.name || '#BirthDay'}</strong></p>
                                                <p className="card-text">Mã đơn đặt hàng: <strong>{project.requestOrder.id}</strong></p>
                                                <p className="card-text">Ngày đặt hàng: <strong>{formatDate(project.createdDate)}</strong></p>
                                                <p className="card-text">Ngày tổ chức: <strong>{formatDate(project.holdTime)}</strong></p>
                                            </>)
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="invoice-description">
                        <div className="post-title--inline">
                            <h3 className="word-break mr-05">
                                <div className="link">
                                    {<>Mô tả về đơn hàng</>}
                                </div>
                            </h3>
                        </div>
                        <div className="description-table">
                            <table id="table-content">
                                <thead className="tb-content-header">
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Tên đơn hàng</th>
                                        <th scope="col">Thời gian tổ chức</th>
                                        <th scope="col">Địa điểm tổ chức</th>
                                        <th scope="col">Đơn giá</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th scope="col">#</th>
                                        <td scope="col">
                                            {project.images.length > 0
                                                ? <img src={project.images[0].url} alt="" className="event-avt" />
                                                : <img src="https://drive.google.com/uc?export=view&id=14fxBR27qIkZl0FqStLcYvkKOMGgYGS3l" alt="" className="event-avt" />
                                            }
                                            <div className="event-name">{project.name}</div>
                                            <br/>
                                            <div className="event-name">{project.packs[0].name}</div>
                                        </td>
                                        <td scope="col">
                                            <div className="event-hour">{formatHour(project.holdTime)}</div>
                                            <div className="event-date">{formatDate(project.holdTime)}</div>
                                        </td>
                                        <td scope="col">
                                            <div className="event-location">Địa chỉ:</div>
                                            <div className="event-address">{project.location}</div>
                                        </td>
                                        <td scope="col"><div className="event-cost">{formatCurrency(project.finTotal - project.requestOrder.discount)}</div></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div className="calc-cost">
                            <div className="calc-title">
                                <div>Phí dự kiến</div>
                                <div>Phí thực tế</div>
                                <div>Giảm giá</div>
                                <div className="purchase-title">Thanh toán</div>
                            </div>

                            <div className="calc-data">
                                <div>{formatCurrency(project.preTotal)}</div>
                                <div>{formatCurrency(project.finTotal)}</div>
                                <div>{formatCurrency(project.requestOrder.discount)}</div>
                                <div className="purchase-data">{formatCurrency(project.finTotal - project.requestOrder.discount)}</div>
                            </div>
                        </div>
                        <div className="btns">
                            <button className="invoice-btn">Edit</button>
                            <button className="invoice-btn">Print/PDF</button>
                            <button className="invoice-btn">Purchase</button>
                        </div>
                    </div>
                </div>)
                :
                <></>
        }</>
    )


    const ProjectObj = (
        <>
            <Typography
                variant="h4"
                className={classes.title}>
                THÔNG TIN ĐƠN HÀNG CỦA BẠN
            </Typography>
            <div className="container customer-info">
                <div className="row pt-3 pb-1">
                    <div className="">
                        {/* col-lg-9 pl-md-0 */}
                        <div className="post-feed">
                            {
                                ProjectInfoBinding(projectResponse)
                            }
                        </div>
                    </div>
                </div>
            </div>
        </>
    )

    return (
        <div style={{ padding: "20px", backgroundColor: "#EEEEEE" }}>
            {ProjectObj}
            {
                isRequestedTracks
                    ?
                    (
                        Array.isArray(trackStepsResponse)
                            ?
                            (
                                <>
                                    <Typography
                                        variant="h4"
                                        className={classes.title}>
                                        TIẾN TRÌNH ĐƠN HÀNG
                                    </Typography>
                                    <VerticalTimeline>
                                        {Array.isArray(trackStepsResponse) && trackStepsResponse.map(item => (
                                            <TimeLineItem
                                                name={item.name}
                                                description={item.description || item.name}
                                                step={item.step}
                                                status={item.status}
                                                images={item.images}
                                                deadLine={item.deadLine || currentTime}
                                            />
                                        ))}
                                    </VerticalTimeline>
                                    <Typography
                                        variant="h4"
                                        className={classes.title}>
                                        Mọi thắc mắc xin liên hệ 0961329400 để nhận được sự trợ giúp.
                                    </Typography>
                                </>
                            )
                            :
                            (
                                <Typography
                                    variant="h4"
                                    className={classes.title}>
                                    Chưa có dữ liệu về tiến độ thực hiện!
                                </Typography>
                            )
                    )
                    :
                    (
                        <div style={{ padding: "50px", backgroundColor: "#EEEEEE", textAlign: "center" }}>
                            <CircularProgress style={{ margin: "0 auto" }} />
                        </div>
                    )
            }
        </div>

    );
}

export default TrackingRequest;
