import React from "react";
import {VerticalTimelineElement} from "react-vertical-timeline-component";
import "react-vertical-timeline-component/style.min.css";
import DoneIcon from "@material-ui/icons/Done";
import BuildIcon from "@material-ui/icons/Build";
import ScheduleIcon from "@material-ui/icons/Schedule";

import './TimeLineItem.css'
import ImagesCarousel from "../../../Commons/ImagesCarousel";

export const TimeLineItem = (props) => {


  const formatDate = (dateStr) => {
    return new Date(dateStr).toLocaleString("vi-VN", {timeZone: "Asia/Jakarta"}).split(',')[1]
  }

  const formatHour = (dateStr) => {
    const hour = new Date(dateStr).toLocaleString("vi-VN", {timeZone: "Asia/Jakarta"}).split(',')[0].split(':')
    return hour[0] + ' : ' + hour[1]
  }
  const icon = () => {
    switch (props.status) {
      case 'done':
        return (<DoneIcon/>);
      case 'doing':
        return (<BuildIcon/>);
      case 'todo':
        return (<ScheduleIcon/>);
      default:
        return (<></>)
    }
  }

  return (props && <>
    <VerticalTimelineElement
      className="vertical-timeline-element"
      contentStyle={{background: "#F67A80", color: "#000"}}
      contentArrowStyle={{borderRight: "7px solid  #F67A80"}}
      date={formatHour(props.deadLine) + ' Ngày ' + formatDate(props.deadLine)}
      dateStyle={{fontSize: "14px"}}
      iconStyle={{background: "#F67A80", color: "#fff"}}
      icon={icon()}
    >
      <h5 className="vertical-timeline-element-title">Công việc: {props.name}</h5>
      <p style={{fontSize: "14px", marginBottom: "10px"}}>Mô tả: {props.description}</p>
      {props.images && props.images.length > 0 ? <p style={{fontSize: "14px", marginBottom: "10px"}}>Hình ảnh: </p> : <></>}
      <ImagesCarousel images={props.images}/>
    </VerticalTimelineElement>
    </>
  );
}

export default TimeLineItem;
