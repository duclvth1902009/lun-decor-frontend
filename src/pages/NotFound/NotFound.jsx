import React from 'react';

function NotFound() {
    return (
        <div>
            <div className="container"  style={{marginTop:'2em'}}>
                <div className="row">
                    <div className="col-lg-12">
                        <div className="page-404 text-center margin-bottom-30 margin-top-20 padding-top-40 padding-bottom-40">
                            <div className="image-404"><img className="img-responsive center-block" src="//bizweb.dktcdn.net/100/344/983/themes/704702/assets/404.png?1619100636914" alt="404" /></div>
                            <h1>Lỗi không tìm thấy trang</h1>
                            <p className="land">Có vẻ như các trang mà bạn đang cố gắng tiếp cận không tồn tại nữa hoặc có thể nó vừa di chuyển.</p>
                            <div className="mt-5">
                                <a href="/" className="btn btn-success" title="Về trang chủ"><i className="ion ion-md-home"></i> Về trang chủ</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default NotFound;