import React from 'react';
import {NavLink} from 'react-router-dom';
function About() {
    return (
        <div>
            <div className="container article-wraper"  style={{marginTop:'2em'}}>
                <div className="row">
                    <section className="right-content col-md-12">
                        <article className="article-main" itemType="http://schema.org/Article">
                            <div className="row">
                                <div className="col-md-12">
                                    <h1 className="title-head">Lun Decor với phương châm "For your life" là một nơi hoàn hảo dành cho mọi bữa tiệc tuyệt vời nhất
                                    </h1>
                                    <div className="article-details">
                                        <div className="article-content">
                                            <div className="rte">
                                                <p>Lun Decor được lên ý tưởng và bắt đầu hình hài đứa trẻ từ những năm thứ 21 của thể kỉ 21, có lẽ Lun Decor sẽ là thứ gì đó mới mẻ, sự thu hút mạnh mẽ đối với ngành dịch vụ nói chung và khách hàng nói riêng.</p>
                                                <p><strong>Đội ngũ phát triển Lun Decor</strong></p>
                                                <p>Người lên ý tưởng chính là anh Lại Văn Đức, với sự tưởng tượng phong phú của mình, Lun Decor sẽ bay nhanh và xa hơn nữa, ngoài ra mọi thành viên trong đội ngũ đều có vai trò quan trọng trong quá trình phát triển.</p>
                                                <div className="row">
                                                    <div className="col-md-15 col-sm-4 col-xs-6">
                                                        <div className="ant-single-product">
                                                            <div className="ant-single-product-image">
                                                                <NavLink to="#"><img src="https://dl.dropbox.com/s/kw6bakmzgh7rwf4/L%E1%BA%A1i%20V%C4%83n%20%C4%90%E1%BB%A9c.png" alt="Lại Văn Đức" className="img-responsive center-block" /></NavLink >
                                                                {/* <span className="discount-label discount-label--green">- 50% </span> */}
                                                                <form action="/cart/add" method="post" encType="multipart/form-data" className="hover-icons hidden-sm hidden-xs variants form-nut-grid form-ajaxtocart" data-id="product-actions-13604715">
                                                                    <NavLink className="button ajax_addtocart" to="#" title="Chi tiết"></NavLink >
                                                                    <NavLink className="add-to-cart quick-view quickview" to="#" data-handle="#" title="Xem nhanh"></NavLink >
                                                                </form>
                                                            </div>
                                                            <div className="ant-product-border">
                                                                <h3 className="product-title"><NavLink to="#" title="">Lại Văn Đức</NavLink ></h3>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="col-md-15 col-sm-4 col-xs-6">
                                                        <div className="ant-single-product">
                                                            <div className="ant-single-product-image">
                                                                <NavLink to="#"><img src="https://dl.dropbox.com/s/kest7cv13usz24f/Nguy%E1%BB%85n%20Minh%20Ch%C3%AD.png" alt="Nguyễn Minh Chí" className="img-responsive center-block" /></NavLink >
                                                                <form action="/cart/add" method="post" encType="multipart/form-data" className="hover-icons hidden-sm hidden-xs variants form-nut-grid form-ajaxtocart" data-id="product-actions-13604688">
                                                                    <input type="hidden" name="variantId" defaultValue="22735177" />
                                                                    <NavLink className="button ajax_addtocart" to="#" title="Chi tiết"></NavLink >
                                                                    <NavLink className="add-to-cart quick-view quickview" to="#" data-handle="#" title="Xem nhanh"></NavLink >
                                                                </form>
                                                            </div>
                                                            <div className="ant-product-border">
                                                                <h3 className="product-title"><NavLink to="#" title="">Nguyễn Minh Chí</NavLink ></h3>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="col-md-15 col-sm-4 col-xs-6">
                                                        <div className="ant-single-product">
                                                            <div className="ant-single-product-image">
                                                                <NavLink to="/#"><img src="https://dl.dropboxusercontent.com/s/0mx6hygircpd7k7/Lê%20Trọng%20Đức.png" alt="Lê Trọng Đức" className="img-responsive center-block" /></NavLink >
                                                                <form action="/cart/add" method="post" encType="multipart/form-data" className="hover-icons hidden-sm hidden-xs variants form-nut-grid form-ajaxtocart" data-id="product-actions-13604580">
                                                                    <input type="hidden" name="variantId" defaultValue="22735060" />
                                                                    <NavLink className="button ajax_addtocart" to="#" title="Chi tiết"></NavLink >
                                                                    <NavLink className="add-to-cart quick-view quickview" to="#" data-handle="#" title="Xem nhanh"></NavLink >
                                                                </form>
                                                            </div>
                                                            <div className="ant-product-border">
                                                                <h3 className="product-title"><NavLink to="#" title="">Lê Trọng Đức</NavLink ></h3>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="col-md-15 col-sm-4 col-xs-6">
                                                        <div className="ant-single-product">
                                                            <div className="ant-single-product-image">
                                                                <NavLink to="/#"><img src="https://dl.dropbox.com/s/8bx5a5ohbzhsz0k/%C4%90%C3%A0o%20Ng%E1%BB%8Dc%20Long.png" alt="Đào Ngọc Long" className="img-responsive center-block" /></NavLink >
                                                                <form action="/cart/add" method="post" encType="multipart/form-data" className="hover-icons hidden-sm hidden-xs variants form-nut-grid form-ajaxtocart" data-id="product-actions-13604495">
                                                                    <input type="hidden" name="variantId" defaultValue="22734973" />
                                                                    <NavLink className="button ajax_addtocart" to="#" title="Chi tiết"></NavLink >
                                                                    <NavLink className="add-to-cart quick-view quickview" to="#" data-handle="#" title="Xem nhanh"></NavLink >
                                                                </form>
                                                            </div>
                                                            <div className="ant-product-border">
                                                                <h3 className="product-title"><NavLink to="#" title="">Đào Ngọc Long</NavLink ></h3>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="col-md-15 col-sm-4 col-xs-6">
                                                        <div className="ant-single-product">
                                                            <div className="ant-single-product-image">
                                                                <NavLink to="/#"><img src="https://dl.dropbox.com/s/zhsk11v5i62srb1/Ph%E1%BA%A1m%20H%E1%BB%93ng%20Th%C3%A1i.png" alt="Phạm Hồng Thái" className="img-responsive center-block" /></NavLink >
                                                                <form action="/cart/add" method="post" encType="multipart/form-data" className="hover-icons hidden-sm hidden-xs variants form-nut-grid form-ajaxtocart" data-id="product-actions-13604523">
                                                                    <input type="hidden" name="variantId" defaultValue="22735001" />
                                                                    <NavLink className="button ajax_addtocart" to="#" title="Chi tiết"></NavLink >
                                                                    <NavLink className="add-to-cart quick-view quickview" to="#" data-handle="#" title="Xem nhanh"></NavLink >
                                                                </form>
                                                            </div>
                                                            <div className="ant-product-border">
                                                                <h3 className="product-title"><NavLink to="#" title="">Phạm Hồng Thái</NavLink ></h3>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <p>Chúng tôi sẽ luôn cố gắng đem lại những gì tốt nhất cho khách hàng, đặt khách hàng làm trọng tâm, vì khách hàng.</p>
                                                <p><strong>Về Lun Decor</strong></p>
                                                <p><img alt="Logo About" src="https://dl.dropbox.com/s/40zzq5v8osipgpo/logo-about.png" />
                                                </p>
                                                <p>Logo là một khối vuông được bo góc tỉ mỉ với tỉ lệ vàng, chữ L với font Be Vietnam kèm hình mặt trăng nhỏ mang ý nghĩa là Lunar New Year - cái tết cổ truyển - văn hóa dân tốc Việt Nam, nhìn vào logo, chúng tôi muốn thể hiện sự mềm mại, uyển chuyển, sự chỉnh chu từ những thứ nhỏ nhất, gắn bó với con người Việt Nam hay chính là đem lại sự quen thuộc, thoải mái nhất cho khách hàng.</p>
                                                <h2>Lời cuối cùng chúng tôi xin chân thành cảm ơn quý khách hàng đã tin dùng và ủng hộ chúng tôi trong suốt thời gian sử dụng dịch vụ tại Lun Decor - Chúc quý khách có một cuộc sống ấm no, hạnh phúc.</h2>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </section>

                </div>
            </div>
        </div>
    )
}

export default About;