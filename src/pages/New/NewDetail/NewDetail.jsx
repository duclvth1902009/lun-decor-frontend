import React from 'react';
import {NavLink} from 'react-router-dom';
function NewDetail() {
    return (
        <div>
            <div className="container article-wraper" style={{ marginTop: '2em' }}>
                <div className="row">
                    <section className="right-content col-md-9">
                        <article className="article-main"  itemType="http://schema.org/Article">

                            <meta itemProp="mainEntityOfPage"
                                content="/dat-chau-cay-canh-phong-thuy-o-dau-trong-nha-thi-tien-bac-tai-loc-se-vao-nhu-nuoc" />
                            <meta itemProp="description" content="" />
                            <meta itemProp="author" content="Nguyễn Chánh Bảo Trung" />
                            <meta itemProp="headline"
                                content="Đặt chậu cây cảnh phong thủy ở đâu trong nhà thì tiền bạc, tài lộc sẽ vào như nước?" />
                            <meta itemProp="image"
                                content="https://bizweb.dktcdn.net/100/344/983/articles/cac-loai-cay-phong-thuy-trong-van-phong-lam-viec-nen-trong-de-thu-hut-tai-loc-2-20180614174338919.jpg?v=1547735691727" />
                            <meta itemProp="datePublished" content="17-01-2019" />
                            <meta itemProp="dateModified" content="17-01-2019" />
                            <div className="hidden" itemProp="publisher"  itemType="https://schema.org/Organization">
                                <div itemProp="logo" itemType="https://schema.org/ImageObject"><img
                                    src="//bizweb.dktcdn.net/100/344/983/themes/704702/assets/logo.png?1619100636914"
                                    alt="Lun Decor" />
                                    <meta itemProp="url"
                                        content="https://bizweb.dktcdn.net/100/344/983/themes/704702/assets/logo.png?1619100636914" />
                                    <meta itemProp="width" content="200" />
                                    <meta itemProp="height" content="49" />
                                </div>
                                <meta itemProp="name"
                                    content="Đặt chậu cây cảnh phong thủy ở đâu trong nhà thì tiền bạc, tài lộc sẽ vào như
                            nước?"/>
                            </div>
                            <div className="row">
                                <div className="col-md-12">
                                    <h1 className="title-head">Trang trí cho một bữa tiệc sinh nhật thì hết khoảng bao nhiêu chi phí?
                                    </h1>

                                    <div className="postby"><span>Đăng bởi <b>Glowstore</b> vào lúc 01/08/2021</span>
                                    </div>
                                    <div className="blog_related margin-bottom-10">
                                        <article className="blog_entry clearfix">
                                            <h3 className="blog_entry-title"><a className="line-clampss" rel="bookmark"
                                                href="/7-loai-cay-cuoi-nam-nhat-dinh-phai-mua-de-giai-tru-van-den-keo-tai-loc-vao-nha"
                                                title="7 loại cây cuối năm nhất định phải mua để giải trừ vận đen, kéo tài lộc vào nhà"><i
                                                    className="fa fa-angle-right" aria-hidden="true"></i> Cách trang trí bong bóng sinh nhật tại nhà</a></h3>
                                        </article>
                                        <article className="blog_entry clearfix">
                                            <h3 className="blog_entry-title"><a className="line-clampss" rel="bookmark"
                                                href="/6-thoi-xau-khien-ban-ngheo-quanh-nam-bo-ngay-di-de-than-tai-go-cua"
                                                title="6 thói xấu khiến bạn nghèo quanh năm, bỏ ngay đi để Thần Tài gõ cửa"><i
                                                    className="fa fa-angle-right" aria-hidden="true"></i> Bóng Jumbo - Trái bóng kim tuyến nhiệm màu</a></h3>
                                        </article>
                                        <article className="blog_entry clearfix">
                                            <h3 className="blog_entry-title"><a className="line-clampss" rel="bookmark"
                                                href="/dat-cay-luoi-ho-trong-phong-ngu-sang-di-lam-gap-may-chieu-ve-nha-chong-cung-nhu-trung"
                                                title="Đặt cây lưỡi hổ trong phòng ngủ, sáng đi làm gặp may chiều về nhà chồng 'cưng như trứng'"><i
                                                    className="fa fa-angle-right" aria-hidden="true"></i> Tổ chức sinh nhật an toàn mùa dịch</a></h3>
                                        </article>
                                        <article className="blog_entry clearfix">
                                            <h3 className="blog_entry-title"><a className="line-clampss" rel="bookmark"
                                                href="/trong-cay-phong-thuy-theo-tuoi-cho-12-con-giap-gap-may-ca-nam-tai-loc-day-nha"
                                                title="Trồng cây phong thủy theo tuổi cho 12 con giáp gặp may cả năm, tài lộc đầy nhà"><i
                                                    className="fa fa-angle-right" aria-hidden="true"></i> Bí quyết trang trí bàn Gallery sinh nhật đẹp và ấn tượng</a></h3>
                                        </article>
                                    </div>
                                    <div className="article-details">
                                        <div className="article-content">
                                            <div className="rte">
                                                <h2>Trang trí cho một bữa tiệc sinh nhật thì hết khoảng bao nhiêu chi phí?</h2>
                                                <p>Đây là câu hỏi thường trực, là nỗi băn khoăn của không biết bao nhiêu khách hàng mỗi khi cân nhắc để tổ chức một buổi sinh nhật. Bài viết ở đây sẽ chỉ đề cập đến chi phí thực hiện trang trí, không bàn tới các chi phí khác như thuê không gian, địa điểm tổ chức, đặt món ăn, âm thanh hay ánh sáng cùng vô vàn các chi phí có thể phát sinh khác.
                                                    <br />
                                                    Đầu tiên, để bạn dễ hình dung, Glow sẽ phân loại trang trí sinh nhật ra làm 3 kiểu như sau.</p>
                                                <p><strong>1.Trang trí từ các phụ kiện có sẵn, có thể mua tại các cửa hàng bán đồ sinh nhật.</strong></p>
                                                <p>Khi bạn đặt ưu tiên làm sao cho chi phí được thấp nhất mà vẫn đẹp thì sử dụng đồ có sẵn bao giờ cũng là lựa chọn số 1, đó là những sản phẩm được sử dụng phổ biến, được các nhà sản xuất làm theo kiểu công nghiệp nên số lượng sản xuất ra cũng cực kỳ lớn, giảm giá thành sản phẩm để dễ dàng đến tay người mua.</p>
                                                <p><img alt="dat chau cay canh phong thuy o dau trong nha thi tien bac, tai loc se &amp;#34;vao nhu nuoc? - 1"
                                                    data-original="https://cdn.eva.vn/upload/2-2018/images/2018-06-09/nhung-luu-y-khi-lua-chon-cay-canh-phong-thuy-trong-nha-giup-thu-hut-tien-bac-tai-loc-5-1528512200-989-width600height366.jpg"
                                                    data-was-processed="true" height="305"
                                                    src="https://file.hstatic.net/1000246347/file/m_boutique_-_1tr8_14cb172f8fc646aaaacc0ade077b93d6_1024x1024.jpg"
                                                    width="500" /></p>
                                                <p><strong>2.Trang trí từ các phụ kiện có sẵn kết hợp cùng một số phụ kiện thiết kế riêng.</strong></p>
                                                <p>Nếu bạn muốn bữa tiệc của mình vẫn phải có những nét riêng, không bị đụng hàng, tuy nhiên chi phí vẫn phải đảm bảo không phát sinh quá nhiều. Bạn có thể cân nhắc đặt một vài phụ kiện thiết kế riêng đơn giản như Bảng tên, Hình Chibi, Cây Welcome hay là bộ Photobooth chụp hình, cây Hashtag cầm tay cho mọi người chụp hình Selfie.</p>
                                                <p><strong>3.Trang trí với phần lớn các phụ kiện được thiết kế, đặt riêng.</strong></p>
                                                <p>Nếu bạn muốn buổi sinh nhật thật "chất lừ", có thể không cần phải quá hoành tráng, nhưng tất cả các phụ kiện trang trí đều xịn xò. Từ kích thước chuẩn như đo ni đóng giày, từ Tông màu sử dụng hài hòa ăn khớp với nơi tổ chức cho đến việc Chủ đề của buổi sinh nhật toát lên được chủ ý của chủ tiệc. Thì việc đặt đồ thiết kế riêng và bàn cẩn thận với bên Ideas lẫn Design là chuyện bắt buộc.
                                                    <br />
                                                    Tổng quan: Càng nhiều đồ phụ kiện liên quan tới việc thiết kế riêng thì chi phí của buổi tiệc sẽ càng cao, chưa kể khoảng thời gian phát sinh khác sau đó phải đợi như thời gian thiết kế, in ấn, gia công hoàn thiện sản phẩm cũng là những điểm khách hàng cần chú ý để tránh tình trạng đặt hàng quá gấp.</p>
                                                <p><img alt="dat chau cay canh phong thuy o dau trong nha thi tien bac, tai loc se &amp;#34;vao nhu nuoc? - 2"
                                                    data-original="https://cdn.eva.vn/upload/2-2018/images/2018-06-09/nhung-luu-y-khi-lua-chon-cay-canh-phong-thuy-trong-nha-giup-thu-hut-tien-bac-tai-loc-4-1528512200-496-width600height450.jpg"
                                                    data-was-processed="true"
                                                    src="https://file.hstatic.net/1000246347/file/be_bambi__2__d34941d1b2534961b18bb41d6a619278_1024x1024.jpg" />
                                                </p>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-12 margin-bottom-10">
                                    <div className="tag_article">
                                        <span className="inline">Tags: </span>
                                        <NavLink to="#" title="Chậu cảnh">Tin mới</NavLink>, <NavLink
                                            to="#" title="Phong thủy">Phong thủy</NavLink>, <NavLink
                                                to="#" title="Tài lộc">Tài lộc</NavLink>, <NavLink
                                                    to="#" title="Tiền bạc">Tiền bạc</NavLink>
                                    </div>
                                </div>
                                <div className="col-md-12">
                                    <script type="text/javascript"
                                        src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5a099baca270babc"></script>
                                    <div className="addthis_inline_share_toolbox_jje8"></div>
                                </div>
                                <div className="col-md-12 margin-top-20">
                                    <div id="article-comments" className="clearfix">
                                        <h5 className="title-form-coment">Bình luận (2 bình luận)</h5>

                                        <div className="article-comment clearfix" id="438880">
                                            <figure className="article-comment-user-image"><img
                                                src="//bizweb.dktcdn.net/100/344/983/themes/704702/assets/loader.svg?1619100636914"
                                                data-lazyload="https://www.gravatar.com/avatar/7f63a73e68aed17c555fa1b2fb41f811?s=55&d=identicon"
                                                alt="binh-luan" className="block" /></figure>
                                            <div className="article-comment-user-comment">
                                                <p className="user-name-comment"><strong>A A</strong></p>
                                                <span className="article-comment-date-bull">21/01/2019</span>
                                                <p>Dương tính là những cây cần nhiều ánh sáng. Khi trồng ở nơi ẩm thấp, thiếu
                                                    ánh sáng, chúng sẽ phát triển yếu ớt, khó ra hoa kết trái và dễ chết, chẳng
                                                    hạn như hoa hồng, hoa cúc, thược dược, đỗ quyên,... Âm tính là những cây có
                                                    thể đặt ở trong nhà hay bóng râm. Cây nội thất (trồng được trong nhà) thuộc
                                                    dòng cây Âm tính.</p>
                                            </div>
                                        </div>
                                        <div className="article-comment clearfix" id="438879">
                                            <figure className="article-comment-user-image"><img
                                                src="//bizweb.dktcdn.net/100/344/983/themes/704702/assets/loader.svg?1619100636914"
                                                data-lazyload="https://www.gravatar.com/avatar/7f63a73e68aed17c555fa1b2fb41f811?s=55&d=identicon"
                                                alt="binh-luan" className="block" /></figure>
                                            <div className="article-comment-user-comment">
                                                <p className="user-name-comment"><strong>A A</strong></p>
                                                <span className="article-comment-date-bull">21/01/2019</span>
                                                <p>Mệnh Kim: Muốn tăng vận khí của bản thân thì người mệnh Kim nên để 4 hoặc 9
                                                    chậu cảnh trong nhà là những con số cực kỳ tốt. Cây cảnh phù hợp với người
                                                    mệnh Kim là cây kim ngân, kim tiền, cây phát lộc, sen đá nâu. Người mệnh Kim
                                                    có thể chọn các loại cây có lá ánh vàng, hoa vàng như hoa hướng dương, cúc
                                                    vàng, ly vàng... hoặc kết hợp các loại cây xanh thuộc hành Thổ.</p>
                                            </div>
                                        </div>

                                    </div>
                                    <form acceptCharset="utf-8"
                                        action="/posts/dat-chau-cay-canh-phong-thuy-o-dau-trong-nha-thi-tien-bac-tai-loc-se-vao-nhu-nuoc/comments"
                                        id="article_comments" method="post">
                                        <input name="FormType" type="hidden" defaultValue="article_comments" />
                                        <input name="utf8" type="hidden" defaultValue="true" />
                                        <div className="form-coment margin-bottom-10">
                                            <h5 className="title-form-coment">VIẾT BÌNH LUẬN CỦA BẠN</h5>
                                            <p>Địa chỉ email của bạn sẽ được bảo mật. Các trường bắt buộc được đánh dấu <span
                                                className="required">*</span></p>
                                            <div className="row">
                                                <div className="col-sm-12">
                                                    <fieldset className="form-group">
                                                        <label>Nội dung<span className="required">*</span></label>
                                                        <textarea placeholder="Nội dung" className="form-control form-control-lg"
                                                            id="comment" name="Body" rows="6" required
                                                            data-validation-error-msg="Không được để trống"
                                                            data-validation="required"></textarea>
                                                    </fieldset>
                                                </div>
                                                <div className="col-sm-6">
                                                    <fieldset className="form-group">
                                                        <label>Họ tên<span className="required">*</span></label>
                                                        <input placeholder="Họ tên" type="text"
                                                            className="form-control form-control-lg" defaultValue="" id="full-name"
                                                            name="Author" required
                                                            data-validation-error-msg="Không được để trống"
                                                            data-validation="required" />
                                                    </fieldset>
                                                </div>
                                                <div className="col-sm-6">
                                                    <fieldset className="form-group">
                                                        <label>Email<span className="required">*</span></label>
                                                        <input placeholder="Email" type="email"
                                                            className="form-control form-control-lg" defaultValue="" id="email"
                                                            name="Email" data-validation="email"
                                                            pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,63}$"
                                                            data-validation-error-msg="Email sai định dạng" required />
                                                    </fieldset>
                                                </div>
                                                <div className="col-sm-12">
                                                    <button type="submit" className="btn btn-blues pull-right">Gửi bình
                                                        luận</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </article>
                    </section>
                    <aside className="left left-content col-md-3">
                        <aside className="aside-item collection-category blog-category">
                            <div className="heading">
                                <h2 className="title-head"><span>Danh mục</span></h2>
                            </div>
                            <div className="aside-content">
                                <nav className="nav-category  navbar-toggleable-md">
                                    <ul className="nav navbar-pills">
                                        <li className="nav-item ">
                                            <a href="/tieu-canh-de-ban" className="nav-link" title="Tiểu Cảnh Để Bàn">Tiệc Cưới</a><i className="fa fa-angle-down"></i>
                                            <ul className="dropdown-menu">
                                                <li className="nav-item "><a className="nav-link" href="/tieu-canh-terrarium"
                                                    title="Tiểu Cảnh Terrarium">Tiểu Cảnh Terrarium</a></li>
                                            </ul>
                                        </li>
                                        <li className="nav-item ">
                                            <a href="/chau-canh-de-ban" className="nav-link" title="Chậu Cảnh Để Bàn">Tiệc Sinh Nhật</a><i className="fa fa-angle-down"></i>
                                            <ul className="dropdown-menu">


                                                <li className="nav-item "><a className="nav-link" href="/chau-trung-oasis"
                                                    title="Chậu Trứng Oasis">Chậu Trứng Oasis</a></li>

                                            </ul>
                                        </li>



                                        <li className="nav-item ">
                                            <a href="/cay-canh-mini" className="nav-link" title="Cây Cảnh Mini">Tiệc Tân Gia</a><i
                                                className="fa fa-angle-down"></i>
                                            <ul className="dropdown-menu">


                                                <li className="nav-item "><a className="nav-link" href="/cay-terrarium"
                                                    title="Cây Terrarium">Cây Terrarium</a></li>

                                            </ul>
                                        </li>



                                        <li className="nav-item ">
                                            <a href="/phu-kien-trang-tri" className="nav-link" title="Phụ Kiện Trang Trí">Tiệc Sinh Nhật</a><i className="fa fa-angle-down"></i>
                                            <ul className="dropdown-menu">


                                                <li className="nav-item "><a className="nav-link" href="/binh-thuy-tinh"
                                                    title="Bình Thủy Tinh">Bình Thủy Tinh</a></li>
                                            </ul>
                                        </li>



                                        <li className="nav-item ">
                                            <a href="/chau-trong-cay" className="nav-link" title="Chậu trồng cây">Tiệc Bạn Bè</a><i className="fa fa-angle-down"></i>
                                            <ul className="dropdown-menu">


                                                <li className="nav-item "><a className="nav-link" href="/chau-da-mai-trong-cay"
                                                    title="Chậu đá mài trồng cây">Chậu đá mài trồng cây</a></li>


                                            </ul>
                                        </li>



                                        <li className="nav-item ">
                                            <a href="/hoa-chau" className="nav-link" title="Hoa chậu">Tiệc khai trương</a><i
                                                className="fa fa-angle-down"></i>
                                            <ul className="dropdown-menu">


                                                <li className="nav-item "><a className="nav-link" href="/hoa-chau-treo"
                                                    title="Hoa chậu treo">Hoa chậu treo</a></li>


                                            </ul>
                                        </li>



                                        <li className="nav-item ">
                                            <a href="/cay-cong-trinh" className="nav-link" title="Cây công trình">Dịch Vụ Khác</a><i className="fa fa-angle-down"></i>
                                            <ul className="dropdown-menu">


                                                <li className="nav-item "><a className="nav-link" href="/cay-xanh-do-thi"
                                                    title="Cây xanh đô thị">Cây xanh đô thị</a></li>



                                                <li className="nav-item "><a className="nav-link" href="/cay-bong-mat"
                                                    title="Cây bóng mát">Cây bóng mát</a></li>



                                                <li className="nav-item "><a className="nav-link" href="/cay-la-mau"
                                                    title="Cây lá màu">Cây lá màu</a></li>



                                                <li className="nav-item "><a className="nav-link" href="/cac-loai-co-canh"
                                                    title="Các loại cỏ cảnh">Các loại cỏ cảnh</a></li>


                                            </ul>
                                        </li>


                                    </ul>
                                </nav>
                            </div>
                        </aside>


                        <div className="aside-item">
                            <div className="heading">
                                <h2 className="title-head"><a href="tin-tuc" title="Xem nhiều nhất">Xem nhiều nhất</a></h2>
                            </div>
                            <div className="list-blogs">
                                <article className="blog-item blog-item-list clearfix">
                                    <a href="/dat-chau-cay-canh-phong-thuy-o-dau-trong-nha-thi-tien-bac-tai-loc-se-vao-nhu-nuoc"
                                        className="panel-box-media"
                                        title="Đ&#7863;t ch&#7853;u cây c&#7843;nh phong th&#7911;y &#7903; đâu trong nhà thì ti&#7873;n b&#7841;c, tài l&#7897;c s&#7869; &quot;vào như nư&#7899;c?"><img
                                            src="https://file.hstatic.net/1000246347/article/be_harry_73505906742e4a6fa913b72501762855_large.jpg"
                                            data-lazyload="//bizweb.dktcdn.net/thumb/small/100/344/983/articles/cac-loai-cay-phong-thuy-trong-van-phong-lam-viec-nen-trong-de-thu-hut-tai-loc-2-20180614174338919.jpg?v=1547735691727"
                                            width="70" height="70"
                                            alt="Đ&#7863;t ch&#7853;u cây c&#7843;nh phong th&#7911;y &#7903; đâu trong nhà thì ti&#7873;n b&#7841;c, tài l&#7897;c s&#7869; &quot;vào như nư&#7899;c?" /></a>
                                    <div className="blogs-rights">
                                        <h3 className="blog-item-name"><a
                                            href="/dat-chau-cay-canh-phong-thuy-o-dau-trong-nha-thi-tien-bac-tai-loc-se-vao-nhu-nuoc"
                                            title="Đ&#7863;t ch&#7853;u cây c&#7843;nh phong th&#7911;y &#7903; đâu trong nhà thì ti&#7873;n b&#7841;c, tài l&#7897;c s&#7869; &quot;vào như nư&#7899;c?">Trang trí cho một bữa tiệc sinh nhật thì hết khoảng bao nhiêu chi phí?</a></h3>
                                        <div className="post-time">Ngày đăng: 01/08/2021</div>
                                    </div>
                                </article>
                                <article className="blog-item blog-item-list clearfix">
                                    <a href="/7-loai-cay-cuoi-nam-nhat-dinh-phai-mua-de-giai-tru-van-den-keo-tai-loc-vao-nha"
                                        className="panel-box-media"
                                        title="7 lo&#7841;i cây cu&#7889;i năm nh&#7845;t đ&#7883;nh ph&#7843;i mua đ&#7875; gi&#7843;i tr&#7915; v&#7853;n đen, kéo tài l&#7897;c vào nhà"><img
                                            src="https://file.hstatic.net/1000246347/article/48_bistro_-_2tr5__2__0b2affe7239e4b46ad753c7ccff9cb85_large.jpg"
                                            data-lazyload="//bizweb.dktcdn.net/thumb/small/100/344/983/articles/hoa-sen.jpg?v=1547735515717"
                                            width="70" height="70"
                                            alt="7 lo&#7841;i cây cu&#7889;i năm nh&#7845;t đ&#7883;nh ph&#7843;i mua đ&#7875; gi&#7843;i tr&#7915; v&#7853;n đen, kéo tài l&#7897;c vào nhà" /></a>
                                    <div className="blogs-rights">
                                        <h3 className="blog-item-name"><a
                                            href="/7-loai-cay-cuoi-nam-nhat-dinh-phai-mua-de-giai-tru-van-den-keo-tai-loc-vao-nha"
                                            title="7 lo&#7841;i cây cu&#7889;i năm nh&#7845;t đ&#7883;nh ph&#7843;i mua đ&#7875; gi&#7843;i tr&#7915; v&#7853;n đen, kéo tài l&#7897;c vào nhà">Làm sao để trang trí và phối màu bong bóng đẹp trong tiệc sinh nhật</a></h3>
                                        <div className="post-time">Ngày đăng: 05/08/2021</div>
                                    </div>
                                </article>
                                <article className="blog-item blog-item-list clearfix">
                                    <a href="/6-thoi-xau-khien-ban-ngheo-quanh-nam-bo-ngay-di-de-than-tai-go-cua"
                                        className="panel-box-media"
                                        title="6 thói x&#7845;u khi&#7871;n b&#7841;n nghèo quanh năm, b&#7887; ngay đi đ&#7875; Th&#7847;n Tài gõ c&#7917;a"><img
                                            src="https://file.hstatic.net/1000246347/article/57439877_10217218368922661_4313614910127865856_o_40fda2fc4bb4446fb1a2b167cf36cd34_large.jpg"
                                            data-lazyload="//bizweb.dktcdn.net/thumb/small/100/344/983/articles/sen-da-thai-4a.jpg?v=1547735444777"
                                            width="70" height="70"
                                            alt="6 thói x&#7845;u khi&#7871;n b&#7841;n nghèo quanh năm, b&#7887; ngay đi đ&#7875; Th&#7847;n Tài gõ c&#7917;a" /></a>
                                    <div className="blogs-rights">
                                        <h3 className="blog-item-name"><a
                                            href="/6-thoi-xau-khien-ban-ngheo-quanh-nam-bo-ngay-di-de-than-tai-go-cua"
                                            title="6 thói x&#7845;u khi&#7871;n b&#7841;n nghèo quanh năm, b&#7887; ngay đi đ&#7875; Th&#7847;n Tài gõ c&#7917;a">Thuê phụ kiện trang trí sinh nhật, có nên hay không?</a></h3>
                                        <div className="post-time">Ngày đăng: 10/08/2021</div>
                                    </div>
                                </article>
                            </div>
                            <div className="blogs-mores text-center"><a href="tin-tuc" title="Xem thêm">Xem thêm</a></div>
                        </div>

                    </aside>
                </div>
            </div>
        </div>
    )
}

export default NewDetail;
