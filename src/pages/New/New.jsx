/* eslint-disable */
import React from 'react';
import { NavLink } from 'react-router-dom';

function New() {
    return (
        <div>
            <div className="container" itemType="http://schema.org/Blog"  style={{marginTop:'2em'}}>
                <meta itemProp="name" content="Tin tức" />
                <meta itemProp="description" content="Chủ đề không có mô tả" />
                <div className="row">
                    <section className="right-content col-md-9 list-blog-page">
                        <div className="box-heading">
                            <h1 className="title-head">Tin tức</h1>
                        </div>

                        <section className="list-blogs blog-main margin-top-30">
                            <div className="row">
                                <div className="col-md-12 col-sm-12 col-xs-12 clearfix">
                                    <article className="blog-item">
                                        <div className="blog-item-thumbnail"><NavLink to="/new-detail" title=""><img src="https://file.hstatic.net/1000246347/article/48_bistro_-_2tr5__2__0b2affe7239e4b46ad753c7ccff9cb85_large.jpg" className="img-responsive center-block" /></NavLink></div>
                                        <div className="blog-item-mains">
                                            <h3 className="blog-item-name"><NavLink to="/new-detail" title="">Trang trí cho một bữa tiệc sinh nhật thì hết khoảng bao nhiêu chi phí?</NavLink></h3>
                                            <div className="post-time"><i className="fa fa-user" aria-hidden="true"></i> Glowstore | <i className="fa fa-calendar"></i> 01-08-2021</div>
                                            <p className="blog-item-summary margin-bottom-5"> Trang trí cho một bữa tiệc sinh nhật thì hết khoảng bao nhiêu chi phí?Đây là câu hỏi thường trực, là nỗi băn khoăn của không biết bao nhiêu khách hàng mỗi khi cân nhắc để tổ chức một buổi...</p>
                                        </div>
                                    </article>
                                </div>
                                <div className="col-md-12 col-sm-12 col-xs-12 clearfix">
                                    <article className="blog-item">
                                        <div className="blog-item-thumbnail"><a href="/7-loai-cay-cuoi-nam-nhat-dinh-phai-mua-de-giai-tru-van-den-keo-tai-loc-vao-nha" 
                                        title="7 lo&#7841;i cây cu&#7889;i năm nh&#7845;t đ&#7883;nh ph&#7843;i mua đ&#7875; gi&#7843;i tr&#7915; v&#7853;n đen, kéo tài l&#7897;c vào nhà">
                                            <img src="https://file.hstatic.net/1000246347/article/be_harry_73505906742e4a6fa913b72501762855_large.jpg" alt="Làm sao để trang trí và phối màu bong bóng đẹp trong tiệc sinh nhật" className="img-responsive center-block" /></a></div>
                                        <div className="blog-item-mains">
                                            <h3 className="blog-item-name"><a href="/7-loai-cay-cuoi-nam-nhat-dinh-phai-mua-de-giai-tru-van-den-keo-tai-loc-vao-nha" title="Làm sao để trang trí và phối màu bong bóng đẹp trong tiệc sinh nhật">Làm sao để trang trí và phối màu bong bóng đẹp trong tiệc sinh nhật</a></h3>
                                            <div className="post-time"><i className="fa fa-user" aria-hidden="true"></i> Glowstore | <i className="fa fa-calendar"></i> 08-05-2021</div>
                                            <p className="blog-item-summary margin-bottom-5"> Cách phối màu bong bóng trong trang trí tiệcBong bóng ngày càng được ưa chuộng và là một trong những phụ kiện trang trí không thể thiếu ở những bữa tiệc, sự kiện từ quy mô nhỏ đến lớn, với...</p>
                                        </div>
                                    </article>
                                </div>
                                <div className="col-md-12 col-sm-12 col-xs-12 clearfix">
                                    <article className="blog-item">
                                        <div className="blog-item-thumbnail"><a href="/6-thoi-xau-khien-ban-ngheo-quanh-nam-bo-ngay-di-de-than-tai-go-cua" title="abc">
                                            <img src="https://file.hstatic.net/1000246347/article/57439877_10217218368922661_4313614910127865856_o_40fda2fc4bb4446fb1a2b167cf36cd34_large.jpg" 
                                           
                                            alt="6 thói x&#7845;u khi&#7871;n b&#7841;n nghèo quanh năm, b&#7887; ngay đi đ&#7875; Th&#7847;n Tài gõ c&#7917;a" className="img-responsive center-block" /></a></div>
                                        <div className="blog-item-mains">
                                            <h3 className="blog-item-name"><a href="/">Thuê phụ kiện trang trí sinh nhật, có nên hay không?</a></h3>
                                            <div className="post-time"><i className="fa fa-user" aria-hidden="true"></i> Glowstore | <i className="fa fa-calendar"></i> 10-08-2021</div>
                                            <p className="blog-item-summary margin-bottom-5">THUÊ PHỤ KIỆN TRANG TRÍ SINH NHẬTDù ở độ tuổi nào thì ngày sinh nhật cũng là một trong những thời điểm ý nghĩa nhất trong năm. Bạn đang muốn tự tay trang trí một bữa tiệc cho người thân...</p>
                                        </div>
                                    </article>
                                </div>
        
                                <div className="col-md-12 col-sm-12 col-xs-12"></div>
                            </div>
                        </section>

                    </section>
                    <aside className="left left-content col-md-3">
                    <aside className="aside-item collection-category blog-category">
                            <div className="heading">
                                <h2 className="title-head"><span>Danh mục</span></h2>
                            </div>
                            <div className="aside-content">
                                <nav className="nav-category  navbar-toggleable-md">
                                    <ul className="nav navbar-pills">
                                        <li className="nav-item ">
                                            <a href="/tieu-canh-de-ban" className="nav-link" title="Tiểu Cảnh Để Bàn">Tiệc Cưới</a><i className="fa fa-angle-down"></i>
                                            <ul className="dropdown-menu">
                                                <li className="nav-item "><a className="nav-link" href="/tieu-canh-terrarium"
                                                    title="Tiểu Cảnh Terrarium">Tiểu Cảnh Terrarium</a></li>
                                            </ul>
                                        </li>
                                        <li className="nav-item ">
                                            <a href="/chau-canh-de-ban" className="nav-link" title="Chậu Cảnh Để Bàn">Tiệc Sinh Nhật</a><i className="fa fa-angle-down"></i>
                                            <ul className="dropdown-menu">


                                                <li className="nav-item "><a className="nav-link" href="/chau-trung-oasis"
                                                    title="Chậu Trứng Oasis">Chậu Trứng Oasis</a></li>

                                            </ul>
                                        </li>



                                        <li className="nav-item ">
                                            <a href="/cay-canh-mini" className="nav-link" title="Cây Cảnh Mini">Tiệc Tân Gia</a><i
                                                className="fa fa-angle-down"></i>
                                            <ul className="dropdown-menu">


                                                <li className="nav-item "><a className="nav-link" href="/cay-terrarium"
                                                    title="Cây Terrarium">Cây Terrarium</a></li>

                                            </ul>
                                        </li>



                                        <li className="nav-item ">
                                            <a href="/phu-kien-trang-tri" className="nav-link" title="Phụ Kiện Trang Trí">Tiệc Sinh Nhật</a><i className="fa fa-angle-down"></i>
                                            <ul className="dropdown-menu">


                                                <li className="nav-item "><a className="nav-link" href="/binh-thuy-tinh"
                                                    title="Bình Thủy Tinh">Bình Thủy Tinh</a></li>
                                            </ul>
                                        </li>



                                        <li className="nav-item ">
                                            <a href="/chau-trong-cay" className="nav-link" title="Chậu trồng cây">Tiệc Bạn Bè</a><i className="fa fa-angle-down"></i>
                                            <ul className="dropdown-menu">


                                                <li className="nav-item "><a className="nav-link" href="/chau-da-mai-trong-cay"
                                                    title="Chậu đá mài trồng cây">Chậu đá mài trồng cây</a></li>


                                            </ul>
                                        </li>



                                        <li className="nav-item ">
                                            <a href="/hoa-chau" className="nav-link" title="Hoa chậu">Tiệc khai trương</a><i
                                                className="fa fa-angle-down"></i>
                                            <ul className="dropdown-menu">


                                                <li className="nav-item "><a className="nav-link" href="/hoa-chau-treo"
                                                    title="Hoa chậu treo">Hoa chậu treo</a></li>


                                            </ul>
                                        </li>



                                        <li className="nav-item ">
                                            <a href="/cay-cong-trinh" className="nav-link" title="Cây công trình">Dịch Vụ Khác</a><i className="fa fa-angle-down"></i>
                                            <ul className="dropdown-menu">


                                                <li className="nav-item "><a className="nav-link" href="/cay-xanh-do-thi"
                                                    title="Cây xanh đô thị">Cây xanh đô thị</a></li>



                                                <li className="nav-item "><a className="nav-link" href="/cay-bong-mat"
                                                    title="Cây bóng mát">Cây bóng mát</a></li>



                                                <li className="nav-item "><a className="nav-link" href="/cay-la-mau"
                                                    title="Cây lá màu">Cây lá màu</a></li>



                                                <li className="nav-item "><a className="nav-link" href="/cac-loai-co-canh"
                                                    title="Các loại cỏ cảnh">Các loại cỏ cảnh</a></li>


                                            </ul>
                                        </li>


                                    </ul>
                                </nav>
                            </div>
                        </aside>
                        <div className="aside-item">
                            <div className="heading">
                                <h2 className="title-head"><a href="tin-tuc" title="Xem nhiều nhất">Xem nhiều nhất</a></h2>
                            </div>
                            <div className="list-blogs">
                                <article className="blog-item blog-item-list clearfix">
                                    <a href="/dat-chau-cay-canh-phong-thuy-o-dau-trong-nha-thi-tien-bac-tai-loc-se-vao-nhu-nuoc"
                                        className="panel-box-media"
                                        title="Đ&#7863;t ch&#7853;u cây c&#7843;nh phong th&#7911;y &#7903; đâu trong nhà thì ti&#7873;n b&#7841;c, tài l&#7897;c s&#7869; &quot;vào như nư&#7899;c?"><img
                                            src="https://file.hstatic.net/1000246347/article/be_harry_73505906742e4a6fa913b72501762855_large.jpg"
                                            data-lazyload="//bizweb.dktcdn.net/thumb/small/100/344/983/articles/cac-loai-cay-phong-thuy-trong-van-phong-lam-viec-nen-trong-de-thu-hut-tai-loc-2-20180614174338919.jpg?v=1547735691727"
                                            width="70" height="70"
                                            alt="Đ&#7863;t ch&#7853;u cây c&#7843;nh phong th&#7911;y &#7903; đâu trong nhà thì ti&#7873;n b&#7841;c, tài l&#7897;c s&#7869; &quot;vào như nư&#7899;c?" /></a>
                                    <div className="blogs-rights">
                                        <h3 className="blog-item-name"><a
                                            href="/dat-chau-cay-canh-phong-thuy-o-dau-trong-nha-thi-tien-bac-tai-loc-se-vao-nhu-nuoc"
                                            title="Đ&#7863;t ch&#7853;u cây c&#7843;nh phong th&#7911;y &#7903; đâu trong nhà thì ti&#7873;n b&#7841;c, tài l&#7897;c s&#7869; &quot;vào như nư&#7899;c?">Trang trí cho một bữa tiệc sinh nhật thì hết khoảng bao nhiêu chi phí</a></h3>
                                        <div className="post-time">Ngày đăng: 01/08/2021</div>
                                    </div>
                                </article>
                                <article className="blog-item blog-item-list clearfix">
                                    <a href="/7-loai-cay-cuoi-nam-nhat-dinh-phai-mua-de-giai-tru-van-den-keo-tai-loc-vao-nha"
                                        className="panel-box-media"
                                        title="7 lo&#7841;i cây cu&#7889;i năm nh&#7845;t đ&#7883;nh ph&#7843;i mua đ&#7875; gi&#7843;i tr&#7915; v&#7853;n đen, kéo tài l&#7897;c vào nhà"><img
                                            src="https://file.hstatic.net/1000246347/article/48_bistro_-_2tr5__2__0b2affe7239e4b46ad753c7ccff9cb85_large.jpg"
                                            data-lazyload="//bizweb.dktcdn.net/thumb/small/100/344/983/articles/hoa-sen.jpg?v=1547735515717"
                                            width="70" height="70"
                                            alt="7 lo&#7841;i cây cu&#7889;i năm nh&#7845;t đ&#7883;nh ph&#7843;i mua đ&#7875; gi&#7843;i tr&#7915; v&#7853;n đen, kéo tài l&#7897;c vào nhà" /></a>
                                    <div className="blogs-rights">
                                        <h3 className="blog-item-name"><a
                                            href="/7-loai-cay-cuoi-nam-nhat-dinh-phai-mua-de-giai-tru-van-den-keo-tai-loc-vao-nha"
                                            title="7 lo&#7841;i cây cu&#7889;i năm nh&#7845;t đ&#7883;nh ph&#7843;i mua đ&#7875; gi&#7843;i tr&#7915; v&#7853;n đen, kéo tài l&#7897;c vào nhà">Làm sao để trang trí và phối màu bong bóng đẹp trong tiệc sinh nhật</a></h3>
                                        <div className="post-time">Ngày đăng: 05/08/2021</div>
                                    </div>
                                </article>
                                <article className="blog-item blog-item-list clearfix">
                                    <a href="/6-thoi-xau-khien-ban-ngheo-quanh-nam-bo-ngay-di-de-than-tai-go-cua"
                                        className="panel-box-media"
                                        title="6 thói x&#7845;u khi&#7871;n b&#7841;n nghèo quanh năm, b&#7887; ngay đi đ&#7875; Th&#7847;n Tài gõ c&#7917;a"><img
                                            src="https://file.hstatic.net/1000246347/article/57439877_10217218368922661_4313614910127865856_o_40fda2fc4bb4446fb1a2b167cf36cd34_large.jpg"
                                            data-lazyload="//bizweb.dktcdn.net/thumb/small/100/344/983/articles/sen-da-thai-4a.jpg?v=1547735444777"
                                            width="70" height="70"
                                            alt="6 thói x&#7845;u khi&#7871;n b&#7841;n nghèo quanh năm, b&#7887; ngay đi đ&#7875; Th&#7847;n Tài gõ c&#7917;a" /></a>
                                    <div className="blogs-rights">
                                        <h3 className="blog-item-name"><a
                                            href="/6-thoi-xau-khien-ban-ngheo-quanh-nam-bo-ngay-di-de-than-tai-go-cua"
                                            title="6 thói x&#7845;u khi&#7871;n b&#7841;n nghèo quanh năm, b&#7887; ngay đi đ&#7875; Th&#7847;n Tài gõ c&#7917;a">Thuê phụ kiện trang trí sinh nhật, có nên hay không?</a></h3>
                                        <div className="post-time">Ngày đăng: 10/08/2021</div>
                                    </div>
                                </article>
                            </div>
                            <div className="blogs-mores text-center"><a href="tin-tuc" title="Xem thêm">Xem thêm</a></div>
                        </div>
                    </aside>
                </div>
            </div>
        </div>
    )
}

export default New;