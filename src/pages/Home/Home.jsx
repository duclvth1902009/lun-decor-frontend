/* eslint-disable */
import React, { useEffect, useState } from 'react';
import { NavLink } from 'react-router-dom';
import packageApi from '../../api/packageApi';
import {formatCurrency} from "../../utils/utils";
import SinglePackage from "../../components/Commons/SinglePackage";

function Home() {
    const [packageList, setPackageList] = useState([]);
    const [packageBDList, setPackageBDList] = useState([]);
    const [packageBD2List, setPackageBD2List] = useState([]);

    useEffect(() => {
        const fetchPackageList = async () => {
            try {
                const packs = (await packageApi.getAll(0)).data;
                setPackageList(packs);
                const lstBd = packs.filter(x => x.type.name ==="Tiệc Sinh Nhật")
                setPackageBD2List(lstBd.splice(0,2))
                setPackageBDList(lstBd)
            } catch (error) {
                console.log('Failed to fetch service package list', error);
            }
        }
        fetchPackageList().then(r => r);
    }, [])

    return (
        <div className="container">
            <div className="row">
                <section className="awe-section-1">
                    <div className="margin-top-15 top-sliders col-md-12">
                        <div className="home-slider owl-carousel not-dqowl owl-loaded owl-drag">
                            <div className="owl-stage-outer">
                                <div className="owl-stage">
                                    <div className="owl-item active" style={{width:"100%"}}>
                                        <div className="item">
                                            <NavLink to="#" className="clearfix" title="Lun Decor">
                                                <img src={process.env.PUBLIC_URL + 'assets/images/Balloon_Ceiling_Background.jpeg'} alt="Lun Decor" className="img-responsive center-block" />
                                            </NavLink>
                                            <div className="info text-center">
                                                <h3 className="heading">Tân trang bữa tiệc của bạn</h3>
                                                <p className="caption-header hidden-xs">Lun Decor mang đến cho bạn những bữa tiệc mang đậm sự sáng tạo, độc đáo.<br />Giúp mọi người hòa hợp, thân thiết với nhau hơn.</p>
                                                <NavLink to="/package-booking" title="Thuê dịch vụ ngay" className="btn btn-primary" tabIndex="0">Thuê dịch vụ ngay</NavLink>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </section>
                <div className="clearfix"/>
                <section className="awe-section-2">
                    <div className="section_banner clearfix">
                        <div className="col-lg-4 col-md-4 col-sm-4">
                            <div className="single-banner">
                                <NavLink to="#" className="box-img" title="Mùa Tết">
                                    <img src="https://product.hstatic.net/1000246347/product/chu_de_baby_girl_-_2tr_a1e49acb2f384a01965cdb8e251eaa73_large.jpg" alt="Mùa Tết" className="img-responsive center-block" />
                                    <div className="banner-content">
                                        <div className="fix-banner-content">
                                            <div className="banner-title">Mùa Hè</div>
                                            <div className="banner-subtitle">Giảm giá</div>
                                            <div className="percent-sale">đến 50<span className='percent'>%</span></div>
                                        </div>
                                    </div>
                                </NavLink>
                            </div>
                        </div>
                        <div className="col-lg-4 col-md-4 col-sm-4">
                            <div className="single-banner">
                                <NavLink to="#" className="box-img" title="Đón tết">
                                    <img src="https://product.hstatic.net/1000246347/product/combo_bong_bong_trang_tri__1__3adc84259c254d55b487a1f3383df4d0_large.png" alt="Đón tết" className="img-responsive center-block" />
                                    <div className="banner-content">
                                        <div className="fix-banner-content custom-position">
                                            <div className="banner-title">Đón tết</div>
                                            <div className="banner-subtitle">Tân trang bữa tiệc</div>
                                        </div>
                                    </div>
                                </NavLink>
                            </div>
                        </div>
                        <div className="col-lg-4 col-md-4 col-sm-4">
                            <div className="single-banner">
                                <NavLink to="#" className="box-img" title="Flash Sale">
                                    <img src="https://product.hstatic.net/1000246347/product/combo_trang_tri_phong_mau_18_-_pink_gold_38f3b7ab7fb6434aba9d6b310ded0f69_large.jpg" alt="Flash Sale" className="img-responsive center-block" />
                                    <div className="banner-content">
                                        <div className="fix-banner-content custom-position2">
                                            <div className="banner-title">Flash Sale</div>
                                            <div className="percent-sale">50<span className='percent'>%</span></div>
                                            <div className="banner-subtitle">Dịch vụ</div>
                                        </div>
                                    </div>
                                </NavLink>
                            </div>
                        </div>
                    </div>
                </section>
                <div className="clearfix"/>
                <section className="awe-section-3">
                    <div className="section_flash_sale clearfix">
                        <div className="col-md-12">
                            <div className="flash-sale-title margin-bottom-30">
                                <NavLink to="#" title="Sản phẩm mới">Các gói dịch vụ</NavLink>
                            </div>
                            <div className="section-tour-owl2 products products-view-grid owl-carousel not-dqowl owl-loaded owl-drag">
                                <div className="owl-stage-outer">
                                    <div className="owl-stage row" style={{ transform: "translate3d(0px, 0px, 0px)", transition: "all 0s ease 0s"}}>
                                        {
                                            packageList && packageList.map(pack =>
                                                <SinglePackage key={pack.id} pack={pack}/>
                                            )
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <div className="clearfix"/>
                <section className="awe-section-4">
                    <div className="section_about_us clearfix">
                        <div className="rows">
                            <div className="col-md-5 no-padding">
                                <div className="abount-content">
                                    <h3>Top Dịch Vụ</h3>
                                    <h4>đẹp nhất!</h4>
                                    <p>Hãy đi tìm dịch vụ tuyệt vời nhất trong năm</p>
                                    <NavLink to="/service" title="Khám phá ngay">Khám phá ngay</NavLink>
                                </div>
                            </div>
                            <div className="col-md-7 no-padding">
                                <div className="about-image">
                                    <NavLink to="#" title="Lun Decor">
                                        <picture>
                                            <img src="https://cdn.shopify.com/s/files/1/0257/6718/2416/files/Pastel_Birthday_background.jpg?v=1595051319" alt="Lun Decor" className="img-responsive center-block" />
                                        </picture>
                                    </NavLink>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <div className="clearfix"/>
                <section className="awe-section-5">
                    <div className="section_product section_product-1 clearfix">
                        <div className="col-md-12">
                            <div className="section-head clearfix">
                                <h2 className="title_blog"><NavLink to="#" title="Tiểu cảnh để bàn">Tiệc Sinh Nhật</NavLink></h2>
                            </div>
                            <div className="row margin-bottom-10">
                                <div className="col-md-6 col-sm-12 col-xs-12 margin-bottom-10">
                                    <NavLink className="box-img" to="#" title="Lun Decor">
                                        <picture>
                                            <img src={process.env.PUBLIC_URL + "assets/images/birthday-party.jpeg"} className="img-responsive center-block"  alt="image-birthday-party"/>
                                        </picture>
                                    </NavLink>
                                </div>
                                {
                                    packageBD2List && packageBD2List.map(pack =>
                                        <SinglePackage key={pack.id} pack={pack}/>
                                    )
                                }
                            </div>
                            <div className="row">
                                {
                                    packageBDList && packageBDList.map(pack =>
                                        <SinglePackage key={pack.id} pack={pack}/>
                                    )
                                }
                            </div>
                        </div>
                    </div>
                </section>
                <div className="clearfix"/>
                <section className="awe-section-9">
                    <div className="section_policy clearfix">
                        <div className="col-md-12">
                            <div className="owl-policy-mobile owl-carousel not-dqowl owl-loaded owl-drag">
                                <div className="owl-stage-outer">
                                    <div className="owl-stage">
                                        <div className="owl-item active col-sm-3 col-xs-6" >
                                            <div className="item section_policy_content">
                                                <NavLink to="#" title="Mi&#7877;n phí v&#7853;n chuy&#7875;n">
                                                    <img src="//bizweb.dktcdn.net/100/344/983/themes/704702/assets/policy_images_1.png?1619100636914" alt="Miễn phí vận chuyển" />
                                                    <div className="section-policy-padding">
                                                        <h3>Miễn phí vận chuyển</h3>
                                                        <div className="section_policy_title">Cho các đơn hàng {'>'} 5tr</div>
                                                    </div>
                                                </NavLink>
                                            </div>
                                        </div>
                                        <div className="owl-item active col-sm-3 col-xs-6">
                                            <div className="item section_policy_content">
                                                <NavLink to="#" title="H&#7895; tr&#7907; 24/7">
                                                    <img src="//bizweb.dktcdn.net/100/344/983/themes/704702/assets/policy_images_2.png?1619100636914" alt="Hỗ trợ 24/7" />
                                                    <div className="section-policy-padding">
                                                        <h3>Hỗ trợ 24/7</h3>
                                                        <div className="section_policy_title">Liên hệ hỗ trợ 24/7</div>
                                                    </div>
                                                </NavLink>
                                            </div>
                                        </div>

                                        <div className="owl-item active col-sm-3 col-xs-6">
                                            <div className="item section_policy_content">
                                                <NavLink to="#" title="Hoàn ti&#7873;n 100%">
                                                    <img src="//bizweb.dktcdn.net/100/344/983/themes/704702/assets/policy_images_3.png?1619100636914" alt="Hoàn tiền 100%" />
                                                    <div className="section-policy-padding">
                                                        <h3>Hoàn tiền 100%</h3>
                                                        <div className="section_policy_title">Nếu sản phẩm bị lỗi, hư hỏng</div>
                                                    </div>
                                                </NavLink>
                                            </div>
                                        </div>

                                        <div className="owl-item active col-sm-3 col-xs-6">
                                            <div className="item section_policy_content">
                                                <NavLink to="#" title="Thanh toán">
                                                    <img src="//bizweb.dktcdn.net/100/344/983/themes/704702/assets/policy_images_4.png?1619100636914" alt="Thanh toán" />
                                                    <div className="section-policy-padding">
                                                        <h3>Thanh toán</h3>
                                                        <div className="section_policy_title">Được bảo mật 100%</div>
                                                    </div>
                                                </NavLink>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <div className="clearfix"></div>
                <section className="awe-section-10">
                    <div className="section_category clearfix">
                        <div className="col-md-12">
                            <div className="flash-sale-title"> Tiệc cưới </div>
                            <div className="products products-view-grid owl-carousel owl-loaded owl-drag" data-lg-items='4' data-md-items='4' data-sm-items='3' data-xs-items="2" data-xss-items="2" data-margin='10' data-nav="false" data-dot="false">
                                <div className="owl-stage-outer">
                                    <div className="owl-stage">
                                        {
                                            packageList && packageList.map(pack => {
                                                if (pack.type.name === "Tiệc Cưới") {
                                                    return <div key={pack.id} className="owl-item active col-sm-3 col-xs-6">
                                                        <div className="item">
                                                            <NavLink to="/service-detail" title="Tiệc cưới">
                                                                <img className="img-responsive center-block" src={pack && pack.images[0] && pack.images[0].url} alt="" />
                                                                <div className="category-content">
                                                                    <h5>{pack.name}</h5>
                                                                    <p>2 sản phẩm</p>
                                                                </div>
                                                            </NavLink>
                                                        </div>
                                                    </div>
                                                }
                                            })
                                        }

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <div className="clearfix"></div>
                <section className="awe-section-11">
                    <section className="section-news clearfix">
                        <div className="col-md-12">
                            <h3 className="title_blog">
                                <NavLink to="/new" title="Chăm sóc cây cảnh">Tin tức</NavLink>
                            </h3>
                            <div className="section-news-owl owl-carousel not-dqowl owl-loaded owl-drag">
                                <div className="owl-stage-outer">
                                    <div className="owl-stage">
                                        <div className="owl-item active col-sm-4 col-xs-6">
                                            <div className="post-inner clearfix">
                                                <NavLink to="new-detail" title="LÀM SAO ĐỂ TRANG TRÍ VÀ PHỐI MÀU BONG BÓNG ĐẸP TRONG TIỆC SINH NHẬT">
                                                    <div className="post_image">
                                                        <img src="https://file.hstatic.net/1000246347/article/be_harry_73505906742e4a6fa913b72501762855_large.jpg" alt="LÀM SAO ĐỂ TRANG TRÍ VÀ PHỐI MÀU BONG BÓNG ĐẸP TRONG TIỆC SINH NHẬT" className="img-responsive center-block" />
                                                    </div>
                                                    <div className="border-news">
                                                        <h5>LÀM SAO ĐỂ TRANG TRÍ VÀ PHỐI MÀU BONG BÓNG ĐẸP TRONG TIỆC SINH NHẬT</h5>
                                                        <div className="date_added">Ngày đăng: 01/08/2021</div>
                                                    </div>
                                                </NavLink>
                                            </div>
                                        </div>
                                        <div className="owl-item active col-sm-4 col-xs-6">
                                            <div className="post-inner clearfix">
                                                <NavLink to="#" title="Trang trí cho một bữa tiệc sinh nhật thì hết khoảng bao nhiêu chi phí?">
                                                    <div className="post_image">
                                                        <img src="https://file.hstatic.net/1000246347/article/48_bistro_-_2tr5__2__0b2affe7239e4b46ad753c7ccff9cb85_large.jpg" alt="Trang trí cho một bữa tiệc sinh nhật thì hết khoảng bao nhiêu chi phí?" className="img-responsive center-block" />
                                                    </div>
                                                    <div className="border-news">
                                                        <h5>Trang trí cho một bữa tiệc sinh nhật thì hết khoảng bao nhiêu chi phí?</h5>
                                                        <div className="date_added">Ngày đăng: 05/08/2021</div>
                                                    </div>
                                                </NavLink>
                                            </div>
                                        </div>
                                        <div className="owl-item active col-sm-4 col-xs-6">
                                            <div className="post-inner clearfix">
                                                <NavLink to="#" title="Thuê phụ kiện trang trí sinh nhật, có nên hay không?">
                                                    <div className="post_image">
                                                        <img src="https://file.hstatic.net/1000246347/article/57439877_10217218368922661_4313614910127865856_o_40fda2fc4bb4446fb1a2b167cf36cd34_large.jpg" alt="Thuê phụ kiện trang trí sinh nhật, có nên hay không?" className="img-responsive center-block" />
                                                    </div>
                                                    <div className="border-news">
                                                        <h5>Thuê phụ kiện trang trí sinh nhật, có nên hay không?</h5>
                                                        <div className="date_added">Ngày đăng: 10/08/2021</div>
                                                    </div>
                                                </NavLink>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </section>
                <div className="clearfix"></div>
            </div>
        </div>
    )
}

export default Home;
