/* eslint-disable */
import React from 'react';

function Contact() {
    return (
        <div>
            <div className="contact margin-bottom-20 page-contact"  style={{marginTop:'2em'}}>
                <div className="container">
                    <div className="row">
                        <div className="col-md-4">
                            <div className="widget-item info-contact in-fo-page-content">
                                <h1 className="title-head">Thông tin liên hệ</h1>
                                <ul className="widget-menu contact-info-page">
                                    <li>
                                        Lun Decor xin hân hạnh phục vụ quý khách với các dịch vụ cung cấp hay cho thuê đồ trang trí được rất nhiều khách hàng tại Việt Nam ưa thích và chọn lựa.
                                    </li>

                                    <li><i className="fa fa-map-marker" aria-hidden="true"></i>Số 8A Tôn Thất Thuyết, Mỹ Đình, Nam Từ Liêm, Hà Nội 100000</li>
                                    <li><i className="fa fa-phone" aria-hidden="true"></i> <a href="tel:0399002423" title="0399 002 423">0399 002 423</a></li>
                                    <li><i className="fa fa-envelope" aria-hidden="true"></i> <a href="mailto:lundecor@gmail.com" title="lundecor@gmail.com">lundecor@gmail.com</a></li>

                                </ul>
                            </div>
                        </div>
                        <div className="col-md-8">
                            <div className="page-login">
                                <h3 className="title-head">Gửi thông tin</h3>
                                <span className="text-contact margin-bottom-10 block">Bạn hãy điền nội dung tin nhắn vào form dưới đây và gửi cho chúng tôi. Chúng tôi sẽ trả lời bạn sau khi nhận được.</span>
                                <form acceptCharset="utf-8" action="/contact" id="contact" method="post">
                                    <input name="FormType" type="hidden" defaultValue="contact" />
                                    <input name="utf8" type="hidden" defaultValue="true" /><input type="hidden" id="Token-a9fe56fccaa243749a95072375b17ef6" name="Token" />
                                    <script src="https://www.google.com/recaptcha/api.js?render=6Ldtu4IUAAAAAMQzG1gCw3wFlx_GytlZyLrXcsuK"></script>
                                    <div className="form-signup clearfix">
                                        <div className="row">
                                            <div className="col-sm-6 col-xs-12">
                                                <fieldset className="form-group">
                                                    <label>Họ tên<span className="required">*</span></label>
                                                    <input type="text" name="contact[name]" id="name" className="form-control  form-control-lg" data-validation-error-msg="Không được để trống" data-validation="required" required />
                                                </fieldset>
                                            </div>
                                            <div className="col-sm-6 col-xs-12">
                                                <fieldset className="form-group">
                                                    <label>Email<span className="required">*</span></label>
                                                    <input type="email" name="contact[email]" data-validation="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,63}$" data-validation-error-msg="Email sai định dạng" id="email" className="form-control form-control-lg" required />
                                                </fieldset>
                                            </div>
                                            <div className="col-sm-12 col-xs-12">
                                                <fieldset className="form-group">
                                                    <label>Điện thoại<span className="required">*</span></label>
                                                    <input type="tel" name="contact[phone]" data-validation="tel" data-validation-error-msg="Không được để trống" id="tel" className="number-sidebar form-control form-control-lg" required />
                                                </fieldset>
                                            </div>
                                            <div className="col-sm-12 col-xs-12">
                                                <fieldset className="form-group">
                                                    <label>Nội dung<span className="required">*</span></label>
                                                    <textarea name="contact[body]" id="comment" className="form-control form-control-lg" rows="5" data-validation-error-msg="Không được để trống" data-validation="required" required></textarea>
                                                </fieldset>
                                                <div className="pull-xs-left" style={{ marginTop: "20px" }}>
                                                    <button type="submit" className="btn btn-blues btn-style btn-style-active">Gửi tin nhắn</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-12">
                            <div className="contact-map">
                                <div className="page-login text-center">
                                    <h3 className="title-head">Bản đồ cửa hàng</h3>
                                </div>
                                <div className="box-maps margin-top-10 margin-bottom-10">
                                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.0966890674963!2d105.78008835087572!3d21.02881689308271!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x313455f9bdf0e1c7%3A0x26caee8e7662dd9b!2zRlBUIEFwdGVjaCBIw6AgTuG7mWk!5e0!3m2!1sen!2s!4v1626343408793!5m2!1sen!2s" width="100" height="300" style={{ border: "0" }} allowFullScreen></iframe>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Contact;