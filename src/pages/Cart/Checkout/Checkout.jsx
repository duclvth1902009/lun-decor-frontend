/* eslint-disable */
import React from 'react';
import {NavLink} from 'react-router-dom';

function Checkout() {
    return (
        <div>
            <div className="container white collections-container margin-bottom-20"  style={{marginTop:'2em'}}>
                <div className="white-background">
                    <div className="row">
                        <div className="col-md-8">
                            <form action="https://docs.google.com/forms/d/e/1FAIpQLSeRbEA0DYZAwjNzBXa350VYQ0Vke6eSGcjuNsUgVfbjShHT0Q/formResponse?embedded=true" className="blockform" id="form_dathang" method="POST" onsubmit="return emptycart()" style={{display: "block"}} target="hidden_iframe">
                                <div className="content-muangay">
                                    <div className="checkout">
                                        <span className="tt-muangay"><label>Thông tin giao hàng</label></span>
                                        <table cellpadding="0" cellspacing="0" className="muangay" style={{width: "100%"}}>
                                            <tbody>
                                                <tr>
                                                    <td colspan="2"><input aria-required="true" className="ss-q-short" name="entry.2118409370" placeholder="Tên của bạn" required="" title="" type="text" defaultValue="" /> </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2"><input aria-required="true" className="ss-q-short" name="entry.1690189054" placeholder="Địa chỉ nhận hàng" title="" type="text" defaultValue="" /> </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2"><input aria-required="true" className="ss-q-short" name="entry.1864044881" placeholder="Điện thoại" required="" title="" type="text" defaultValue="" /> </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2"><input aria-required="true" className="ss-q-short" name="entry.1657487933" placeholder="Ghi chú" required="" title="" type="text" defaultValue="" /> </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2" className="hidden">
                                                        <div className="checkout-khoirom">
                                                            <div className="simpleCart_items "><div className="cartHeaders"><div className="itemthumb">thumb</div><div className="itemName">Name</div><div className="itemPrice">Price</div><div className="itemdecrement">Decrement</div><div className="itemQuantity">Quantity</div><div className="itemincrement">Increment</div><div className="itemtotal">Total</div><div className="itemRemove">Remove</div></div></div>
                                                            <div className="tamtinh"><div className="simplelove">Tạm tính:</div><div className="text-right simpleCart_total">0&nbsp;₫</div></div>
                                                            <div className="vanchuyen"><div className="simplelove">Chi phí vận chuyển:</div><div className="text-right">35,000 ₫</div></div>
                                                            <div className="thanhtien"><div className="simplelove">Tổng đơn hàng:</div><div className="text-right simpleCart_finalTotal">35,000&nbsp;₫</div></div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <textarea aria-label="Tổng đơn hàng" className="ss-q-long" cols="0" dir="auto" id="layidthongtinsanpham" name="entry.66428279" placeholder="Tổng đơn hàng" readonly="true" rows="5" type="hidden">
                                                            - Phí vận chuyển: 35,000 đ
                                                            - Tổng thanh toán: 35,000 đ
                                                        </textarea>
                                                    </td>
                                                </tr>
                                                <tr> </tr>
                                                <tr>
                                                    <td colspan="2"><label> <input className="button" id="ss-submit" name="submit" type="submit" defaultValue="Đặt hàng" /> </label> </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </form>
                            <div className="100">
                                <div className="hidden-message alert-box success" style={{display: "none"}}>Đơn hàng của bạn đã được chuyển đến chúng tôi.<br />Chúng tôi sẽ liên lạc lại trong thời gian sớm nhất. </div>
                            </div>
                            <iframe id="hidden_iframe" name="hidden_iframe" onload="if(submitted){}" style={{display: "none"}}></iframe>
                            {/* <script type="text/javascript">
                        var submitted = false;
                    
                        function emptycart() {
                            submitted = true;
                            $('.blockform').toggle();
                            $('.hidden-message').show();
                        }
                    $("form").submit(function() {
                        $("iframe#hidden_iframe").on('load', function() {
                            simpleCart.empty();
                        });
                    })
                    </script> */}
                        </div>
                        <div className="col-md-4">
                            <div className="module_service_details clearfix">
                                <div className="item_service clearfix">
                                    <NavLink to="#" title="Miễn phí vận chuyển">
                                        <img alt="Miễn phí vận chuyển" src="https://lh4.googleusercontent.com/-r2RhL8IXDBg/XPXFTenKnlI/AAAAAAAAIqQ/ryIXUnYf5RgLoMgMn4Pyd5j_DTLgDl-fACLcBGAs/s1600/policy_images_1.png" />
                                        <div className="service-content">
                                            <p>Miễn phí vận chuyển</p>
                                            <span>Cho các đơn hàng &gt; 5tr</span>
                                        </div>
                                    </NavLink>
                                </div>
                                <div className="item_service clearfix">
                                    <NavLink to="#" title="Hỗ trợ 24/7">
                                        <img alt="Hỗ trợ 24/7" src="https://lh4.googleusercontent.com/-QcvG46zrqc4/XPXFtXQeqvI/AAAAAAAAIqk/kbki5uj9KGQhoHFRwnl6bGP-Cn610ZVWACLcBGAs/s1600/policy_images_4.png" />
                                        <div className="service-content">
                                            <p>Hỗ trợ 24/7</p>
                                            <span>Liên hệ hỗ trợ 24h/ngày</span>
                                        </div>
                                    </NavLink>
                                </div>
                                <div className="item_service clearfix">
                                    <NavLink to="#" title="Hoàn tiền 100%">
                                        <img alt="Hoàn tiền 100%" src="https://lh4.googleusercontent.com/-XRqhM2tC-4Q/XPXFa3Bd6zI/AAAAAAAAIqY/Z30_GU9UmJEH4WFYTCkhXiCDtvZzJhIUQCLcBGAs/s1600/policy_images_2.png" />
                                        <div className="service-content">
                                            <p>Hoàn tiền 100%</p>
                                            <span>Nếu sản phẩm bị lỗi, hư hỏng</span>
                                        </div>
                                    </NavLink>
                                </div>
                                <div className="item_service clearfix">
                                    <NavLink to="#" title="Chất lượng cao">
                                        <img alt="Chất lượng cao" src="https://lh4.googleusercontent.com/-RvQwJPz0knE/XPXFmbbfOiI/AAAAAAAAIqc/qRKoO6XIwuYHvM6LDWrtr2E_I7h4MB-9ACLcBGAs/s1600/policy_images_3.png" />
                                        <div className="service-content">
                                            <p>Chất lượng cao</p>
                                            <span>Đảm bảo hàng chính hãng 100%</span>
                                        </div>
                                    </NavLink>
                                </div>
                            </div>
                            <div className="widget-item info-contact in-fo-page-content">
                                <div className="logos text-xs-left">
                                    <NavLink className="logo-wrapper" to="/">
                                        <img alt="logo " className="" src={process.env.PUBLIC_URL + '/logo.svg'} style={{backgroundColor:'#F67A80', marginTop:'10px'}} />
                                    </NavLink>
                                </div>
                                <br />
                                Uy tín chất lượng, đảm bảo hàng đầu.<br /><br />
                                <ul className="widget-menu contact-info-page">
                                    <li><i aria-hidden="true" className="fa fa-map-marker color-x"></i> Số 8A Tôn Thất Thuyết, Mỹ Đình, Nam Từ Liêm, Hà Nội 100000.</li>
                                    <li><i aria-hidden="true" className="fa fa-phone color-x"></i> <a href="tel:0399002423">0399 002 423</a></li>
                                    <li><i aria-hidden="true" className="fa fa-envelope-o"></i> <a href="mailto:lundecor@gmail.com">lundecor@gmail.com</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Checkout;