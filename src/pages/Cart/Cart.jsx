/* eslint-disable */
import React from 'react';
import {Link, NavLink} from 'react-router-dom'

function Cart() {
    return (
        <div>
            <div className="container white collections-container margin-bottom-20"  style={{marginTop:'2em'}}>
                <div className="white-background">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="shopping-cart">
                                <div className="visible-md visible-lg">
                                    <div className="shopping-cart-table">
                                        <div className="row">
                                            <div className="col-md-12">
                                                <h1 className="lbl-shopping-cart lbl-shopping-cart-gio-hang">Giỏ hàng <span>(<span className="count_item_pr">1</span> sản phẩm)</span></h1>
                                            </div>
                                        </div>
                                        <div className="row">

                                            <div className="col-main cart_desktop_page cart-page">
                                                <form id="shopping-cart" action="/cart" method="post" noValidate="" className="has-validation-callback">
                                                    <div className="cart page_cart cart_des_page hidden-xs-down">
                                                        <div className="col-xs-9 cart-col-1">
                                                            <div className="cart-tbody">
                                                                <div className="row shopping-cart-item productid-22735316">
                                                                    <div className="col-xs-3 img-thumnail-custom">
                                                                        <p className="image"><img className="img-responsive" src="https://drive.google.com/uc?export=view&id=1S4ewg_8Walm44THWY3yOL76YE2j7ogk_" alt="Cây Lan Ý trong nước" /></p>
                                                                    </div>
                                                                    <div className="col-right col-xs-9">
                                                                        <div className="box-info-product">
                                                                            <p className="name"><NavLink to="/cay-lan-y-trong-nuoc" title="Cây Lan Ý trong nước" target="_blank">Cây Lan Ý trong nước</NavLink></p>
                                                                            <p className="seller-by hidden" style={{ display: "none" }}>Default Title</p>
                                                                            <p className="action"><NavLink to="javascript:;" className="btn btn-link btn-item-delete remove-item-cart" data-id="22735316" title="Xóa">Xóa</NavLink></p>
                                                                        </div>
                                                                        <div className="box-price">
                                                                            <p className="price">5,000,000₫</p>
                                                                        </div>
                                                                        <div className="quantity-block">
                                                                            <div className="input-group bootstrap-touchspin">
                                                                                <div className="input-group-btn">
                                                                                    <input className="variantID" type="hidden" name="variantId" defaultValue="22735316" />
                                                                                    <button className="increase_pop items-count btn-plus btn btn-default bootstrap-touchspin-up" type="button">+</button>
                                                                                    <input type="text" maxLength="12" min="1" disabled="" className="form-control quantity-r2 quantity js-quantity-product input-text number-sidebar input_pop input_pop qtyItem22735316" id="qtyItem22735316" name="Lines" size="4" defaultValue="1" />
                                                                                    <button className="reduced_pop items-count btn-minus btn btn-default bootstrap-touchspin-down" type="button">–</button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xs-3 cart-col-2 cart-collaterals cart_submit">
                                                            <div id="right-affix">
                                                                <div className="each-row">
                                                                    <div className="box-style fee">
                                                                        <p className="list-info-price"><span>Tạm tính: </span><strong className="totals_price price _text-right text_color_right1">5,000,000₫</strong></p>
                                                                    </div>
                                                                    <div className="box-style fee">
                                                                        <div className="total2 clearfix"><span className="text-label">Thành tiền: </span>
                                                                            <div className="amount">
                                                                                <p><strong className="totals_price">5,000,000₫</strong></p>
                                                                            </div>
                                                                        </div>
                                                                    </div><NavLink to="/checkout" className="button btn-proceed-checkout btn btn-large btn-block btn-danger btn-checkout" title="Thanh toán ngay">Thanh toán ngay</NavLink><Link to="/" className="button btn-proceed-checkout btn btn-large btn-block btn-danger btn-checkouts" title="Tiếp tục mua hàng" type="button">Tiếp tục mua hàng</Link>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div className="visible-sm visible-xs">

                                    <div className="cart-mobile">
                                        <form action="/cart" method="post" className="margin-bottom-0 has-validation-callback">
                                            <div className="header-cart">
                                                <div className="title-cart">
                                                    <h3>Giỏ hàng của bạn</h3>
                                                </div>
                                            </div>
                                            <div className="header-cart-content">
                                                <div className="cart_page_mobile content-product-list">
                                                    <div className="item-product item productid-22735316 ">
                                                        <div className="item-product-cart-mobile"><NavLink to="#"></NavLink><NavLink to="#" className="product-images1" title=""><img width="80" height="150" alt="" src="//bizweb.dktcdn.net/thumb/small/100/344/983/products/cay-thinh-vuong-trong-nuoc-1-1543039041.jpg" /></NavLink></div>
                                                        <div className="title-product-cart-mobile">
                                                            <h3><NavLink to="#" title=""></NavLink></h3>
                                                            <p>Giá: <span>5,000,000₫</span></p>
                                                        </div>
                                                        <div className="select-item-qty-mobile">
                                                            <div className="txt_center">
                                                                <input className="variantID" type="hidden" name="variantId" defaultValue="22735316" />
                                                                <button className="reduced items-count btn-minus" type="button">–</button>
                                                                <input type="text" maxLength="12" min="0" disabled="" className="input-text number-sidebar qtyMobile22735316" id="qtyMobile22735316" name="Lines" size="4" defaultValue="1" />
                                                                <button className="increase items-count btn-plus" type="button">+</button></div><NavLink className="button remove-item remove-item-cart" to="javascript:;" data-id="22735316" title="Xóa">Xoá</NavLink>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="header-cart-price">
                                                    <div className="title-cart clearfix">
                                                        <h3 className="text-xs-left">Tổng tiền</h3><NavLink to="#" className="text-xs-right totals_price_mobile" title="5,000,000₫">5,000,000₫</NavLink>
                                                    </div>
                                                    <div className="checkout"><button className="btn-proceed-checkout-mobile" title="Thanh toán ngay" type="button"><span>Thanh toán ngay</span></button></div><button className="btn btn-proceed-continues-mobile" title="Tiếp tục mua hàng" type="button" >Tiếp tục mua hàng</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Cart;